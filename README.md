# Formalization of graph rewriting in Coq

An ongoing work on formalizing results about graph rewriting in Coq. Currently contains a formalization of Adhesive categories.

## Acknoledgements
This project was initially built on top of John Wiegley's [Category theory in Coq](https://github.com/jwiegley/category-theory) library. While this is not the case anymore, several definitions are heavily inspired from this library.

The definition of categories we currently use has been written by Cyril Cohen for Hierarchy Builder.

We also use a file for [quotients](https://poleiro.info/posts/2019-12-25-quotients-in-coq.html) written by Arthur Azevedo de Amorim.

## Requirements
Works with Coq 8.20.0\
Hierarchy Builder 1.8.0\
Ssreflect 2.3.0

## Mapping of the theorems of the JFLA 2025 article

### Theorem 1
In `Rm_Adhesive.v`\
1 -> 3: factory `Cat_is_Rm_Adhesive_usual`\
2 -> 3: factory `Cat_is_Rm_Adhesive_rm_adh`\
3 -> 1: lemma `Reg_mono_is_adh` (in `Rm_Quasi_Adhesive.v`)\
3 -> 2: lemma `Po_rm_VK`

### Theorem 2
In `Adhesive.v`\
1 -> 4: factory `Cat_is_Adhesive`\
2 -> 4: factory `Cat_is_Adhesive_adh_mono`\
3 -> 4: factory `Cat_is_Adhesive_quasi`\
4 -> 1: lemmas `po_along_mono` and `Po_mono_VK`\
4 -> 2: lemma `Mono_is_adh`\
4 -> 3: lemmas `po_along_mono`, `Po_mono_stable` and `Po_mono_pb`

## File contents
`cat.v`: written by Cyril Cohen, and contains the basic definition for categories.

`cat_notations.v`: basic notations.

`Utils.v`: general lemmas.

`Morphism.v`: monomorphisms and epimorphisms.

`Slice.v`: slice categories.

`Unique.v`: definitions for unicity, especially for the existence of a unique morphism.

`Subcategory.v`: subcategories (full, wide or general) and proofs that they give categories.

`Isomorphism.v`: category of isomorphisms and notations.

`Terminal.v`: terminal objects.

`Initial.v`: dual of `Terminal.v`.

`Product.v`: product and coproduct.

`Span.v`: spans and the category of spans.

`Cospan.v`: dual of `Span.v`.

`Square.v`: the category of spans commuting with a given cospan.

`Pullback.v`: definition of pullbacks, basic lemmas and the definition of categories with all pullbacks.

`Pushout.v`: definition of pushouts, mostly the dual of `Pullback.v`.

`Pullback_lemmas.v`: lemmas about pullbacks.

`Pushout_lemmas.v`: lemmas about pushout, many of them as the dual of lemmas in `Pullback_lemmas.v`.

`Equalizer.v`: definition of equalizers and some lemmas.

`Reg_mono.v`: definition of regular monomorphisms, some lemmas and the definition of categories with pushouts along regular monomorphisms.

`Split_mono.v`: definition of split monomorphisms and some lemmas.

`Subobject.v`: definition of the category of subobjects and definitions and lemmas about the binary union and intersection of subobjects.

`VanKampen.v`: definition of Van Kampen pushouts and some lemmas.

`Cokernel_pair.v`: lemmas about cokernel pairs (pushout of a morphism along itself).

`Adhesive_morphisms.v`: definition of (pre-)adhesive morphisms and some lemmas (see Garner & Lack, On the axioms for adhesive and quasiadhesive categories, 2012).

`Rm_Quasi_Adhesive.v`: definition of rm-quasiadhesive categories and lemmas.

`Rm_Adhesive.v`: definition of rm-adhesive categories and lemmas.

`Adhesive.v`: definition of Adhesive categories and lemmas.

`Quotients.v`: comes from https://poleiro.info/posts/2019-12-25-quotients-in-coq.html

Under `Instances`:

Under `Sets`:\
`Sets.v`: category of sets.\
`Sets_pb.v`: proof that the category of sets has all pullbacks.\
`Sets_adhesive.v`: proof that the category of sets is adhesive, using propositional extensionality and constructive definite description.

Under `Simple_graphs`:\
`SGraph.v`: category of simple graphs, and proof that is it rm-quasiadhesive, using the proof of adhesivity for Sets.


