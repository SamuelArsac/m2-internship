(** * Terminal objects *)

(** ** Definition of [Terminal] *)

Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Isomorphism.
Require Import Unique.

Local Open Scope cat.

Definition Terminal {𝐂: quiver} (t: 𝐂) := forall x, ∃! f: x ~> t, True.

Definition term_one {𝐂: quiver} [t: 𝐂] [term: Terminal t] x: x ~> t
  := unique_morph (term x).
Definition term_one_unique {𝐂: quiver} [t: 𝐂] [term: Terminal t] [x]
  := fun g => unique_morph_uniqueness (term x) g I.

(** ** Lemma: uniqueness of the terminal object *)

Lemma unique_terminal {𝐂: precat}:
  forall (t t': 𝐂) (term_t: Terminal t) (term_t': Terminal t'),
  ∃! h: t ≅ t', True.
Proof.
  intros.
  pose (f := term_t' t).
  pose (g := term_t t').
  exists (Build_Iso f g
   (Unicity (term_t' t') (f ∘ g) idmap I I,
   Unicity (term_t t) (g ∘ f) idmap I I)).
  trivial.
  intros iso _.
  apply Iso_mapP.
  simpl.
  split.
  by apply (''f).
  by apply (''g).
Qed.
