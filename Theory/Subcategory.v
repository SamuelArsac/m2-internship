Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Unique.

Local Open Scope cat.

Section Full_Subcat.

Definition Full_sub {𝐂: quiver} (fsobj: 𝐂 -> Prop) :=
  {x: 𝐂 | fsobj x}.

HB.instance Definition _ (𝐂: quiver) (fsobj: 𝐂 -> Prop) :=
  IsQuiver.Build (Full_sub fsobj)
  (fun x y => proj1_sig x ~> proj1_sig y).
HB.instance Definition _ (𝐂: precat) (fsobj: 𝐂 -> Prop) :=
  Quiver_IsPreCat.Build (Full_sub fsobj)
  (fun x => @idmap _ (proj1_sig x))
  (fun x y z f g => g ∘ f).
HB.instance Definition _ (𝐂: cat) (fsobj: 𝐂 -> Prop) :=
  PreCat_IsCat.Build (Full_sub fsobj)
  (fun x y f => comp1o f)
  (fun x y f => compo1 f)
  (fun w x y z f g h => compoA f g h).

Lemma Build_full_subcat_hom {𝐂: quiver} (fsobj: 𝐂 -> Prop):
  forall (x y: 𝐂) (sobjx: fsobj x)
  (sobjy: fsobj y),
  (x ~> y) -> ((exist _ x sobjx) ~>_(Full_sub fsobj) (exist _ y sobjy)).
Proof.
  intros x y sobjx sobjy f.
  assumption.
Qed.

Lemma Destruct_full_subcat_hom {𝐂: quiver} (fsobj: 𝐂 -> Prop): forall (x y: Full_sub fsobj),
  (x ~> y) -> (proj1_sig x) ~>_𝐂 (proj1_sig y).
Proof.
  intros x y f.
  assumption.
Qed.

End Full_Subcat.

Definition pre_hom_sub (𝐂: quiver) := ∀ (x y : 𝐂), (x ~> y) → Prop.

HB.mixin Record Is_Hom_sub (𝐂: precat)
  (P: pre_hom_sub 𝐂) := {
  wscomp {x y z : 𝐂} {f : y ~> z} {g : x ~> y}:
    P _ _ f → P _ _ g → P _ _ (f ∘ g);
  wsid {x : 𝐂}: P _ _ (@idmap 𝐂 x)
}.
Unset Universe Checking.
#[short(type="hom_sub")]
HB.structure Definition Hom_sub (𝐂: precat): Set :=
  { P of Is_Hom_sub 𝐂 P }.
Set Universe Checking.

Section Wide_Subcat.

Definition Wide_sub {𝐂: quiver} (wshom: pre_hom_sub 𝐂): Type := 𝐂.

HB.instance Definition _ (𝐂: quiver) (wshom: pre_hom_sub 𝐂) :=
  IsQuiver.Build (Wide_sub wshom)
  (fun x y => {f: x ~> y | wshom _ _ f}).

Lemma wscomp' (𝐂: precat) (wshom: hom_sub 𝐂): forall (x y z: Wide_sub wshom)
  (f: x ~> y) (g: y ~> z), x ~> z.
Proof.
  intros.
  destruct f as [f ws_f].
  destruct g as [g ws_g].
  exists (g ∘ f).
  by apply wscomp.
Defined.

Lemma wsid' (𝐂: precat) (wshom: hom_sub 𝐂): forall (x: Wide_sub wshom),
  x ~> x.
Proof.
  intro.
  exists (@idmap _ x).
  apply wsid.
Defined.

Definition Build_WS_hom {𝐂: quiver} [x y: 𝐂] (P: pre_hom_sub 𝐂)
  (a: x ~> y) (Pa: P _ _ a): x ~>_(Wide_sub P) y := exist _ a Pa.

Definition WS_hom {𝐂: quiver} [x y: 𝐂] [P: pre_hom_sub 𝐂]
  (a: x ~>_(Wide_sub P) y): x ~> y := proj1_sig a.

Lemma WS_mapP (𝐂: quiver) (wshom: pre_hom_sub 𝐂)
  (x y: Wide_sub wshom) (f g: x ~> y):
  proj1_sig f = proj1_sig g ↔ f = g.
Proof.
  split.
  intro eq.
  destruct f as [f ws_f].
  destruct g as [g ws_g].
  simpl in eq.
  revert ws_f.
  rewrite eq.
  intro.
  f_equal.
  apply Prop_irrelevance.
  intros eq.
  by rewrite eq.
Defined.

HB.instance Definition _ (𝐂: precat) (wshom: hom_sub 𝐂) :=
  Quiver_IsPreCat.Build (Wide_sub wshom)
  (wsid' _ wshom) (wscomp' _ wshom).

Lemma wscomp1o (𝐂: cat) (wshom: hom_sub 𝐂):
  forall (x y: Wide_sub wshom) (f: x ~> y), idmap\; f = f.
Proof.
  intros.
  destruct f as [f ws_f].
  apply WS_mapP.
  simpl.
  by rewrite comp1o.
Qed.

Lemma wscompo1 (𝐂: cat) (wshom: hom_sub 𝐂):
  forall (x y: Wide_sub wshom) (f: x ~> y), f \; idmap = f.
Proof.
  intros.
  destruct f as [f ws_f].
  apply WS_mapP.
  simpl.
  by rewrite compo1.
Qed.

Lemma wscompoA (𝐂: cat) (wshom: hom_sub 𝐂):
  forall (w x y z: Wide_sub wshom)
  (f: w ~> x) (g: x ~> y) (h: y ~> z), f \; (g \; h) = (f \; g) \; h.
Proof.
  intros.
  destruct f as [f ws_f].
  destruct g as [g ws_g].
  destruct h as [h ws_h].
  apply WS_mapP.
  simpl.
  by rewrite compoA.
Qed.
  
HB.instance Definition _ (𝐂: cat) (wshom: hom_sub 𝐂) :=
  PreCat_IsCat.Build (Wide_sub wshom)
  (wscomp1o 𝐂 wshom) (wscompo1 𝐂 wshom) (wscompoA 𝐂 wshom).

End Wide_Subcat.

Lemma Unique_WS_eq {𝐂: quiver} (x y: 𝐂) (P: pre_hom_sub 𝐂):
  (∃! a: x ~> y, P _ _ a) ↔ (∃! a: x ~>_(Wide_sub P) y, True).
Proof.
  split.
  intro a.
  unshelve eexists.
  exists a.
  apply ('a).
  trivial.
  intros [v Pv] _.
  apply WS_mapP.
  by apply (''a).
  intros [[a Pa] _ unique_a].
  exists a.
  assumption.
  intros v Pv.
  pose (eq := unique_a (Build_WS_hom P v Pv) I).
  by apply WS_mapP in eq.
Qed.

Lemma Unique_Full_eq {𝐂: quiver} (P: 𝐂 -> Prop) (x y: 𝐂) (Px: P x) (Py: P y)
  (Q: (x ~> y) -> Type):
  (∃! a: (exist _ x Px) ~>_(Full_sub P) (exist _ y Py), Q a) ↔
  ∃! a: x ~> y, Q a.
Proof.
  by auto.
Qed.

Section Subcat.

Definition Sub {𝐂: quiver} (sobj: 𝐂 -> Prop)
  (wshom: pre_hom_sub (Full_sub sobj))
  := Wide_sub wshom.

End Subcat.