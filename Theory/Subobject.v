Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Subcategory.
Require Import Slice.
Require Import Morphism.
Require Import Isomorphism.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pushout.
Require Import Unique.
Require Import Utils.

Local Open Scope cat.

Section Subobject.

Context {𝐂: cat}.

Definition Subobj (C: 𝐂) := @Full_sub (Slice C) (fun x => Mono (`2 x)).

Definition Build_subobj [A C: 𝐂] (m: A ~> C) (mono_m: Mono m): Subobj C.
  unshelve econstructor.
  econstructor.
  apply m.
  assumption.
Defined.

End Subobject.

Definition Subobj_inter {𝐂: cat} (X: 𝐂) (A B I: Subobj X) :=
  ∃ (a: I ~> A) (b: I ~> B), forall I' (a': I' ~> A) (b': I' ~> B),
  ∃! (u: I' ~> I), a ∘ u = a' ∧ b ∘ u = b'.

Definition Subobj_union {𝐂: cat} (X: 𝐂) (A B U: Subobj X) :=
  ∃ (a: A ~> U) (b: B ~> U), forall U' (a': A ~> U') (b': B ~> U'),
  ∃! (u: U ~> U'), u ∘ a = a' ∧ u ∘ b = b'.

Lemma subobj_pb_is_inter {𝐂: cat} [A B C D: 𝐂] [f: A ~> C] [g: B ~> C]
  [pA: D ~> A] [pB: D ~> B] (mono_f: Mono f) (mono_g: Mono g)
  (pb: Pullback f g pA pB): ∃ (mono_fpA: Mono (f ∘ pA)),
  Subobj_inter _ (Build_subobj f mono_f) (Build_subobj g mono_g)
    (Build_subobj (f ∘ pA) mono_fpA).
Proof.
  simpl.
  pose proof (mono_pB := Mono_stable_pb pb mono_f).
  apply Pb_sym in pb.
  pose proof (mono_pA := Mono_stable_pb pb mono_g).
  pose proof (mono_fpA := Mono_comp _ _ mono_pA mono_f).
  destruct (Destruct_Pullback pb) as [pb_comm ump_pb].
  exists mono_fpA.
  unfold Subobj_inter.
  simpl.
  unshelve eexists.
  by unshelve eapply (exist _ pA _).
  unshelve eexists.
  unshelve eapply (exist _ pB pb_comm).
  intros [[D' m] mono_m] [qA eq_qA] [qB eq_qB].
  simpl in *.
  unshelve epose (ump_pb D' qB qA _).
  by rewrite eq_qA eq_qB.
  unshelve eexists.
  unshelve eapply (exist _ (u :> D' ~> D) _).
  simpl.
  by rewrite compoA -('u).
  split;
  apply sig_eqP;
  symmetry;
  apply ('u).

  intros v [eq_v1 eq_v2].
  destruct v as [v ?]; simpl in *.
  apply sig_eqP.
  apply sig_eqP in eq_v1, eq_v2.
  apply (''u).
  by split; symmetry.
Qed.

Definition Subobj_effective_union {𝐂: cat} [A B Z: 𝐂] 
  (a: A ~> Z) (b: B ~> Z) (mono_a: Mono a)
  (mono_b: Mono b) :=
  ∃ (C I: 𝐂) (p: I ~> A) (u: A ~> C)
  (q: I ~> B) (v: B ~> C) (pb: Pullback a b p q)
  (po: Pushout p q u v), forall (c: C ~> Z),
  a = c ∘ u ∧ b = c ∘ v -> Mono c.

Lemma Subobj_effective_union_mono {𝐂: cat} [A B C I Z: 𝐂]
  (a: A ~> Z) (b: B ~> Z) (c: C ~> Z) (mono_a: Mono a)
  (mono_b: Mono b)
  (effective: Subobj_effective_union a b mono_a mono_b)
  (p: I ~> A) (u: A ~> C) (q: I ~> B) (v: B ~> C)
  (pb: Pullback a b p q) (po: Pushout p q u v):
  c ∘ u = a ∧ c ∘ v = b -> Mono c.
Proof.
  intros [eq_c1 eq_c2].
  destruct effective
    as [C' [I' [p' [u' [q' [v' [pb' [po' prop]]]]]]]].
  destruct (unique_pullback pb' pb) as [i1 [eq_i11 eq_i12] _].
  rewrite -(comp1o p') -(iso_to_from (iso_eq i1))
    -compoA in po'.
  apply Pushout_iso_C in po'.
  rewrite eq_i11 eq_i12 in po'.
  destruct (unique_pushout po' po) as [i2 [eq_i21 eq_i22] _].
  apply (Mono_decomp_iso i2).
  apply prop.
  split.
  by rewrite compoA eq_i21 eq_c1.
  by rewrite compoA eq_i22 eq_c2.
Defined.

Lemma Subobj_effective_union_is_union {𝐂: cat} [A B C I Z: 𝐂]
  (a: A ~> Z) (b: B ~> Z) (c: C ~> Z) (mono_a: Mono a)
  (mono_b: Mono b)
  (effective: Subobj_effective_union a b mono_a mono_b)
  (p: I ~> A) (u: A ~> C) (q: I ~> B) (v: B ~> C)
  (pb: Pullback a b p q) (po: Pushout p q u v)
  (eq_c: c ∘ u = a ∧ c ∘ v = b):
  Subobj_union Z (Build_subobj a mono_a)
  (Build_subobj b mono_b)
  (Build_subobj c (Subobj_effective_union_mono a b c
    _ _ effective p u q v pb po eq_c)).
Proof.
  unshelve eexists.
  exists u.
  apply eq_c.
  unshelve eexists.
  exists v.
  apply eq_c.

  intros [[C' c'] mono_c'] [u' eq_u'] [v' eq_v'].
  simpl in *.
  unshelve epose proof (χ := ump_po po u' v' _).
  apply mono_c'.
  by rewrite -2!compoA eq_u' eq_v' (pb_comm pb).
  unshelve eexists.
  exists χ.
  simpl.
  apply (Pushout_joint_epi po).
  by rewrite eq_c compoA -('χ).
  by rewrite eq_c compoA -('χ).
  split;
  (apply sig_eqP;
  simpl);
  by rewrite -('χ).
  intros [χ' eq_χ'] [eq1 eq2].
  simpl in *.
  apply sig_eqP in eq1, eq2.
  simpl in eq1, eq2.
  apply sig_eqP.
  by apply (''χ).
Qed.

Lemma Subobj_effective_union_exists {𝐂: cat} [A B Z: 𝐂]
  (a: A ~> Z) (b: B ~> Z) (mono_a: Mono a)
  (mono_b: Mono b)
  (effective: Subobj_effective_union a b mono_a mono_b):
  ∃ C (c: C ~> Z) (mono_c: Mono c),
  Subobj_union Z (Build_subobj a mono_a)
  (Build_subobj b mono_b)
  (Build_subobj c mono_c).
Proof.
  destruct effective as [C [I [p [u [q [v [pb [po prop]]]]]]]] eqn: H.
  exists C.
  pose proof (c := ump_po po a b (pb_comm pb)).
  exists c.
  exists (prop c ('c)).
  unshelve eapply (Subobj_effective_union_is_union a b c
    _ _ effective p u q v pb po).
  split; by rewrite -('c).
Qed. 

Lemma Subobj_effective_union_pb_po {𝐂: cat_pb} [A B C Z: 𝐂]
  (a: A ~> Z) (b: B ~> Z) (c: C ~> Z) (mono_a: Mono a)
  (mono_b: Mono b) (mono_c: Mono c)
  (union: Subobj_union Z (Build_subobj a mono_a)
    (Build_subobj b mono_b)
    (Build_subobj c mono_c))
  (effective: Subobj_effective_union a b mono_a mono_b):
  ∃ I (p: I ~> A) (u: A ~> C) (q: I ~> B) (v: B ~> C)
  (pb: Pullback a b p q) (po: Pushout p q u v),
  c ∘ u = a ∧ c ∘ v = b.
Proof.
  destruct union as [[u eq_u] [[v eq_v] ump]].
  simpl in *.
  destruct effective as [C' [I [p [u' [q [v' [pb [po effective]]]]]]]].
  exists I.
  exists p.
  exists u.
  exists q.
  exists v.
  exists pb.
  pose proof (c' := ump_po po a b (pb_comm pb)).
  unshelve epose proof (i := ump_po po u v _).
  apply mono_c.
  rewrite -2!compoA eq_u eq_v.
  apply (pb_comm pb).
  assert (iso_i: IsIso i).
  unshelve epose proof (i' := ump _ _ _).
  unshelve econstructor.
  eexists.
  apply c'.
  apply (effective c' ('c')).
  exists u'.
  symmetry.
  apply ('c').
  simpl.
  trivial.
  exists v'.
  symmetry.
  apply ('c').
  simpl.
  trivial.
  destruct i' as [[i' eq_i'c'] [eq_i'1 eq_i'2] _].
  simpl in *.
  apply sig_eqP in eq_i'1, eq_i'2.
  simpl in eq_i'1, eq_i'2.
  exists i'.
  assert (eq_i'i: i' ∘ i = idmap).
  apply (Pushout_joint_epi po).
  by rewrite compoA compo1 -('i).
  by rewrite compoA compo1 -('i).
  constructor.
  apply mono_c.
  by rewrite -{1}eq_i'c' comp1o compoA -(compoA i')
    eq_i'i compo1.
  assumption.
  destruct i as [i [eq_i1 eq_i2] ?].
  simpl in iso_i.
  promote_iso i.
  apply (Pushout_iso_D' i) in po.
  rewrite -eq_i1 -eq_i2 in po.
  by exists po.
Defined.
