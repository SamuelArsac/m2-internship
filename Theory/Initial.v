Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Terminal.
Require Import Isomorphism.
Require Import Unique.

Local Open Scope cat.

Definition Initial {𝐂: quiver} (i: 𝐂) := @Terminal 𝐂^op i.

Definition init_one {𝐂: quiver} [i: 𝐂] (init: Initial i):
  ∀ x: 𝐂, i ~> x := @term_one _ _ init.

Definition init_one_unique {𝐂: quiver} [i: 𝐂] [init: Initial i] [x: 𝐂]
  (g: i ~> x): ((init x) :> i ~> x) = g
  := @term_one_unique 𝐂^op _ init x g.

(** ** Lemma: uniqueness of the terminal object *)

Lemma unique_initial {𝐂: precat}:
  forall (i i': 𝐂) (init_i: Initial i) (init_i': Initial i'),
  ∃! h: i ≅ i', True.
Proof.
  intros i i' init_i init_i'.
  pose (iso := @unique_terminal 𝐂^op i' i init_i' init_i).
  exists (iso_op' iso).
  trivial.
  intros iso' _.
  by rewrite ((''iso) (iso_op iso') I) iso_op'_op.
Qed.