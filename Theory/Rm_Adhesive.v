From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Utils.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Product.
Require Import Cokernel_pair.
Require Import Split_mono.
Require Import Subcategory.
Require Import Adhesive_morphisms.
Require Import Rm_Quasi_Adhesive.

Local Open Scope cat.

HB.mixin Record Rm_Q_Adhesive_is_Rm_Adhesive 𝐂 of Rm_Q_Adhesive 𝐂 := {
  rm_subobj_stb_union: forall [A B C Z: 𝐂] [a: A ~> Z] [b: B ~> Z]
    (rm_a: Reg_mono a) (rm_b: Reg_mono b) (c: C ~> Z) (mono_c: Mono c),
    Subobj_union Z (Build_subobj a (Reg_mono_is_mono _ rm_a))
    (Build_subobj b (Reg_mono_is_mono _ rm_b))
    (Build_subobj c mono_c)
    -> Reg_mono c
}.

Unset Universe Checking.
#[short(type="rm_adhesive")]
HB.structure Definition Rm_Adhesive: Set :=
  { 𝐂 of Rm_Q_Adhesive_is_Rm_Adhesive 𝐂 & }.
Set Universe Checking.

(* Factories and lemmas for Rm_adhesive categories *)

Lemma VanKampen_stable {𝐂: cat}: forall [A B C D: 𝐂]
  [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
  (po: Pushout f g iA iB), VanKampen po -> Stable_po po.
Proof.
  intros A B C D f g iA iB po VK.
  unfold Stable_po.
  intros.
  apply Pb_sym in pb1, pb2, pb3, pb4.
  destruct (VK _ _ _ _ f' g' iA' iB' α β χ δ); auto;
    by apply pb_comm; apply Pb_sym.
Qed.

HB.factory Record Cat_is_Rm_Adhesive_usual 𝐂 of Cat_pb 𝐂 & Cat_po_along_reg_mono 𝐂 := {
  po_rm_VK: forall [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
    [iA: A ~> D] [iB: B ~> D] (rm_f: Reg_mono f) (po: Pushout f g iA iB),
    VanKampen po;
}.

Section Rm_Adhesive_usual.

Context {𝐂: rm_q_adhesive}.

Hypothesis po_rm_VK: forall [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
    [iA: A ~> D] [iB: B ~> D] (rm_f: Reg_mono f) (po: Pushout f g iA iB),
    VanKampen po.

Arguments po_rm_VK [_ _ _ _ _ _ _ _].

(* Adhesive and quasiadhesive categories - Lack & Sobocinski - 2005
Lemma 6.5 *)
Lemma Reg_mono_stable_po [A B C D: 𝐂]
  [f: C ~> B] [g: A ~> D] [m: C ~> A] [n: B ~> D]
  (rm_m: Reg_mono m): Pushout m f g n -> Reg_mono n.
Proof.
  intro po_D.
  destruct (po_along_reg_mono m m rm_m) as [A' [p [q po_A']]].
  simpl in A'.
  destruct (Cokernel_rm_coproj po_A') as [rm_p rm_q].
  destruct (po_along_reg_mono q g rm_q) as [D' [h [s po_D']]].
  simpl in D'.
  destruct (ump_po po_D (h ∘ p) (s ∘ n)) as [r [eq_r1 eq_r2] _].
  by rewrite compoA (po_comm po_A') -compoA (po_comm po_D') compoA
    (po_comm po_D) compoA.
  pose (po_is_pb po_D rm_m).
  destruct (po_rm_VK rm_q po_D' _ _ _ _ _ _ _
    _ _ _ _ _ (po_comm po_D) (eq_sym eq_r1) (eq_sym eq_r2)
    (Pb_sym (po_is_pb po_A' rm_m)) (po_is_pb po_D rm_m)) as [_ VK].
  destruct (VK po_D) as [_ pb_B].
  do 3 eexists.
  apply (Pb_equalizer pb_B).
Qed.

(* Quasitoposes, Quasiadhesive Categories and Artin Glueing -
Johnstone et al. - 2007
Theorem 19. In [rm-adhesive] categories, binary unions of
regular subobjects are regular. *)
Lemma reg_subobj_union_is_reg_subobj:
  forall [U V X Z: 𝐂] [sub_U: U ~> Z] [sub_V: V ~> Z]
    (rm_sub_U: Reg_mono sub_U) (rm_sub_V: Reg_mono sub_V)
    (sub_X: X ~> Z) (mono_sub_X: Mono sub_X),
    Subobj_union Z (Build_subobj sub_U (Reg_mono_is_mono _ rm_sub_U))
    (Build_subobj sub_V (Reg_mono_is_mono _ rm_sub_V))
    (Build_subobj sub_X mono_sub_X)
    -> Reg_mono sub_X.
Proof.
  (* Suppose that Z ∈ C and that U and V are two regular subobjects of Z [...] *)
  intros ? ? ? ? ? ? ? ? ? ? union.
  destruct (Subobj_effective_union_pb_po sub_U sub_V sub_X
    (Reg_mono_is_mono _ rm_sub_U)
    (Reg_mono_is_mono _ rm_sub_V)
    mono_sub_X union
    (Adhesive_subobj_union_effective _ _
      (Reg_mono_is_mono _ rm_sub_U)
      (Reg_mono_is_adh rm_sub_V)))
    as [W [π1 [u [π2 [v [pb_W [po_X [eq_u eq_v]]]]]]]].
  clear union.

  (* Let X_ denote the smallest regular subobject of Z which contains X,
  ie the join of U and V in the lattice of regular subobjects of Z.
  This object can be obtained by factorising the map X → Z into an epi
  followed by a regular mono. *)
  destruct (reg_union_epi_rm_fact _ _ _ _ _ _ _ rm_sub_U rm_sub_V
    (eq_sym eq_u) (eq_sym eq_v)
    (Pb_sym pb_W) (Po_sym po_X))
    as [X_ [sub_X_ [x_ [eq_x_ [rm_sub_X_ epi_x_]]]]].

  (* We obtain objects A and A_ by constructing the pushouts
  in the left diagram below, where v_ = x_ v.
  Clearly v_ is regular mono by the usual cancellation properties. *)
  pose proof (rm_π2 := Reg_mono_stable_PB _ _ _ _ pb_W rm_sub_U).
  pose proof (rm_π1 := Reg_mono_stable_PB _ _ _ _ (Pb_sym pb_W) rm_sub_V).
  pose proof (rm_v := Reg_mono_stable_po rm_π1 po_X).
  pose proof (rm_u := Reg_mono_stable_po rm_π2 (Po_sym po_X)).
  pose (v_ := x_ ∘ v).
  assert (rm_v_: Reg_mono v_).
  rewrite -eq_v eq_x_ compoA -/v_ in rm_sub_V.
  apply (Reg_mono_decomp v_ sub_X_ rm_sub_V (Reg_mono_is_mono _ rm_sub_X_)).
  pose (u_ := x_ ∘ u).
  assert (rm_u_: Reg_mono u_).
  rewrite -eq_u eq_x_ compoA -/u_ in rm_sub_U.
  apply (Reg_mono_decomp u_ sub_X_ rm_sub_U (Reg_mono_is_mono _ rm_sub_X_)).

  destruct (po_along_reg_mono v v_ rm_v) as [A [p [q po_A]]].
  simpl in A.
  pose proof (rm_q := Reg_mono_stable_po rm_v po_A).
  pose proof (rm_p := Reg_mono_stable_po rm_v_ (Po_sym po_A)).

  destruct (po_along_reg_mono p x_ rm_p) as [A_ [a_ [j1 po_A_]]].
  simpl in A_.
  pose proof (rm_j1 := Reg_mono_stable_po rm_p po_A_).
  pose (j2 := a_ ∘ q).

  assert (po'_A_: Pushout v_ v_ j1 j2).
  apply (Po_comp po_A (Po_sym po_A_)).
  pose proof (sub_A_ := ump_po po'_A_ sub_X_ sub_X_ eq_refl).

  (* The fact that the lower square is a pullback together with
    the fact that x_ is not an isomorphism implies that a_
    is not an isomorphism. *)
  assert (a__to_x__iso: IsIso a_
    -> IsIso x_).
  intros [a__ [a__eq1 a__eq2]].
  pose proof (iso_a_ := Build_IsIso a_ a__
    (Build_Iso_eq a__eq1 a__eq2)).
  clear dependent a__. 
  promote_iso a_.
  assert (pb_a_: Pullback a_¹ j1 p x_).
  apply (po_is_pb po_A_ rm_p).
  apply (Iso_stable_pb _ pb_a_).

  (* Now consider the second diagram above in which (‡)
  is a pushout and u_ = x_ u. *)
  destruct (po_along_reg_mono π1 π1 rm_π1) as [B [i2 [i1 po_B]]].
  simpl in B.
  pose proof (rm_i1 := Reg_mono_stable_po rm_π1 po_B).
  pose proof (rm_i2 := Reg_mono_stable_po rm_π1 (Po_sym po_B)).

  (* Using the fact that uπ1 = vπ2 the pushout of X and U along W is A
  and we obtain a map b : B → A such that the upper right square commutes
  and b i1 = p u.*)
  assert (eq_u__v_: u_ ∘ π1 = v_ ∘ π2).
  by rewrite compoA (po_comm po_X) -compoA.

  unshelve epose proof (b := ump_po po_B (q ∘ u_) (p ∘ u) _).
  by rewrite compoA eq_u__v_ -compoA -(po_comm po_A) compoA
    -(po_comm po_X) compoA.

  (* By the pasting properties of pushouts, this square is also a pushout. *)
  pose proof (po'_A := Po_comp (Po_sym po_X) (Po_sym po_A)).
  
  rewrite -eq_u__v_ ('b) in po'_A.
  unshelve epose proof (po''_A := Po_decomp _ po_B po'_A).
  apply ('b).

  assert (rm_b: Reg_mono b).
  apply (Reg_mono_stable_po rm_u_ po''_A).

  (* Let h : B → U be the codiagonal (the unique map such that
  hi1 = hi2 =idU ) and let r be the unique map which satisfies
  r q = idX_ and r b = u_ h. *)
  pose proof (h := Cokernel_codiagonal po_B).

  unshelve epose proof (r := ump_po po''_A idmap (u_ ∘ h) _).
  by rewrite compo1 compoA -('h) comp1o.
  
  assert (po_X': Pushout b h r u_).
  apply Po_sym.
  eapply Po_decomp.
  by rewrite -('r).
  apply (Po_sym po''_A).
  rewrite -('h) -('r).
  apply Po_sym.
  apply Po_along_id.

  destruct (Get_pb sub_U sub_A_) as [B' [h' [b'_ pb_B']]].
  simpl in B'.

  assert (pb_U: Pullback sub_U sub_X_ idmap u_).
  apply Build_Pullback.
  by rewrite comp1o -compoA -eq_x_ eq_u.
  intros Y qA qB eq.
  exists qA.
  split.
  by rewrite compo1.
  rewrite -eq_u eq_x_ !compoA in eq.
  apply (Reg_mono_is_mono _ rm_sub_X_) in eq.
  by rewrite -eq compoA.
  intros v0 [eq_v0 _].
  by rewrite eq_v0 compo1.

  rewrite ('sub_A_) in pb_U.
  destruct (pb_fill pb_U pb_B') as [i'1 [eq_i'1 pb'_U]].
  rewrite -('sub_A_) (snd ('sub_A_)) in pb_U.
  destruct (pb_fill pb_U pb_B') as [i'2 [eq_i'2 pb''_U]].

  assert (pb'_W: Pullback u_ v_ π1 π2).
  apply Build_Pullback.
  by rewrite !compoA (po_comm po_X).
  intros Y qA qB eq.
  apply (ump_pb pb_W qA qB).
  by rewrite -eq_u -eq_v eq_x_ (compoA u) (compoA v) (compoA qA) (compoA qB)
    eq.

  pose proof (rm_b'_ := Reg_mono_stable_PB _ _ _ _ pb_B' rm_sub_U).
  
  unshelve epose proof (po_B' := stable_po po'_A_ rm_v_ pb'_W pb'_W pb'_U pb''_U _).
  apply (Reg_mono_is_mono _ rm_b'_).
  by rewrite -!compoA (pb_comm pb'_U) (pb_comm pb''_U) !(compoA _ u_)
    !eq_u__v_ -compoA (po_comm po'_A_) compoA.
  
  destruct (unique_pushout (Po_sym po_B') po_B)
    as [iso_B'B [eq_isoB'B2 eq_isoB'B1] _].
    
  pose (b_ := b'_ ∘ iso_B'B⁻¹).
  assert (eq_hh': (h: B ~> U) = h' ∘ iso_B'B⁻¹).
  apply (Pushout_joint_epi po_B).
  by rewrite -('h) -eq_isoB'B2 compoA -(compoA i'2)
    (iso_from_to (iso_eq iso_B'B)) compo1.
  by rewrite -('h) -eq_isoB'B1 compoA -(compoA i'1)
    (iso_from_to (iso_eq iso_B'B)) compo1.

  apply (Pullback_iso_D' (iso_sym iso_B'B)) in pb_B'.
  rename pb_B' into pb_B.
  cbn in pb_B.
  rewrite -eq_hh' -/b_ in pb_B.
  
  pose proof (s := Cokernel_codiagonal po'_A_).

  assert (eq_sub_A_: (sub_A_: A_ ~> Z) = sub_X_ ∘ s).
  apply (Pushout_joint_epi po'_A_);
  by rewrite -('sub_A_) compoA -('s) comp1o.
  rewrite eq_sub_A_ -(compo1 h) in pb_B.
  rewrite eq_sub_A_ compoA -('s) comp1o in pb_U.
  unshelve epose proof (pb'_B:= Pb_decomp _ pb_U pb_B).
  apply (Reg_mono_is_mono _ rm_sub_X_).
  by rewrite -(compoA b_) -(pb_comm pb_B) compo1 -eq_u
    eq_x_ !compoA.

  assert (po_X_: Pushout h b_ u_ s).
  apply (Build_Pushout (pb_comm pb'_B)).
  intros C g f eq_fg.

  pose proof (Epi_h := Cokernel_codiagonal_epi h po_B
  (fst ('h)) (snd ('h))).
  pose proof (Epi_s := Cokernel_codiagonal_epi s po'_A_
  (fst ('s)) (snd ('s))).

  exists (f ∘ j1).
  assert (f = (f ∘ j1) ∘ s).

  apply (Pushout_joint_epi po'_A_).
  by rewrite compoA -('s) comp1o.
  rewrite compoA -('s) comp1o.
  apply epi_x_.
  apply (Pushout_joint_epi po_X).

  rewrite compoA (compoA u) -/u_.

  by rewrite compoA -(pb_comm pb''_U) -(comp1o b'_)
  -(iso_from_to (iso_eq iso_B'B)) !(compoA i'2) eq_isoB'B2
  -(compoA i2) -/b_ -compoA -eq_fg compoA
  -('h) (snd ('h)) -compoA eq_fg (compoA u_)
  -(pb_comm pb'_U) compoA /b_ compoA
  -eq_isoB'B1 -(compoA i'1) (iso_from_to (iso_eq iso_B'B))
  compo1.
  rewrite compoA (compoA v) -/v_.
  by rewrite compoA -(po_comm po'_A_) !compoA.

  split.
  apply Epi_h.
  cbn.
  by rewrite eq_fg compoA (pb_comm pb'_B) -(compoA b_) -H.
  assumption.

  intros.
  by rewrite H compoA -('s) comp1o.

  pose proof (rm_b_ := Reg_mono_iso_r rm_b'_ (iso_sym iso_B'B)).

  assert (eq_b_: a_ ∘ b = b_).
  apply (Pushout_joint_epi po_B).
  by rewrite compoA -('b) -compoA -/j2 /b_ (compoA i2)
    -eq_isoB'B2 -(compoA i'2) (iso_from_to (iso_eq iso_B'B))
    compo1 (pb_comm pb''_U).
  by rewrite compoA -('b) -compoA (po_comm po_A_)
    /b_ (compoA i1) -eq_isoB'B1 -(compoA i'1) (iso_from_to (iso_eq iso_B'B))
    compo1 (pb_comm pb'_U) compoA.

  assert (eq_r: (r: A ~> X_) = s ∘ a_).
  apply (''r).
  split.
  by rewrite compoA -/j2 -('s).
  by rewrite (compoA b) eq_b_ (pb_comm pb'_B).
  
  assert (pb''_B: Pullback a_ b_ b idmap).
  assert (pb''_X_: Pullback (s ∘ a_) u_ b h).
  rewrite -eq_r.
  apply (po_is_pb po_X' rm_b).

  apply Pb_sym.
  apply Pb_sym in pb''_X_.
  rewrite -(comp1o h) in pb''_X_.
  unshelve eapply (Pb_decomp _ pb'_B pb''_X_).
  by rewrite comp1o.

  assert (idmap ∘ r = s ∘ a_).
  rewrite compo1.
  apply (''r).
  split.
  by rewrite compoA -/j2 -('s).
  by rewrite (compoA b) eq_b_ (pb_comm pb'_B).

  unshelve epose proof (po_rm_VK rm_b_
    (Po_sym po_X_) _ _ _ _ _ _ _ _ _ _ _ idmap
    (po_comm po_X') H _ (Pb_sym pb''_B) (Pb_along_id h)).
  by rewrite compo1 comp1o.
  destruct X0.
  destruct (p1 po_X').
  pose proof (Pb_along_id s).
  unshelve epose proof (a__ := ump_pb p2 idmap s _).
  by rewrite compo1 comp1o.

  unshelve epose proof (x__ := a__to_x__iso _).
  exists a__.
  constructor.
  by rewrite -('a__).

  unshelve epose proof (idA := ump_pb p2 a_ r _).
  apply (pb_comm p2).
  unshelve epose proof ((''idA) idmap _).
  split; by rewrite comp1o.
  destruct a__ as [a__ [a___eq1 a___eq2] unique_a__].
  simpl.
  apply (Unicity idA).
  split.
  destruct r as [r [eq_r1 eq_r2] unique_r]; simpl in *.
  by rewrite -compoA -a___eq1 compo1.
  by rewrite -(compoA a_) -a___eq2 (pb_comm p2) compo1.
  split; by rewrite comp1o.

  destruct x__ as [x__ ?].
  pose (iso := Build_Iso x_ x__ i).
  rewrite eq_x_.
  apply (Reg_mono_iso_r rm_sub_X_ iso).
Qed.

End Rm_Adhesive_usual.

HB.builders Context 𝐂 of Cat_is_Rm_Adhesive_usual 𝐂.

HB.instance Definition _ := Cat_is_Rm_Q_Adhesive.Build 𝐂
  (fun A B C D f g iA iB po rm => VanKampen_stable po
    (po_rm_VK _ _ _ _ _ _ _ _ rm po))
  (fun A B C D f g iA iB po rm => VK_mono_pb _ _ _ _ po
    (Reg_mono_is_mono _ rm) (po_rm_VK _ _ _ _ _ _ _ _ rm po)).

HB.instance Definition _ := Rm_Q_Adhesive_is_Rm_Adhesive.Build 𝐂
  (fun A B C Z a b rm_a rm_b c mono_c =>
    reg_subobj_union_is_reg_subobj po_rm_VK rm_a rm_b _ mono_c).

HB.end.

HB.factory Record Cat_is_Rm_Adhesive_rm_adh 𝐂 of Cat_pb 𝐂 := {
  rm_subobj_stb_union: forall [A B C Z: 𝐂] [a: A ~> Z] [b: B ~> Z]
    (rm_a: Reg_mono a) (rm_b: Reg_mono b) (c: C ~> Z) (mono_c: Mono c),
    Subobj_union Z (Build_subobj a (Reg_mono_is_mono _ rm_a))
    (Build_subobj b (Reg_mono_is_mono _ rm_b))
    (Build_subobj c mono_c)
    -> Reg_mono c;
  rm_is_adh: forall [A B: 𝐂] [m: A ~> B], Reg_mono m ->
    Adhesive_morph m
}.

HB.builders Context 𝐂 of Cat_is_Rm_Adhesive_rm_adh 𝐂.

Lemma Po_along_reg_mono [A B C: 𝐂] (m: C ~> A) (g: C ~> B) (rm_m: Reg_mono m):
∃ D (iA: A ~> D) (iB: B ~> D), Pushout m g iA iB.
destruct (Adhesive_is_Pre_adhesive _ (rm_is_adh _ _ _ rm_m) _ g)
as [D [iA [iB [po _]]]].
exists D.
exists iA.
exists iB.
assumption.
Defined.

HB.instance Definition _ := Cat_has_po_along_reg_mono.Build 𝐂
  (fun A B C m g rm_m => Po_along_reg_mono m g rm_m).

HB.instance Definition _ := Cat_is_Rm_Q_Adhesive.Build 𝐂
  (fun A B C D f g iA iB po rm_f =>
    Pre_adh_stb_po po
      (Adhesive_is_Pre_adhesive _ (rm_is_adh _ _ _ rm_f)))
  (fun A B C D f g iA iB po rm_f =>
    Pre_adh_po_pb po
      (Adhesive_is_Pre_adhesive _ (rm_is_adh _ _ _ rm_f))).

HB.instance Definition _ := Rm_Q_Adhesive_is_Rm_Adhesive.Build 𝐂
  rm_subobj_stb_union.

HB.end.

Lemma Adh_stable_union {𝐂: rm_adhesive}: Adh_morph_stable_union 𝐂.
Proof.
  intros A B C Z a b adh_a adh_b c mono_c union.
  apply (Reg_mono_is_adh (f := c)).
  apply (rm_subobj_stb_union _ _ _ _ _ _ (Adh_is_Reg_mono _ adh_a)
    (Adh_is_Reg_mono _ adh_b) _ mono_c union).
Defined.

Lemma Po_rm_VK {𝐂: rm_adhesive} [A B C D: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (rm_f: Reg_mono f)
  (po: Pushout f g iA iB): VanKampen po.
Proof.
  unshelve eapply (Bin_union_closed_VK Adh_stable_union
    Split_mono_adh _ _ _ _ f g iA iB po
    (Reg_mono_is_adh rm_f)).
Defined.