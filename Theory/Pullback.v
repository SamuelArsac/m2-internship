(** * Pullbacks *)

Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Span.
Require Import Cospan.
Require Import Square.
Require Import Terminal.
Require Import Unique.

Local Open Scope cat.

Section Pullback.

(** Definition of pullbacks using the category [Square_cat] *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context (f: A ~> C) (g: B ~> C).
Context (pA: D ~> A) (pB: D ~> B).

Definition Pullback :=
  {pb_comm: f ∘ pA = g ∘ pB &
  @Terminal (Square (Build_cospan f g))
    (exist _ (Build_span pA pB) pb_comm)}.

Definition pb_comm (pb: Pullback) := projT1 pb.
Definition pb_term (pb: Pullback) := projT2 pb.

Definition Build_Pullback' (comm: f ∘ pA = g ∘ pB)
  (term: @Terminal (Square (Build_cospan f g))
  (exist _ (Build_span pA pB) comm)): Pullback
  := existT _ comm term.

Definition Ump_pb :=
  forall E (qA: E ~> A) (qB: E ~> B),
        f ∘ qA = g ∘ qB ->
        ∃! u: E ~> D, qA = pA ∘ u ∧ qB = pB ∘ u.

(** Proof of the equivalence between the categorical definition and
the usual definition *)

Definition Build_term_sq_cat
  (comm: f ∘ pA = g ∘ pB):
  Ump_pb ->
  @Terminal (Square (Build_cospan f g))
      (exist _ (Build_span pA pB) comm).
  intros ump_pb.
  intros [[Y qA qB] comm']; unfold Square_obj in comm';
  simpl in comm';
  pose (u := ump_pb Y qA qB comm').
  unshelve eexists.
  unshelve econstructor; cbn.
  apply u.
  apply ('u).
  trivial.
  simpl.
  intros [v [comm_v1 comm_v2]] tt1.
  cbn in *.
  f_equal.
  assert (eq_vu: v = u).
  apply (Unicity u); auto.
  apply ('u).
  revert comm_v1 comm_v2.
  rewrite eq_vu.
  intros.
  f_equal; apply Prop_irrelevance.
Defined.

Definition Build_Pullback
  (comm: f ∘ pA = g ∘ pB):
  Ump_pb -> Pullback.
  intros ump_pb.
  apply (Build_Pullback' comm).
  exact (Build_term_sq_cat _ ump_pb).
Defined.

Definition Destruct_Pullback:
  Pullback -> f ∘ pA = g ∘ pB ∧ Ump_pb.
  intros [pb_comm term_pb].
  split.
  - assumption.
  - intros Y qA qB comm'.
    pose (span' := Build_span qA qB).
    destruct (@term_one _ _ term_pb (exist _ span' comm'))
    as [v [comm_v1 comm_v2]]; cbn in *.
    exists v.
    + split; assumption.
    + intros w [comm_w1 comm_w2].
    simpl in *.
    pose (span := Build_span pA pB).
    pose (eq := Unicity (term_pb (exist _ span' comm'))
      (Build_Span_hom (v: span' ~>_𝐂 span) comm_v1 comm_v2)
      (Build_Span_hom (w: span' ~>_𝐂 span) comm_w1 comm_w2)
      I I).
    by inversion eq.
Defined.

Definition ump_pb (pb: Pullback): Ump_pb.
  by destruct (Destruct_Pullback pb).
Defined.

End Pullback.

Arguments pb_comm {_} [_ _ _ _ _ _ _ _].
Arguments pb_term {_} [_ _ _ _ _ _ _ _].
Arguments Build_Pullback {_} [_ _ _ _ _ _ _ _].
Arguments Destruct_Pullback {_} [_ _ _ _ _ _ _ _].
Arguments pb_comm {_} [_ _ _ _ _ _ _ _] _.
Arguments ump_pb {_} [_ _ _ _ _ _ _ _] _ [_] _ _ comm'.

Definition Has_pb {𝐂: cat} [A B C: 𝐂] (f: A ~> C) (g: B ~> C)
  := ∃ D (pA: D ~> A) (pB: D ~> B), Pullback f g pA pB.

Section Identity_along_pullback.

Lemma Pb_along_id {𝐂: cat} [A B: 𝐂] (a: A ~> B): Pullback a idmap idmap a.
Proof.
  unshelve eapply Build_Pullback.
  by rewrite comp1o compo1.
  intros Y qA qB comm'; simpl.
  rewrite compo1 in comm'.
  exists qA.
  split; auto.
  by rewrite compo1.
  intros u [comm_u _].
  by rewrite compo1 in comm_u.
Qed.

End Identity_along_pullback.

Section Symmetry.

(** Swapping the two first morphisms with the two others in the arguments *)
Lemma Pb_sym {𝐂: cat} [A B C X: 𝐂]
  [f: A ~> C] [g: B ~> C] [pA: X ~> A] [pB: X ~> B]
  (pb: Pullback f g pA pB): Pullback g f pB pA.
Proof.
  apply Build_Pullback.
  symmetry.
  apply (pb_comm pb).
  intros Y qA qB comm'.
  symmetry in comm'.
  pose proof (u := ump_pb pb qB qA comm').
  exists u.
  split; apply ('u).
  intros v [comm_v1 comm_v2].
  exact ((''u) v (comm_v2, comm_v1)).
Qed.

Lemma Pb_sym_eq {𝐂: cat} [A B C X: 𝐂]
  (f: A ~> C) (g: B ~> C) (pA: X ~> A) (pB: X ~> B):
  Pullback f g pA pB ↔ Pullback g f pB pA.
split; apply Pb_sym.
Qed.

End Symmetry.

HB.mixin Record Cat_has_pb 𝐂 of Cat 𝐂 := {
  Get_pb [A B C: 𝐂] (f: A ~> C) (g: B ~> C): Has_pb f g
}.

Unset Universe Checking.
#[short(type="cat_pb")]
HB.structure Definition Cat_pb: Set :=
  { 𝐂 of Cat_has_pb 𝐂 &}.
Set Universe Checking.

Arguments Get_pb {_} [_ _ _].
