Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.

Local Open Scope cat.

Definition VanKampen {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB) :=
  ∀ A' B' C' D' (f': C' ~> A') (g': C' ~> B') (iA': A' ~> D') (iB': B' ~> D')
  (α: A' ~> A) (β: B' ~> B) (χ: C' ~> C) (δ: D' ~> D),
  iA' ∘ f' = iB' ∘ g' -> δ ∘ iA' = iA ∘ α -> δ ∘ iB' = iB ∘ β ->
  Pullback f α χ f' -> Pullback g β χ g' ->
  (Pullback iA δ α iA' ∧ Pullback iB δ β iB' ↔ Pushout f' g' iA' iB'). 

Lemma VK_stable {𝐂: cat}: forall [A B C D: 𝐂]
  [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
  (po: Pushout f g iA iB), VanKampen po -> Stable_po po.
Proof.
  intros A B C D f g iA iB po VK.
  unfold Stable_po.
  intros.
  apply Pb_sym in pb1, pb2, pb3, pb4.
  destruct (VK _ _ _ _ f' g' iA' iB' α β χ δ); auto;
    by apply pb_comm; apply Pb_sym.
Qed.

Lemma VK_mono_stable_po_and_pb: forall {𝐂: cat} [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (mono_f: Mono f),
  VanKampen po -> Mono iB ∧ Pullback iA iB f g.
Proof.
  intros ? ? ? ? ? ? ? ? ? ? ? VK.
  destruct (VK C B C B idmap g g idmap f idmap idmap iB) as [_ VK2];
    try rewrite compo1; try rewrite comp1o; auto.
  - symmetry. apply (po_comm po).
  - by apply Mono_trivial_pb.
  - apply Pb_along_id.
  - destruct VK2.
    apply Po_sym.
    apply Po_along_id.
  by split; [apply Mono_trivial_pb|].
Qed.
  
Lemma VK_mono_stable_po: forall {𝐂: cat} [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (mono_f: Mono f),
   VanKampen po -> Mono iB.
Proof.
  intros ? ? ? ? ? ? ? ? ? ? ? VK.
  by destruct (VK_mono_stable_po_and_pb f g iA iB po
    mono_f VK).
Qed.

Lemma VK_mono_pb: forall {𝐂: cat} [A B C D: 𝐂] (f: C ~> A) (g: C ~> B)
  (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB) (mono_f: Mono f),
  VanKampen po -> Pullback iA iB f g.
Proof.
  intros ? ? ? ? ? ? ? ? ? ? ? VK.
  by destruct (VK_mono_stable_po_and_pb f g iA iB po
    mono_f VK).
Qed.