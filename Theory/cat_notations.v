Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Export Coq.Classes.CMorphisms.

Notation "f ∘ g" := (comp g f) (at level 40): cat_scope. 

Notation "f ∘[ C ] g" := (@comp C _ _ _ g f)
  (at level 40, only parsing) : cat_scope.

Notation "∃ x .. y , p" := (sigT (fun x => .. (sigT (fun y => p)) ..))
  (at level 200, x binder, right associativity).

Notation "∀ x .. y , P" := (forall x, .. (forall y, P) ..)
  (at level 200, x binder, y binder, right associativity).

Notation "x → y" := (x -> y)
  (at level 99, y at level 200, right associativity).
Notation "x ↔ y" := (iffT x y)
  (at level 95, no associativity).
Notation "¬ x" := (x → False)
  (at level 75, right associativity).
Notation "x ≠ y" := (x <> y) (at level 70).

Notation "`` x" := (@projT1 _ _ x) (at level 0).
Notation "( x ; y )" := (existT _ x y) (at level 0).

Notation "`1 x" := (@projT1 _ _ x) (at level 0).
Notation "`2 x" := (@projT2 _ _ x) (at level 0).
Notation "`3 x" := (@projT3 _ _ x) (at level 0).

Infix "∧" := Datatypes.prod (at level 80, right associativity).
Infix "∨" := sum (at level 85, right associativity).

Notation "'witness' x" := (exist _ x _) (at level 200).