From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Utils.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Product.
Require Import Cokernel_pair.
Require Import Split_mono.
Require Import Adhesive_morphisms.

Local Open Scope cat.

HB.mixin Record Cat_is_Rm_Q_Adhesive 𝐂 of Cat_pb 𝐂 & Cat_po_along_reg_mono 𝐂 := {
  stable_po [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
    Reg_mono f -> Stable_po po;
  po_is_pb [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]: Pushout f g iA iB ->
    Reg_mono f -> Pullback iA iB f g 
}.
Unset Universe Checking.
#[short(type="rm_q_adhesive")]
HB.structure Definition Rm_Q_Adhesive: Set :=
  { 𝐂 of Cat_is_Rm_Q_Adhesive 𝐂 & }.
Set Universe Checking.

Arguments stable_po {_} [_ _ _ _ _ _ _ _] _ rm_f [_ _ _ _ _ _ _ _ _ _ _ _]: rename.
Arguments po_is_pb {_} [_ _ _ _ _ _ _ _] po rm_f: rename.

Section Rm_q_adhesive_lemmas.

Context {𝐂: rm_q_adhesive}.

Lemma Reg_mono_is_adh [A C: 𝐂] [f: A ~> C]:
  Reg_mono f -> Adhesive_morph f.
Proof.
  intros rm_m.
  intros B D g pA pB pb E e.
  pose (rm_pB := Reg_mono_stable_PB _ _ _ _ pb rm_m).
  destruct (po_along_reg_mono pB e rm_pB) as [F [f1 [f2 po]]].
  simpl in F.
  exists F.
  exists f1.
  exists f2.
  exists po.
  split.
  by apply stable_po.
  by apply po_is_pb.
Qed.

Lemma Split_mono_adh: Split_mono_adhesive 𝐂.
Proof.
  intros A B m split_m.
  apply (Reg_mono_is_adh (Split_mono_reg _ split_m)).
Defined.

Lemma Reg_mono_comp [A B C: 𝐂]
  [m: B ~> C] [n: A ~> B]:
  Reg_mono m -> Reg_mono n -> Reg_mono (m ∘ n).
Proof.
  intros rm_m rm_n.
  apply Reg_mono_is_adh in rm_m, rm_n.
  apply (Adh_is_Reg_mono (m ∘ n)).
  apply (Adhesive_comp rm_m rm_n).
Qed.

(* Lack & Sobocinski - Adhesive and quasiadhesive categories - 2005
Theorem 5.1 *)
Lemma reg_subobj_union_is_subobj:
  forall [I A B C Z: 𝐂] [a: A ~> Z] [b: B ~> Z]
    (rm_a: Reg_mono a) (rm_b: Reg_mono b)
    [p: I ~> A] [u: A ~> C] [q: I ~> B] [v: B ~> C]
    (pb: Pullback a b p q) (po: Pushout p q u v)
    (c: C ~> Z) (eq_c1: c ∘ u = a) (eq_c2: c ∘ v = b), Mono c.
  Proof.
  intros.
  intros K f g eq_fg.

  pose proof (rm_q := Reg_mono_stable_PB _ _ _ _ pb rm_a).
  apply Pb_sym in pb.
  pose proof (rm_p := Reg_mono_stable_PB _ _ _ _ pb rm_b).

  destruct (Get_cube p q u v f (po_comm po))
    as [L [L1 [L2 [f' [f1 [f2 [l1' [l2' [l1 [l2
      [pb_f1 [pb_f2 [pb_f3 [pb_f4 comm_f]]]]]]]]]]]]]].
  pose proof (po_f := stable_po po rm_p pb_f1 pb_f3
    pb_f2 pb_f4 comm_f).
  pose proof (joint_epi_l1_l2 := Pushout_joint_epi po_f).

  destruct (Get_cube p q u v g (po_comm po))
    as [M [M1 [M2 [g' [g1 [g2 [m1' [m2' [m1 [m2
      [pb_g1 [pb_g2 [pb_g3 [pb_g4 comm_g]]]]]]]]]]]]]].
  pose proof (po_g := stable_po po rm_p pb_g1 pb_g3
    pb_g2 pb_g4 comm_g).
  pose proof (joint_epi_m1_m2 := Pushout_joint_epi po_g).

  destruct (Get_cube l1' l2' l1 l2 m1 (po_comm po_f))
    as [N [N11 [N12 [l' [l11 [l12 [m11' [m12' [m11 [m12 
      [pb_m11 [pb_m12 [pb_m13 [pb_m14 comm_m]]]]]]]]]]]]]].
  apply Pb_sym in pb_f1.
  pose proof (rm_l1' := Reg_mono_stable_PB _ _ _ _ pb_f1 rm_p). 
  pose proof (po_m := stable_po po_f rm_l1' pb_m11 pb_m13 pb_m12 pb_m14 comm_m).
  pose proof (joint_epi_m11_m12 := Pushout_joint_epi po_m).

  destruct (Get_cube l1' l2' l1 l2 m2 (po_comm po_f))
    as [N' [N21 [N22 [l'' [l21 [l22 [m21' [m22' [m21 [m22 
      [pb_m21 [pb_m22 [pb_m23 [pb_m24 comm_m']]]]]]]]]]]]]].
  pose proof (po_m' := stable_po po_f rm_l1' pb_m21 pb_m23 pb_m22 pb_m24 comm_m').
  pose proof (joint_epi_m21_m22 := Pushout_joint_epi po_m').

  apply joint_epi_m1_m2.

  { apply joint_epi_m11_m12.

  { assert (a ∘ (f1 ∘ l11) = a ∘ (g1 ∘ m11)).
    by rewrite -eq_c1 compoA -(compoA l11) -(pb_comm pb_f2) -2!compoA
      eq_fg compoA -(pb_comm pb_m12) compoA -(compoA m11) (pb_comm pb_g2)
      !compoA.
    apply (Reg_mono_is_mono _ rm_a) in H.
    by rewrite compoA (pb_comm pb_m12) -compoA (pb_comm pb_f2) compoA H
      -compoA -(pb_comm pb_g2).
  }

  unshelve epose (h := ump_pb pb (f2 ∘ l12) (g1 ∘ m12) _).
  by rewrite -eq_c2 -compoA (compoA f2) -(pb_comm pb_f4) -compoA eq_fg compoA
    -(pb_comm pb_m14) -compoA (compoA m1) (pb_comm pb_g2) -compoA eq_c1 compoA.
  by rewrite compoA (pb_comm pb_m14) -compoA (pb_comm pb_f4) compoA ('h) -compoA
    -(po_comm po) compoA -('h) -compoA -(pb_comm pb_g2).
  }

  apply joint_epi_m21_m22.

  { unshelve epose (h := ump_pb pb (g2 ∘ m21) (f1 ∘ l21) _).
  by rewrite -eq_c2 -compoA (compoA g2) -(pb_comm pb_g4) -compoA -eq_fg compoA
    (pb_comm pb_m22) -compoA (compoA l1) (pb_comm pb_f2) -compoA eq_c1 compoA.
  by rewrite compoA (pb_comm pb_m22) -compoA (pb_comm pb_f2) compoA ('h) -compoA
    (po_comm po) compoA -('h) -compoA -(pb_comm pb_g4).
  } 
  assert (b ∘ (f2 ∘ l22) = b ∘ (g2 ∘ m22)).
  by rewrite -eq_c2 compoA -(compoA l22) -(pb_comm pb_f4) -2!compoA
    eq_fg compoA -(pb_comm pb_m24) compoA -(compoA m22) (pb_comm pb_g4)
    !compoA.
  apply (Reg_mono_is_mono _ rm_b) in H.
  by rewrite compoA (pb_comm pb_m24) -compoA (pb_comm pb_f4) compoA H
    -compoA -(pb_comm pb_g4).
Qed.

(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
4.4 Proposition. If m : A → X is the union of regular subobjects
m1 : A1 → X and m2 : A2 → X in an rm-quasiadhesive category,
then m has a stable (epi,regular mono) factorization. *)
Lemma reg_union_epi_rm_fact_aux:
  forall [X A A0 A1 A2: 𝐂] (m1': A0 ~> A2) (n2: A2 ~> A) (m2': A0 ~> A1) (n1: A1 ~> A)
    (m1: A1 ~> X) (m2: A2 ~> X) (m: A ~> X) (rm_m1: Reg_mono m1) (rm_m2: Reg_mono m2)
    (eq_m1: m1 = m ∘ n1) (eq_m2: m2 = m ∘ n2) (pb_A0: Pullback m2 m1 m1' m2')
    (po_A: Pushout m1' m2' n2 n1),
    ∃ B Y X1 (n: B ~> X) (e: A ~> B) (i: X ~> X1) (j: X ~> X1)
      (q: X1 ~> Y), m = n ∘ e ∧ Reg_mono n ∧ Pushout m m (q ∘ i) (q ∘ j)
      ∧ Pushout n n (q ∘ i) (q ∘ j) ∧ Equalizer (q ∘ i) (q ∘ j) n.
Proof.
  intros.
  epose proof (mono_m := reg_subobj_union_is_subobj rm_m2 rm_m1
    pb_A0 po_A m (eq_sym eq_m2) (eq_sym eq_m1)).

  (* First let i, j : X ⇉ X1 be the cokernel pair of m1,
  and e1 : X1 → X the codiagonal. *)
  destruct (Get_cokernel_pair rm_m1) as [X1 [i [j po_X1]]].
  pose (e1 := ump_po po_X1 idmap idmap eq_refl).

  (* We can pull all these maps back along m2 to get the diagram
  https://q.uiver.app/#q=WzAsOCxbMCwwLCJBXzAiXSxbMCwxLCJBXzEiXSxbMSwxLCJYIl0sWzEsMCwiQV8yIl0sWzIsMSwiWF8xIl0sWzIsMCwiWF8yIl0sWzMsMSwiWCJdLFszLDAsIkFfMiJdLFswLDMsIm1fMSciXSxbMCwxLCJtXzInIiwyXSxbMSwyLCJtXzEiXSxbMywyLCJtXzIiLDJdLFszLDUsImlfMiIsMCx7Im9mZnNldCI6LTF9XSxbMyw1LCJqXzIiLDIseyJvZmZzZXQiOjF9XSxbMiw0LCJpIiwwLHsib2Zmc2V0IjotMX1dLFsyLDQsImoiLDIseyJvZmZzZXQiOjF9XSxbNSw0LCJsIiwyXSxbNSw3LCJlXzIiXSxbNyw2LCJtXzIiXSxbNCw2LCJlXzEiXV0=
  *)
  destruct (Get_pb m2 e1) as [X2 [e2 [l pb_X2]]].
  simpl in X2.

  unshelve epose proof (i2 := ump_pb pb_X2 idmap (i ∘ m2) _).
  by rewrite comp1o -compoA -('e1) compo1.
  assert (pb_A2_id1: Pullback m2 (e1 ∘ i) (e2 ∘ i2) m2).
  rewrite -('e1) -('i2).
  apply Pb_along_id.
  unshelve epose proof (pb_A21 := Pb_decomp _ pb_X2 pb_A2_id1).
  by rewrite -('i2).

  unshelve epose proof (j2 := ump_pb pb_X2 idmap (j ∘ m2) _).
  by rewrite comp1o -compoA -('e1) compo1.
  assert (pb_A2_id2: Pullback m2 (e1 ∘ j) (e2 ∘ j2) m2).
  rewrite -('e1) -('j2).
  apply Pb_along_id.
  unshelve epose proof (pb_A22 := Pb_decomp _ pb_X2 pb_A2_id2).
  by rewrite -('j2).

  (* in which l is a pullback of the regular monomorphism m2
  and so is itself a regular monomorphism. *)
  pose proof (rm_l := Reg_mono_stable_PB _ _ _ _ pb_X2 rm_m2).

  (* Since cokernel pairs of regular monomorphisms are
  pushouts along a regular monomorphism, they are stable under pullback,
  and so i2, j2 : A2 ⇉ X2 is the cokernel pair of m1',
  and e2 : X2 → A2 is the codiagonal. *)
  unshelve epose proof (po_X2 := stable_po po_X1 rm_m1 pb_A0 pb_A0 pb_A21 pb_A22 _).
  apply (Reg_mono_is_mono _ rm_l).
  by rewrite -2!compoA -('i2) -('j2) 2!compoA (pb_comm pb_A0)
  -2!compoA (po_comm po_X1).

  (* Since l is a regular monomorphism, we can form the pushout
  https://q.uiver.app/#q=WzAsNCxbMCwwLCJYXzIiXSxbMSwwLCJBXzIiXSxbMCwxLCJYXzEiXSxbMSwxLCJZIl0sWzAsMiwibCIsMl0sWzAsMSwiZV8yIl0sWzEsMywiayJdLFsyLDMsInEiLDJdXQ==
  *)
  destruct (po_along_reg_mono l e2 rm_l) as [Y [q [k po_Y]]].
  simpl in Y.

  (* We shall see that the maps qi, qj : X → Y
  are the cokernel pair of the union m : A → X. *)
  assert (cokern_m: Pushout m m (q ∘ i) (q ∘ j)).
  apply Build_Pushout.
  apply (Pushout_joint_epi po_A).
  by rewrite 4!compoA -eq_m2 -(pb_comm pb_A21) -(pb_comm pb_A22)
   -2! compoA (po_comm po_Y) 2!compoA -('i2) -('j2).
  by rewrite 4!compoA -eq_m1 (po_comm po_X1).
  (* To do this, suppose that u, v : X → Z are given with um = vm,
  or equivalently with um1 = vm1 and um2 = vm2. *)
  intros Z u v comm_uvm.
  (* Since um1 = vm1, there is a unique w : X1 → Z
  with wi = u and wj = v. *)
  unshelve epose (w := ump_po po_X1 u v _).
  by rewrite eq_m1 -2!compoA comm_uvm.

  (* On the other hand wli2 = wim2 = um2 = vm2 = wjm2 = wlj2
  and so the morphism wl out of the cokernel pair X2 of m1' agrees
  on the coprojections i2 and j2 of the cokernel pair,
  and therefore factorizes through the codiagonal e2,
  say as wl = w'e2. *)
  destruct (Codiagonal_fact po_X2 (fst ('i2)) (fst ('j2)) (w ∘ l)) as [w' eq_w'].
  by rewrite 2!compoA (pb_comm pb_A21) (pb_comm pb_A22) -2!compoA
  -2!('w) eq_m2 -2!compoA comm_uvm.

  (* By the universal property of the pushout Y,
  there is a unique w′'': Y → Z with w''q = w and w''k = w'. *)
  pose (w'' := ump_po po_Y w w' eq_w').

  (* Now w''qi = wi = u and w''qj = wj = v,
  and it is easy to see that w'' is unique with the property,
  thus proving that qi and qj give the cokernel pair of m. *)
  exists (w'' :> Y ~> Z).
  split; rewrite -compoA -('w''); apply ('w).
  intros w''' [eq_w'''1 eq_w'''2].
  apply (''w'').
  split.
  by apply (Pushout_joint_epi po_X1); rewrite compoA -('w).
  assert (w ∘ l ∘ i2 = w' ∘ e2 ∘ i2).
  by rewrite eq_w'.
  rewrite !compoA -(fst ('i2)) comp1o in H.
  by rewrite -H (pb_comm pb_A21) -compoA -('w) eq_w'''1 !compoA -(pb_comm pb_A21) -(compoA i2)
    (po_comm po_Y) compoA -('i2) comp1o.

  (* Since qi and qj are a cokernel pair,
  they have a common retraction (the codiagonal),
  and so we can form their equalizer
  n : B → X as their intersection;
  this is of course a regular monomorphism. *)
  destruct (Cokernel_equalizer cokern_m) as [B [n eq_n]]. (*[eq_n equalizer_n]]].*)

  exists B.
  exists Y.
  exists X1.
  exists n.

  (* Since qim = qjm, there is a unique map e : A → B with ne = m; *)
  pose proof (e := Eq_prop _ m (po_comm cokern_m)).

  exists e.
  exists i.
  exists j.
  exists q.
  split.
  apply ('e).
  split.
  eexists.
  exists (q ∘ i).
  exists (q ∘ j).
  assumption.
  split.
  assumption.
  split.
  rewrite ('e) in cokern_m.
  eapply (Cokern_fact cokern_m).
  by apply Eq_comp.
  assumption.
Qed.

Lemma reg_union_epi_rm_fact:
  forall [X A A0 A1 A2: 𝐂] (m1': A0 ~> A2) (n2: A2 ~> A) (m2': A0 ~> A1) (n1: A1 ~> A)
    (m1: A1 ~> X) (m2: A2 ~> X) (m: A ~> X) (rm_m1: Reg_mono m1) (rm_m2: Reg_mono m2)
    (eq_m1: m1 = m ∘ n1) (eq_m2: m2 = m ∘ n2) (pb_A0: Pullback m2 m1 m1' m2') (po_A: Pushout m1' m2' n2 n1),
    ∃ B (n: B ~> X) (e: A ~> B), m = n ∘ e ∧ Reg_mono n ∧ Epi e.
Proof.
  intros.
  destruct (reg_union_epi_rm_fact_aux m1' n2 m2' n1 m1 m2 m rm_m1 rm_m2 eq_m1 eq_m2 pb_A0 po_A)
    as [B [Y [X1 [n [e [i [j [q [eq_ne [rm_n [cokern_m [cokern_n equalizer_n]]]]]]]]]]]].
  exists B.
  exists n.
  exists e.
  split.
  assumption.
  split.
  assumption.

  (* we must show that e is a stable epimorphism.
  In fact it will suffice to show that it is an epimorphism,
  since all these constructions are stable under
  pullback along a map into X, thus if e is an epimorphism
  so will be any pullback. *)
  (* To see that e is an epimorphism, first note that m1 and m2
  factorize as mi = m(ni) = ne(ni), and now e(n1) and e(n2) are regular
  monomorphisms with union e. So once again we can factorize e
  as n'e', where n' is a regular monomorphism constructed as before. *)
  unshelve epose proof (s := reg_union_epi_rm_fact_aux _ _ _ _
    (e ∘ n1) (e ∘ n2) e _ _ eq_refl eq_refl _ po_A).

  { eapply (Reg_mono_decomp (e ∘ n1) n).
  by rewrite -compoA -eq_ne -eq_m1.
  apply (Reg_mono_is_mono _ rm_n). }

  { eapply (Reg_mono_decomp (e ∘ n2) n).
  by rewrite -compoA -eq_ne -eq_m2.
  apply (Reg_mono_is_mono _ rm_n). }

  { rewrite eq_m1 eq_m2 eq_ne in pb_A0.
  rewrite !compoA in pb_A0.
  apply (Pullback_mono (Reg_mono_is_mono _ rm_n) pb_A0). }

  {
    destruct s as [B' [Y' [X1' [n' [e' [i' [j' [q' [eq_e' [rm_n' [cokern_e [cokern_n' equalizer_n']]]]]]]]]]]].
    (* Observe that e is an epimorphism if and only if
    its cokernel pair is trivial, *)
    apply (Epi_cokern e).
    (* which in turn is equivalent to the equalizer n'
    of this cokernel pair being invertible. *)
    apply (Eq_cokern_invert equalizer_n' cokern_e).

    (* In an rm-adhesive category the regular monomorphisms are precisely
    the adhesive morphisms, and so are closed under composition.
    Thus the composite nn′ of the regular monomorphisms n and n′
    is itself a regular monomorphism. *)
    pose proof (rm_nn' := Reg_mono_comp rm_n rm_n').
    
    (* We conclude, using a standard argument,
    by showing that n' is invertible and so that e is an epimorphism. *)

    (* Observe that qi and qj are the cokernel pair of m = ne = nn'e',
    as well as of their equalizer n. But then they are also certainly
    the cokernel pair of nn'. *)
    assert (cokern_nn': Pushout (n ∘ n') (n ∘ n') (q ∘ i) (q ∘ j)).
    rewrite eq_ne eq_e' in cokern_m.
    apply Build_Pushout.
    by rewrite -compoA Eq_comp compoA.
    intros Y0 jA jB H.
    unshelve epose proof (ump_po cokern_m jA jB _).
    by rewrite -2!(compoA e') H !compoA. 
    assumption.

    (* Since this last is a regular monomorphism,
    it must be the equalizer of qi and qj, *)
    pose (equalizer_nn' := Pb_equalizer
      (po_is_pb cokern_nn' rm_nn')).
    (* but then it must be (isomorphic to) n, so that n' is
    invertible as claimed. *)
    pose (Eq_prop equalizer_nn' n (Eq_comp equalizer_n)).
    exists u.
    pose (Eq_prop equalizer_n n (Eq_comp equalizer_n)).
    apply (Unicity u0).
    rewrite -compoA.
    apply ('u).
    by rewrite comp1o.
    }
Qed.

End Rm_q_adhesive_lemmas.