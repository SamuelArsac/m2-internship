Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Coq.Logic.PropExtensionality.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.Description.
Require Import Utils.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Unique.
Require Import Morphism.
Require Import Isomorphism.
Require Import Quotients.
Require Import RelationClasses.
Require Import Relation_Definitions.
Require Import Relation_Operators.
Require Import Reg_mono.
Require Import Rm_Quasi_Adhesive.
Require Import Rm_Adhesive.
Require Import Adhesive.
Require Import Sets_pb.
Require Import Sets.
Require Import Quotients.

Local Open Scope cat.

Definition propext := propositional_extensionality.

Lemma fold_comp: forall [A B C: Sets] (f: B ~> C) (g: A ~> B) x, f (g x) = (f ∘ g) x.
Proof.
  reflexivity.
Qed.

Section Sets_po.

Definition injective [A B: Sets] (f: A ~> B) :=
  forall x y, f x = f y -> x = y.

Lemma Sets_mono [A B: Sets] [f: A ~> B] (mono_f: Mono f):
  injective f.
Proof.
  intros x y eq.
  unfold Mono in mono_f.
  pose (funx := fun _: True => x).
  pose (funy := fun _: True => y).
  unshelve eapply (equal_f (mono_f _ funx funy _) I).
  apply funext.
  intro.
  assumption.
Qed.

Context [A B C: Sets].
Context (f: C ~> A) (g: C ~> B).
Context (inj_f: injective f).

Lemma cdd_unique P: (forall a: A, exists! b: B, P a b)
  -> ∃! u: A ~> B, forall a, P a (u a).
move=> H.
have: forall a: A, {b: B | P a b}.
move=> a. 
by apply: constructive_definite_description.
move=> H'.
exists (fun a => proj1_sig (H' a)).
move=> a.
by case: (H' a).
move=> v Pv.
apply: funext => a.
case: (H a) => b [_ eq_b].
have: b = proj1_sig (H' a) => [|<-];
apply: eq_b.
apply: proj2_sig (H' a).
done.
Qed.

Lemma cdd P: (forall a: A, exists! b: B, P a b)
  -> {u: A ~> B | forall a, P a (u a)}.
Proof.
  move/cdd_unique => [u Pu _].
  by exists u.
Qed.

Definition Pre_sets_po_obj: Sets := (A + B)%type.

Inductive Sets_po_relation: relation Pre_sets_po_obj :=
 | sets_po_rel: forall c, Sets_po_relation (inl (f c)) (inr (g c)). 

Definition Sets_po_relation_fun: relation Pre_sets_po_obj :=
  fun x y => match x, y with
  | inl a, inr b => exists c, f c = a /\ g c = b
  | _, _ => False
  end.

Lemma Sets_po_relation_eq: forall x y: Pre_sets_po_obj,
  Sets_po_relation x y <-> Sets_po_relation_fun x y.
Proof.
  move=> x y.
  split.
  case=> c.
  by exists c.
  move: x y.
  case=> a; case=> b // [c [<- <-]].
  apply: sets_po_rel.
Qed.

Definition Sets_po_equiv: relation Pre_sets_po_obj :=
  clos_refl_sym_trans _ Sets_po_relation.

Lemma Spe_is_equiv: Equivalence Sets_po_equiv.
Proof.
  constructor.
  apply: rst_refl.
  apply: rst_sym.
  apply: rst_trans.
Qed.

Definition Sets_po_obj: Sets := quot Sets_po_equiv.

Definition Sets_po_coproj1: A ~> Sets_po_obj :=
  fun a => Quot _ (inl a).
Definition Sets_po_coproj2: B ~> Sets_po_obj :=
  fun b => Quot _ (inr b).

Definition SPo_unique_morph_aux D' (jA: A ~> D') (jB: B ~> D'):
  Pre_sets_po_obj ~> D' :=
  fun d => match d with
  | inl a => jA a
  | inr b => jB b
  end.

Lemma SPo_unique_morphP D' (jA: A ~> D') (jB: B ~> D'):
  jA ∘ f = jB ∘ g ->
  ∀ (x y: Pre_sets_po_obj), Sets_po_equiv x y →
  SPo_unique_morph_aux D' jA jB x
  = SPo_unique_morph_aux D' jA jB y.
Proof.
  move=> comm x y.
  elim=> [x' y' Rx'y' | x' | x' y' Rxy IHRxy
    | x' y' z' Rxy1 IHRxy1 Rxy2 IHRxy2] //.
  elim: Rx'y' => c /=.
  apply: (equal_f comm).
  by rewrite IHRxy1.
Qed.

Definition SPo_unique_morph D' (jA: A ~> D') (jB: B ~> D'):
  jA ∘ f = jB ∘ g -> Sets_po_obj ~> D' :=
  (fun comm d => quot_rec Spe_is_equiv
    (SPo_unique_morphP D' jA jB comm) d).

Lemma Sets_po: Pushout f g Sets_po_coproj1 Sets_po_coproj2.
Proof.
  apply: Build_Pushout.
  apply: funext => c.
  apply: eq_Quot.
  apply: Spe_is_equiv.
  apply: rst_step.
  apply: sets_po_rel.

  move=> E jA jB comm.
  exists (SPo_unique_morph _ _ _ comm).
  have e := quot_recE Spe_is_equiv
    (SPo_unique_morphP E jA jB comm).
  split; apply: funext => a;
  rewrite /SPo_unique_morph /comp /=.
  by rewrite (e (inl a)).
  by rewrite (e (inr a)).

  move=> v [eq_v1 eq_v2].
  apply: funext => d.
  have:= Quot_inv d.
  case=> [d' ?].
  subst.
  rewrite /SPo_unique_morph /=.
  have:= quot_recE Spe_is_equiv
    (SPo_unique_morphP E (v ∘ Sets_po_coproj1) (v ∘ Sets_po_coproj2) comm) d'
    => ->.
  by case: d'.
Defined.

End Sets_po.

Definition fun_sum [A B C: Type] (f: A -> C) (g: B -> C) (x: A + B) :=
  match x with inl a => f a | inr b => g b end.

Section stb_po.

Variable A B C D': Sets.
Variable (f: C ~> A) (g: C ~> B).
Let D := Sets_po_obj f g.
Let iA := Sets_po_coproj1 f g.
Let iB := Sets_po_coproj2 f g.
Let po := Sets_po f g : Pushout f g iA iB.
Variable δ: D' ~> D.
Let pb_span_fl := Get_pb iA δ.
Let pb_span_fr := Get_pb iB δ.
Let pb_span_mid := Get_pb (iA ∘ f) δ.
Let C' := projT1 pb_span_mid : Sets.
Let A' := projT1 pb_span_fl.
Let B' := projT1 pb_span_fr.
Let iA' := projT1 (projT2 (projT2 pb_span_fl)): A' ~> D'.
Let α := projT1 (projT2 pb_span_fl): A' ~> A.
Let pb_fl := projT2 (projT2 (projT2 pb_span_fl)).
Let iB' := projT1 (projT2 (projT2 pb_span_fr)): B' ~> D'.
Let β := projT1 (projT2 pb_span_fr): B' ~> B.
Let pb_fr := projT2 (projT2 (projT2 pb_span_fr)).
Let iAf' := projT1 (projT2 (projT2 pb_span_mid)): C' ~> D'.
Let χ := projT1 (projT2 pb_span_mid): C' ~> C.
Let pb_mid := projT2 (projT2 (projT2 pb_span_mid)).
Let f' := ump_pb pb_fl (f ∘ χ) iAf' (pb_comm pb_mid).
Lemma eq_g': iB ∘ (g ∘ χ) = δ ∘ iAf'.
  Proof. by rewrite -(pb_comm pb_mid) -compoA -(po_comm po).
Defined.
Let g' := ump_pb pb_fr (g ∘ χ) iAf' eq_g'.

Definition iX := fun_sum iA iB.

Definition make_sumA'B' (x: A + B) (d': D') (eq:
  iX x = δ d'): A' + B'.
destruct x.
apply (inl (exist _ (a, d') eq)).
apply (inr (exist _ (b, d') eq)).
Defined.

Lemma Sets_stb_po_eq_equiv: forall (x y: A + B) (d'1 d'2: D')
  (eq_x: iX x = δ d'1) (eq_y: iX y = δ d'2), d'1 = d'2 ->
  Sets_po_equiv f g x y -> Sets_po_equiv f' g' (make_sumA'B' x d'1 eq_x)
    (make_sumA'B' y d'2 eq_y).
Proof.
  move=> x y d'1 d'2 eq_x eq_y eq Relxy.
  elim: Relxy eq_x eq_y =>
    [x' y'
    |x'
    |x' y' Relxy IHRelxy
    |x' y' z' Relxy1 IHRelxy1 Relxy2 IHRelxy2].

  elim=> c eq1 eq2.
  apply: rst_step.
  apply/Sets_po_relation_eq=> /=.
  exists (exist _ (c, d'1) eq1).
  split; apply/sig_eqP=> //.
  by rewrite -eq.

  move=> eq_x eq_y.
  move: eq_y.
  rewrite -eq=> eq_y.
  have: eq_x = eq_y=> [|->].
  by apply: Prop_irrelevance.
  apply: rst_refl.

  move=> eq_x eq_y.
  apply: rst_sym.
  move: eq_y IHRelxy.
  rewrite -eq=> eq_y.
  apply.

  move=> eq_x eq_y.
  move: eq_y IHRelxy1 IHRelxy2.
  rewrite -eq=> eq_y IHRelxy1 IHRelxy2.
  apply: rst_trans _ _ _ _ _ (IHRelxy1 _ _) (IHRelxy2 _ _).
  rewrite -{}eq_x.
  case: x' Relxy1 IHRelxy1=> [a |b ];
  case: y' Relxy2 IHRelxy2=> [a' Relxy2 IHRelxy2 Relxy1 IHRelxy1
    |b' Relxy2 IHRelxy2 Relxy1 IHRelxy1] /=;
  apply: (eq_Quot (Spe_is_equiv f g));
  by apply: rst_sym.
Qed.

Lemma Sets_stable_po_aux : Pushout f' g' iA' iB'.
Proof.
  pose (D'' := Sets_po_obj f' g').
  pose (iA'' := Sets_po_coproj1 f' g').
  pose (iB'' := Sets_po_coproj2 f' g').
  fold D'' in iA'', iB''.
  pose (po' := Sets_po f' g').
  fold iA'' iB'' in po'.
  suff: ∃ iso: D'' ≅ D', iso¹ ∘ iA'' =  iA' ∧ iso¹ ∘ iB'' = iB'.
    move=> [iso [eq_iso1 eq_iso2]].
    move: po'.
    rewrite -eq_iso1 -eq_iso2.
    by move/(fst (Pushout_iso_D' iso)).
  have comm': iA' ∘ f' = iB' ∘ g'.
    by rewrite -('f') -('g').
  have [u [eq_u1 eq_u2] unique_u]:= ump_po po' iA' iB' comm'.
  suff iso_u: IsIso u.
    promote_iso u.
    by exists u.
  pose P := fun x y => x = u y /\ forall z: D'', x = u z -> y = z.
  suff: {u' : D' ~> D'' | ∀ a : D', P a (u' a)}.
    move=> [u' Pu'].
    exists u'.
    split; apply funext=> d;
    rewrite /comp /=.
      by have:= Pu' d => - [<- _].
    have:= Pu' (u d) => - [_ +].
    by apply.
  apply: cdd=> d'.
  have [[a | b] eq_ab]:= Quot_inv (δ d').
    exists (iA'' (exist _ (a, d') (eq_sym eq_ab))).
    split.
      split.
        by rewrite fold_comp -eq_u1.
      move=> d'' eq_d''.
      subst.
      rewrite /iA'' /Sets_po_coproj1 /=.
      have [[a'|b'] ?]:= Quot_inv d''; subst.
        apply: (eq_Quot (Spe_is_equiv f' g')).
        move: eq_ab.
        rewrite -/(Sets_po_coproj1 f' g' a') -/iA''.
        move: a'=> [[a2 d'2] /= eq_a2] eq_ab.
        have: u (iA'' (exist _ (a2, d'2) eq_a2)) = d'2.
          by rewrite fold_comp -eq_u1.
        move=> eq_d'2.
        apply: (Sets_stb_po_eq_equiv (inl a) (inl a2))=> //.
        apply: (Quot_inj (Spe_is_equiv f g)).
        by rewrite -eq_ab eq_d'2 -eq_a2.
      apply: (eq_Quot (Spe_is_equiv f' g')).
      move: eq_ab.
      rewrite -/(Sets_po_coproj2 f' g' b') -/iB''.
      move: b'=> [[b2 d'2] /= eq_b2] eq_ab.
      have: u (iB'' (exist _ (b2, d'2) eq_b2)) = d'2.
        by rewrite fold_comp -eq_u2.
      move=> eq_d'2.
      apply: (Sets_stb_po_eq_equiv (inl a) (inr b2))=> //.
      apply: (Quot_inj (Spe_is_equiv f g)).
      by rewrite -eq_ab eq_d'2 -eq_b2.
    move=> d'' [_ eq_d''].
    rewrite -(eq_d'' (iA'' _))=> //.
    by rewrite fold_comp -eq_u1.
  exists (iB'' (exist _ (b, d') (eq_sym eq_ab))).
    split.
      split.
        by rewrite fold_comp -eq_u2.
      move=> d'' eq_d''.
      subst.
      have [[a'|b'] ?]:= Quot_inv d''; subst.
        apply: (eq_Quot (Spe_is_equiv f' g')).
        move: eq_ab.
        rewrite -/(Sets_po_coproj1 f' g' a') -/iA''.
        move: a'=> [[a2 d'2] /= eq_a2] eq_ab.
        have: u (iA'' (exist _ (a2, d'2) eq_a2)) = d'2.
          by rewrite fold_comp -eq_u1.
        move=> eq_d'2.
        apply: (Sets_stb_po_eq_equiv (inr b) (inl a2))=> //.
        apply: (Quot_inj (Spe_is_equiv f g)).
        by rewrite -eq_ab eq_d'2 -eq_a2.
      apply: (eq_Quot (Spe_is_equiv f' g')).
      move: eq_ab.
      rewrite -/(Sets_po_coproj2 f' g' b') -/iB''.
      move: b'=> [[b2 d'2] /= eq_b2] eq_ab.
      have: u (iB'' (exist _ (b2, d'2) eq_b2)) = d'2.
        by rewrite fold_comp -eq_u2.
      move=> eq_d'2.
      apply: (Sets_stb_po_eq_equiv (inr b) (inr b2))=> //.
      apply: (Quot_inj (Spe_is_equiv f g)).
      by rewrite -eq_ab eq_d'2 -eq_b2.
    move=> d'' [_ eq_d''].
    rewrite -(eq_d'' (iB'' _))=> //.
    by rewrite fold_comp -eq_u2.
  Defined.

End stb_po.

Lemma Stable_po_from_canonical {𝐂: cat} [A B C: 𝐂]
  (f: C ~> A) (g: C ~> B):
  (∃ D (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB),
  forall D' (δ: D' ~> D), ∃ A' B' C' (f': C' ~> A') (g': C' ~> B')
  (iA': A' ~> D') (iB': B' ~> D') (α: A' ~> A) (β: B' ~> B)
  (χ: C' ~> C), Pullback iA δ α iA' ∧ Pullback iB δ β iB' ∧
  Pullback g β χ g' ∧ Pullback f α χ f' ∧ Pushout f' g' iA' iB')
  -> forall D (iA: A ~> D) (iB: B ~> D) (po: Pushout f g iA iB),
  Stable_po po.
Proof.
move=> [D_can [iA_can [iB_can [po_can get_cube]]]]
  D iA iB po A' B' C' D'
  f' g' iA' iB' α β χ δ
  pb_bl pb_br pb_fl pb_fr comm'.
have [isoD [eq_iD1 eq_iD2] _] := unique_pushout po po_can.
have [A'_can [B'_can [C'_can
  [f'_can [g'_can [iA'_can [iB'_can [α_can [β_can [χ_can
  [pb_fl_can [pb_fr_can [pb_br_can [pb_bl_can po_top_can]]]]]]]]]]]]]]
  := (get_cube D' ((isoD: D ≅ D_can)¹ ∘ δ))=> {get_cube po_can}.
subst.
apply Pullback_iso_C in pb_fl_can, pb_fr_can.
rewrite -compoA (iso_eq isoD) compo1 in pb_fl_can.
rewrite -compoA (iso_eq isoD) compo1 in pb_fr_can => {isoD}.
have [isoA [? ?] _] := unique_pullback pb_fl (Pb_sym pb_fl_can).
subst.
apply Pb_sym in pb_bl_can.
apply Pullback_iso_A in pb_bl_can.
apply Pushout_iso_A in po_top_can.
have [isoB [? ?] _] := unique_pullback pb_fr (Pb_sym pb_fr_can).
subst.
apply Pb_sym in pb_br_can.
apply Pullback_iso_A in pb_br_can.
apply Po_sym in po_top_can.
apply Pushout_iso_A in po_top_can.
clear pb_fl_can pb_fr_can.
have [isoC' [eq_isoC' ?] unique_isoC'] := unique_pullback pb_bl pb_bl_can.
subst.
rewrite -eq_isoC' in po_top_can.
clear pb_bl_can.
have [isoC'' [eq_isoC''1 eq_isoC''2] _] := unique_pullback pb_br pb_br_can.
rewrite -eq_isoC''1 in po_top_can.
clear pb_br_can.
have eq: f' ∘ isoC''¹ = isoA¹ ∘ f'_can.
  rewrite -eq_isoC'.
  apply: (Pullback_joint_mono pb_fl).
    by rewrite -compoA comm' compoA (po_comm po_top_can).
  by rewrite -compoA (pb_comm pb_bl) compoA eq_isoC''2 -compoA
    -(pb_comm pb_bl) compoA.
have ? := unique_isoC' isoC'' (eq, eq_isoC''2)=> {eq}.
subst.
clear dependent A'_can.
clear dependent B'_can.
clear eq_isoC''2.
apply Pushout_iso_C in po_top_can.
rewrite compoA (iso_eq isoC'') comp1o in po_top_can.
by apply Po_sym.
Defined.

Lemma Pb_fan_to_cube {𝐂: cat} [A A' B B' C C' D D': 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] [α: A' ~> A] [β: B' ~> B]
  [χ: C' ~> C] [δ: D' ~> D] [f': C' ~> A'] [g': C' ~> B']
  [iA': A' ~> D'] [iB': B' ~> D'] [iAf': C' ~> D']:
  Pullback iA δ α iA' -> Pullback iB δ β iB' ->
  Pullback (iA ∘ f) δ χ iAf' -> iA ∘ f = iB ∘ g ->
  iAf' = iA' ∘ f' -> α ∘ f' = f ∘ χ -> iAf' = iB' ∘ g' ->
  β ∘ g' = g ∘ χ -> Pullback f α χ f' ∧ Pullback g β χ g'.
Proof.
  move=> pb_fl pb_fr pb_mid comm eq_f'1 eq_f'2 eq_g'1 eq_g'2.
  split.
  rewrite eq_f'1 in pb_mid.
  apply: (Pb_sym (Pb_decomp eq_f'2 (Pb_sym pb_fl) (Pb_sym pb_mid))).
  rewrite eq_g'1 comm in pb_mid.
  apply: (Pb_sym (Pb_decomp eq_g'2 (Pb_sym pb_fr) (Pb_sym pb_mid))).
Defined.

Lemma Sets_stable_po [A B C D: Sets] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB): Stable_po po.
Proof.
  apply: Stable_po_from_canonical.
  pose D_can := Sets_po_obj f g.
  pose iA_can := Sets_po_coproj1 f g.
  pose iB_can := Sets_po_coproj2 f g.
  pose po_can := Sets_po f g : Pushout f g iA_can iB_can.
  exists D_can.
  exists iA_can.
  exists iB_can.
  exists po_can.
  intros D' δ.
  set pb_span_fl := Get_pb iA_can δ.
  set pb_span_fr := Get_pb iB_can δ.
  set pb_span_mid := Get_pb (iA_can ∘ f) δ.
  set C' := projT1 pb_span_mid : Sets.
  set A' := projT1 pb_span_fl.
  set B' := projT1 pb_span_fr.
  set iA' := projT1 (projT2 (projT2 pb_span_fl)): A' ~> D'.
  set α := projT1 (projT2 pb_span_fl): A' ~> A.
  set pb_fl := projT2 (projT2 (projT2 pb_span_fl)).
  set iB' := projT1 (projT2 (projT2 pb_span_fr)): B' ~> D'.
  set β := projT1 (projT2 pb_span_fr): B' ~> B.
  set pb_fr := projT2 (projT2 (projT2 pb_span_fr)).
  set iAf' := projT1 (projT2 (projT2 pb_span_mid)): C' ~> D'.
  set χ := projT1 (projT2 pb_span_mid): C' ~> C.
  set pb_mid := projT2 (projT2 (projT2 pb_span_mid)).
  set f' := ump_pb pb_fl (f ∘ χ) iAf' (pb_comm pb_mid).
  unshelve epose (eq_g' := _: iB_can ∘ (g ∘ χ) = δ ∘ iAf').
    by rewrite -(pb_comm pb_mid) -compoA -(po_comm po_can).
  set g' := ump_pb pb_fr (g ∘ χ) iAf' eq_g'.
  exists A'.
  exists B'.
  exists C'.
  exists f'.
  exists g'.
  exists iA'.
  exists iB'.
  exists α.
  exists β.
  exists χ.
  split.
  apply: pb_fl.
  split.
  apply: pb_fr.
  have [pb_bl pb_br] := (Pb_fan_to_cube pb_fl pb_fr pb_mid (po_comm po_can)
    (snd ('f')) (eq_sym (fst ('f'))) (snd ('g')) (eq_sym (fst ('g')))).
  split.
  apply: pb_br.
  split.
  apply: pb_bl.
  have po_top := Sets_stable_po_aux _ _ _ _ f g δ.
  rewrite -/iA' -/iB' -/f' -/g' in po_top.
  apply po_top.
Defined.

Lemma Sets_po_mono_rel [A B C: Sets] (f: C ~> A) (g: C ~> B)
  (mono_f: Mono f):
  Sets_po_equiv f g =
  fun x y => match x, y with
  | inl a1, inl a2 => a1 = a2 \/ exists b,
    Sets_po_relation f g (inl a1) (inr b)
    /\ Sets_po_relation f g (inl a2) (inr b)
  | inl a, inr b => Sets_po_relation f g (inl a) (inr b)
  | inr b, inl a => Sets_po_relation f g (inl a) (inr b)
  | inr b1, inr b2 => b1 = b2 end.
Proof.
  apply Sets_mono in mono_f.
  apply: funext=> x.
  apply: funext=> y.
  apply propext.
  split.
    elim.
      move=> {}x {}y.
      by case.
    case => // a.
    by left.
      move=> {x} {y} [a|b] [a'|b'] // rel [?|[b [? ?]]] //.
        by left.
        right.
        by exists b.
    move=> {x} {y} [a|b] [a'|b'] [a''|b''] rel1.
      move=> [?|[b1 [relb11 relb12]]] rel2 [?|[b2 [relb21 relb22]]]; subst.
        by left.
      right. exists b2. by split.
        right. exists b1. by split.
      right.
      inversion relb12 as [c1 eqc11 eqc12].
      inversion relb21 as [c2 eqc21 eqc22].
      inversion relb11 as [c3 eqc31 eqc32].
      inversion relb22 as [c4 eqc41 eqc42].
      subst. exists (g c3). split=> //.
      by move: eqc21 eqc32 eqc42=> /mono_f <- -> <-.
        move=> [?|[b [relb1 relb2]]] rel2 IHrel2; subst=> //.
        inversion relb1 as [c1 eqc11 eqc12].
        inversion relb2 as [c2 eqc21 eqc22].
        inversion IHrel2 as [c3 eqc31 eqc32].
        subst.
        by move: eqc31 eqc22 => /mono_f <- ->.
      move=> IHrel1 rel2 IHrel2. right.
      inversion IHrel1 as [c1 eqc11 eqc12].
      inversion IHrel2 as [c2 eqc21 eqc22].
      subst. exists (g c1). by split.
        move=> IHrel1 rel2 ?. by subst.
      move=> IHrel1 rel2 [?|[b' [relb'1 relb'2]]].
      by subst.
        inversion IHrel1 as [c1 eqc11 eqc12].
        inversion relb'1 as [c2 eqc21 eqc22].
        inversion relb'2 as [c3 eqc31 eqc32].
        subst.
        by move: eqc21 eqc32 => /mono_f -> <-.
      move=> IHrel1 rel2 IHrel2.
      inversion IHrel1 as [c1 eqc11 eqc12].
      inversion IHrel2 as [c2 eqc21 eqc22].
      subst.
      by move: eqc21=> /mono_f ->.
        move=> ? rel2 IHrel2. by subst.
      move=> ? rel2 ?. by subst.
  case: x=> [a|b]; case: y=> [a'|b'].
    move=> [->|[b [relb1 relb2]]].
      apply: rst_refl.
    apply rst_step in relb1, relb2.
    apply rst_sym in relb2.
    by apply: (rst_trans _ _ _ _ _ relb1 relb2).
      apply: rst_step.
    move=> ?.
    apply: rst_sym.
    by apply: rst_step.
      move=> ->.
      apply: rst_refl.
Qed.

Lemma Sets_po_mono_is_pb [A B C D: Sets] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f):
  Pushout f g iA iB -> Pullback iA iB f g.
Proof.
  move=> po.
  pose jA := Sets_po_coproj1 f g.
  pose jB := Sets_po_coproj2 f g.
  suff: Pullback jA jB f g.
    have [iso [+ +] _] := unique_pushout po (Sets_po f g).
    rewrite -/jA -/jB=> <- <-.
    move/(snd (Pullback_iso_C _ _)).
    by rewrite -compoA iso_eq compo1.
  apply: Build_Pullback.
    apply: (po_comm (Sets_po f g)).
  move=> E qA qB comm.
  pose P := fun x y => qA x = f y /\ qB x = g y.
  suff: forall e: E, exists! c: C, P e c.
    move/cdd_unique=> [u Pu unique_u].
    exists u.
      split; apply: funext=> e;
      rewrite /comp /=;
      by have [? ?]:= (Pu e).
    move=> v [eq_v1 eq_v2].
    apply: unique_u=> e.
    split; by [rewrite eq_v1|rewrite eq_v2].
  move=> e.
  have: Sets_po_equiv f g (inl (qA e)) (inr (qB e)).
    apply: (Quot_inj (Spe_is_equiv f g)).
    apply: equal_f comm e.
  rewrite Sets_po_mono_rel=> // rel.
  inversion rel as [c eq_c1 eq_c2].
  exists c.
  split.
    by split.
  move=> c' [eq_c'1 eq_c'2].
  by move: mono_f eq_c1 eq_c'1=> /Sets_mono mono_f <- /mono_f.
Defined.

(* This currently fails because of a bug in HB. *)
HB.instance Definition _ :=
  Cat_is_Adhesive_quasi.Build Sets
  (fun A B C f g _ => existT _ _
    (existT _ (Sets_po_coproj1 f g)
    (existT _ (Sets_po_coproj2 f g)
    (Sets_po f g))))
  (fun A B C D f g iA iB _ (po: Pushout f g iA iB) =>
    Sets_stable_po po)
  (fun A B C D f g iA iB mono_f (po: Pushout f g iA iB) =>
    Sets_po_mono_is_pb mono_f po).
  
