Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Morphism.
Require Import Pullback.
Require Import FunctionalExtensionality.
Require Import Sets.Sets.

Local Open Scope cat.

Definition Sets_pb_obj [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z) :=
  {a: X * Y | f (fst a) = g (snd a)}.

Definition Sets_pb_proj1 [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):
  Sets_pb_obj f g ~> X :=
  fun a => fst (proj1_sig a).

Definition Sets_pb_proj2 [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):
  Sets_pb_obj f g ~> Y :=
  fun a => snd (proj1_sig a).

Lemma Sets_pb [X Y Z: Sets] (f: X ~> Z) (g: Y ~> Z):
  Pullback f g (Sets_pb_proj1 f g) (Sets_pb_proj2 f g).
Proof.
  apply: Build_Pullback.
  apply funext.
  by intros [[a1 a2] eq_a].
  intros P qA qB comm.
  simpl in P.
  unshelve econstructor.
  intro p.
  unshelve eapply (exist _ (qA p, qB p) _).
  simpl.
  apply (equal_f comm).
  simpl.
  split.
  apply funext.
  by intro p.
  apply funext.
  by intro p.

  intros v [eq_v1 eq_v2].
  apply funext.
  intro p.
  apply eq_sig_hprop.
  intros.
  apply Prop_irrelevance.
  simpl.
  rewrite eq_v1 eq_v2.
  unfold Sets_pb_proj1.
  unfold Sets_pb_proj2.
  by rewrite -surjective_pairing.
Defined.

Lemma Pb_Sets: Cat_has_pb Sets.
Proof.
  constructor.
  intros X Y Z f g.
  exists (Sets_pb_obj f g).
  exists (Sets_pb_proj1 f g).
  exists (Sets_pb_proj2 f g).
  apply Sets_pb.
Defined.

HB.instance Definition _ := Pb_Sets.