Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.

Local Open Scope cat.

Definition Sets := Type.

HB.instance Definition _ := IsQuiver.Build
    Sets (fun X Y => X -> Y).

Lemma Precat_Sets: Quiver_IsPreCat Sets.
Proof.
  split=> [A | A B C f g a]//.
  apply: g (f a).
Defined.

HB.instance Definition _ := Precat_Sets.

Lemma Cat_Sets: PreCat_IsCat Sets.
Proof.
  split=> [A B f | A B f | A B C D f g h]//.
Defined.

HB.instance Definition _ := Cat_Sets.



