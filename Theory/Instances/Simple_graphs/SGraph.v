Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Coq.Logic.FunctionalExtensionality.
Require Import Coq.Logic.Description.
Require Import Utils.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Reg_mono.
Require Import Rm_Quasi_Adhesive.
Require Import Adhesive.
Require Import Span.
Require Import Unique.
Require Import Morphism.
Require Import Isomorphism.
Require Import Quotients.
Require Import RelationClasses.
Require Import Relation_Definitions.
Require Import Relation_Operators.
Require Import Sets.
Require Import Sets_pb.
Require Import Sets_adhesive.

Local Open Scope cat.

Record SGraph := {
    Simple_V: Type;
    Simple_E: relation Simple_V
}.

Record SGraph_morph (g1 g2: SGraph) := {
    SGraph_morph_V:> Simple_V g1 -> Simple_V g2;
    SGraph_morph_E: forall v1 v2, Simple_E g1 v1 v2 ->
        Simple_E g2 (SGraph_morph_V v1) (SGraph_morph_V v2);
}.

HB.instance Definition _ := IsQuiver.Build
    SGraph SGraph_morph.


Lemma SGraph_morphP (g1 g2: SGraph) (f h: SGraph_morph g1 g2):
  (f: g1 ~> g2) = (h: g1 ~> g2) ↔ (SGraph_morph_V _ _ f: Simple_V g1 ~> Simple_V g2) = SGraph_morph_V _ _ h.
Proof.
  split.
  intro.
  by rewrite H.
  intro.
  destruct f, h.
  simpl in H.
  revert SGraph_morph_E0.
  rewrite H.
  intros.
  f_equal.
  apply Prop_irrelevance.
Qed.

Lemma PreCat_SGraph: Quiver_IsPreCat SGraph.
Proof.
  constructor.
  intro a.
  by exists id.
  intros g1 g2 g3 [m1_V m1_E] [m2_V m2_E].
  unshelve econstructor.
  intro v.
  apply m2_V.
  by apply m1_V.
  intros v1 v2 e.
  apply m2_E.
  by apply m1_E.
Defined.

HB.instance Definition _ := PreCat_SGraph.

Lemma Cat_SGraph: PreCat_IsCat SGraph.
Proof.
  constructor.
  by intros g1 g2 [f_V f_E].
  by intros g1 g2 [f_V f_E].
  by intros g1 g2 g3 g4 [f_V f_E] [g_V g_E] [h_V h_E].
Defined.

HB.instance Definition _ := Cat_SGraph.

Definition Graph_of_Set (A: Sets): SGraph :=
  Build_SGraph A (fun x y => False).

Definition Graph_morph_of_Set [A: Sets] [B: SGraph]
  (f: A ~> Simple_V B): Graph_of_Set A ~> B :=
  Build_SGraph_morph (Graph_of_Set A) B
    f
    (fun v1 v2 F => False_ind _ F).

Lemma SGraph_unfold_comp [A B C: SGraph]
  (f: A ~> B) (g: B ~> C) (x: Simple_V A):
  (g ∘ f) x = g (f x).
Proof.
  destruct f, g.
  reflexivity.
Defined.

Definition SGraph_pb_rel [A B C: SGraph]
  (f: A ~> C) (g: B ~> C): relation (Sets_pb_obj f g).
  intros [[v1 v1'] ?] [[v2 v2'] ?].
  apply (Simple_E _ v1 v2 /\ Simple_E _ v1' v2').
Defined.

Definition SGraph_pb_obj [A B C: SGraph]
  (f: A ~> C) (g: B ~> C): SGraph.
unshelve econstructor.
apply (Sets_pb_obj f g).
apply (SGraph_pb_rel f g).
Defined.

Definition SGraph_pb_proj1 [A B C: SGraph]
  (f: A ~> C) (g: B ~> C): SGraph_pb_obj f g ~> A.
unshelve econstructor.
apply (Sets_pb_proj1 f g).
by intros [[? ?] ?] [[? ?] ?] [? _].
Defined.

Definition SGraph_pb_proj2 [A B C: SGraph]
  (f: A ~> C) (g: B ~> C): SGraph_pb_obj f g ~> B.
unshelve econstructor.
apply (Sets_pb_proj2 f g).
by intros [[? ?] ?] [[? ?] ?] [_ ?].
Defined.

Lemma SGraph_canonical_pb [A B C: SGraph]
  (f: A ~> C) (g: B ~> C):
  Pullback f g (SGraph_pb_proj1 f g) (SGraph_pb_proj2 f g).
Proof.
  destruct f as [fV fE].
  destruct g as [gV gE].
  apply Build_Pullback.

  apply SGraph_morphP.
  simpl.
  apply funext.
  intro.
  apply (equal_f (pb_comm (Sets_pb fV gV))).

  unfold Ump_pb.
  intros Y [p1V p1E] [p2V p2E] eq.
  apply SGraph_morphP in eq.
  simpl in *.
  destruct (ump_pb (Sets_pb fV gV) p1V p2V eq) as [u [eq_u1 eq_u2] unique_u].
  unshelve eexists.
  unshelve econstructor.
  apply u.

  2:{
    split;
    apply SGraph_morphP.
    apply eq_u1.
    apply eq_u2.
   }
  
  2:{
    intros [vV vE] [eq_v1 eq_v2].
    apply SGraph_morphP.
    apply unique_u.
    apply SGraph_morphP in eq_v1, eq_v2.
    by split.
  }

  intros v1 v2 e.
  destruct (u v1) as [[? ?] ?] eqn: eq_uv1.
  destruct (u v2) as [[? ?] ?] eqn: eq_uv2.
  simpl in *.
  split.
  pose proof (e' := p1E _ _ e).
  2: pose proof (e' := p2E _ _ e).
  rewrite eq_u1 in e'.
  2: rewrite eq_u2 in e'.
  all: cbn in e';
  by rewrite eq_uv1 eq_uv2 in e'.
Defined.

Lemma Pb_SGraph: Cat_has_pb SGraph.
Proof.
  constructor.
  intros A B C f g.
  exists (SGraph_pb_obj f g).
  exists (SGraph_pb_proj1 f g).
  exists (SGraph_pb_proj2 f g).
  apply SGraph_canonical_pb.
Defined.

HB.instance Definition _ := Pb_SGraph.

Lemma Pb_SGraph_to_Sets [A B C D: SGraph] [f: A ~> C]
  [g: B ~> C] [pA: D ~> A] [pB: D ~> B]:
  Pullback (𝐂 := SGraph) f g pA pB ->
  Pullback (𝐂 := Sets) f g pA pB.
Proof.
  intro pb.
  apply Destruct_Pullback in pb.
  destruct pb as [comm ump].
  destruct f as [fV fE].
  destruct g as [gV gE].
  destruct pA as [pAV pAE].
  destruct pB as [pBV pBE].
  apply SGraph_morphP in comm.
  simpl in comm.
  apply Build_Pullback.
  apply comm.
  intros Y qA qB comm'.
  simpl in *.
  unshelve epose proof (u := ump _ (Build_SGraph_morph
    (Build_SGraph Y (fun _ _ => False)) _
      qA (fun _ _ f => False_ind _ f))
    (Build_SGraph_morph
    (Build_SGraph Y (fun _ _ => False)) _
      qB (fun _ _ f => False_ind _ f)) _).
  by apply SGraph_morphP.
  simpl in u.
  destruct u as [[uV uE] [eq_u1 eq_u2] unique_u].
  apply SGraph_morphP in eq_u1, eq_u2.
  simpl in eq_u1, eq_u2.
  exists uV.
  by split.
  intros v [eq_v1 eq_v2].
  unshelve epose (eq_uv := unique_u (Build_SGraph_morph
    (Build_SGraph Y (fun _ _ => False)) _
      v (fun _ _ f => False_ind _ f)) _).
  split; apply SGraph_morphP; auto.
  by apply SGraph_morphP in eq_uv.
Defined.

Lemma Pb_SGraph_Sets_eq [A B C D: SGraph] (f: A ~> C)
  (g: B ~> C) (pA: D ~> A) (pB: D ~> B):
  Pullback (𝐂 := SGraph) f g pA pB ↔
  Pullback (𝐂 := Sets) f g pA pB ∧
    (forall d1 d2: Simple_V D,
      Simple_E A (pA d1) (pA d2) ∧
      Simple_E B (pB d1) (pB d2)
      -> Simple_E D d1 d2).
Proof.
  split.
  intro pb.
  split.
  by apply Pb_SGraph_to_Sets.
  intros d1 d2 [e1 e2].
  pose (pb' := SGraph_canonical_pb f g).
  destruct (unique_pullback pb pb') as [i [eq_i1 eq_i2] _].
  pose proof (E_i := SGraph_morph_E _ _ i¹ (i⁻¹ d1) (i⁻¹ d2)).
  assert (i_to_from: forall x, i¹ (i⁻¹ x) = x).
  {pose proof (iso_i := iso_to_from (iso_eq i)).
  apply SGraph_morphP in iso_i.
  intro x.
  pose proof (iso_i_x := equal_f iso_i x).
  by rewrite SGraph_unfold_comp in iso_i_x. }
  rewrite 2!i_to_from in E_i.
  apply E_i.
  rewrite /= /SGraph_pb_rel /=.
  apply iso_eq_switch_r in eq_i1, eq_i2.
  subst.
  rewrite 4!SGraph_unfold_comp in e1, e2.
  set (d1' := i⁻¹ d1) in *.
  set (d2' := i⁻¹ d2) in *.
  destruct d1' as [[? ?] ?].
  destruct d2' as [[? ?] ?].
  by split.

  intros [pb pb_E].
  pose (D' := SGraph_pb_obj f g).
  assert (iso: ∃ i: D' ≅ D, pA ∘ i¹ = SGraph_pb_proj1 f g ∧
    pB ∘ i¹ = SGraph_pb_proj2 f g).
  destruct (unique_pullback pb (Pb_SGraph_to_Sets (SGraph_canonical_pb f g)))
    as [i [eq_i1 eq_i2] _].
  unshelve eexists.
  unshelve econstructor.
  unshelve econstructor.
  apply (i¹).
  shelve.
  unshelve econstructor.
  unshelve econstructor.
  apply (i⁻¹).
  shelve.
  split;
  apply SGraph_morphP.
  apply (iso_to_from (iso_eq i)).
  apply (iso_from_to (iso_eq i)).
  destruct pA, pB.
  split;
  by apply SGraph_morphP.
  destruct iso as [i [eq_i1 eq_i2]].
  apply (Pullback_iso_D' i).
  rewrite eq_i1 eq_i2.
  apply SGraph_canonical_pb.
  Unshelve.
  (* Both goals are proven using the equivalence
  between the existence of edges in A and B and the
  existence of an edge in D (one way is our hypothesis,
  the other way is always true by the property of
  morphisms). This corresponds exactly to the
  definition of edges in the canonical pullback. *)
  intros v1 v2 e.
  apply pb_E.
  pose (eq_pAi1:= equal_f eq_i1).
  pose (eq_pBi1:= equal_f eq_i2).
  cbn in eq_pAi1, eq_pBi1.
  rewrite 2!eq_pAi1 2!eq_pBi1.
  destruct v1 as [[? ?] ?].
  destruct v2 as [[? ?] ?].
  destruct e.
  by split.
  intros v1 v2 e.
  pose proof (e_A := SGraph_morph_E _ _ pA v1 v2 e).
  pose proof (e_B := SGraph_morph_E _ _ pB v1 v2 e).
  apply iso_eq_switch_r in eq_i1, eq_i2.
  rewrite eq_i1 in e_A.
  rewrite eq_i2 in e_B.
  cbn in e_A, e_B.
  destruct (i⁻¹ v1) as [[? ?] ?] eqn: H.
  destruct (i⁻¹ v2) as [[? ?] ?] eqn: H'.
  by rewrite H H' in e_A, e_B.
Defined.

Lemma Po_SGraph_to_Sets [A B C D: SGraph] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D]:
  Pushout (𝐂 := SGraph) f g iA iB ->
  Pushout (𝐂 := Sets) f g iA iB.
Proof.
  intro po.
  apply Destruct_Pushout in po.
  destruct po as [comm ump].
  destruct f as [fV fE].
  destruct g as [gV gE].
  destruct iA as [iAV iAE].
  destruct iB as [iBV iBE].
  apply SGraph_morphP in comm.
  simpl in comm.
  apply Build_Pushout.
  apply comm.
  intros Y jA jB comm'.
  simpl in *.
  unshelve epose proof (u := ump _ (Build_SGraph_morph
    _ (Build_SGraph Y (fun _ _ => True))
      jA (fun _ _ _ => I))
    (Build_SGraph_morph
    _ (Build_SGraph Y (fun _ _ => True))
      jB (fun _ _ _ => I)) _).
  by apply SGraph_morphP.
  
  simpl in u.
  destruct u as [[uV uE] [eq_u1 eq_u2] unique_u].
  apply SGraph_morphP in eq_u1, eq_u2.
  simpl in eq_u1, eq_u2.
  exists uV.
  by split.
  intros v [eq_v1 eq_v2].
  unshelve epose (eq_uv := unique_u (Build_SGraph_morph
    _ (Build_SGraph Y (fun _ _ => True))
      v (fun _ _ _ => I)) _).
  split; apply SGraph_morphP; auto.
  by apply SGraph_morphP in eq_uv.
Defined.

Definition SGraph_po_obj [A B C: SGraph] (f: C ~> A)
  (g: C ~> B): SGraph.
  exists (Sets_po_obj f g).
  intros v1 v2.
  apply ((exists a1 a2: Simple_V A, v1 = Quot (Sets_po_equiv f g) (inl a1)
    /\ v2 = Quot (Sets_po_equiv f g) (inl a2) /\
    Simple_E A a1 a2) \/
    (exists b1 b2: Simple_V B, v1 = Quot (Sets_po_equiv f g) (inr b1)
    /\ v2 = Quot (Sets_po_equiv f g) (inr b2) /\
    Simple_E B b1 b2)).
Defined.

Definition SGraph_po_coproj1 [A B C: SGraph] (f: C ~> A)
  (g: C ~> B): A ~> SGraph_po_obj f g.
  exists (Sets_po_coproj1 f g).
  intros a1 a2 e.
  left.
  exists a1.
  by exists a2.
Defined.

Definition SGraph_po_coproj2 [A B C: SGraph] (f: C ~> A)
  (g: C ~> B): B ~> SGraph_po_obj f g.
  exists (Sets_po_coproj2 f g).
  intros b1 b2 e.
  right.
  exists b1.
  by exists b2.
Defined.

Lemma SGraph_canonical_po [A B C : SGraph] (f : C ~> A) (g : C ~> B):
  Pushout f g (SGraph_po_coproj1 f g) (SGraph_po_coproj2 f g).
Proof.
  destruct f as [fV fE].
  destruct g as [gV gE].
  apply Build_Pushout.
  apply SGraph_morphP.
  simpl.
  apply funext.
  intro.
  apply (equal_f (po_comm (Sets_po fV gV))).

  intros Y [j1V j1E] [j2V j2E] eq.
  simpl in Y.
  apply SGraph_morphP in eq.
  simpl in eq.
  destruct (ump_po (Sets_po fV gV) j1V j2V eq)
    as [u eq_u unique_u].
  unshelve eexists.
  exists u.

  2: {
    split;
    apply SGraph_morphP; simpl;
    rewrite eq_u;
    reflexivity.
  }

  2: {
    intros [vV vE] [eq_v1 eq_v2].
    apply SGraph_morphP; simpl.
    apply unique_u.
    apply SGraph_morphP in eq_v1, eq_v2.
    by split.
  }

  simpl.
  intros v1 v2 [[a1 [a2 [eq_a1 [eq_a2 e]]]]
  |[b1 [b2 [eq_b1 [eq_b2 e]]]]]; subst.
  pose proof (e' := j1E _ _ e).
  2: pose proof (e' := j2E _ _ e).
  all: by rewrite eq_u in e'.
Defined.

Lemma Po_SGraph_Sets_eq [A B C D: SGraph] (f: C ~> A)
  (g: C ~> B) (iA: A ~> D) (iB: B ~> D):
  Pushout (𝐂 := SGraph) f g iA iB ↔
  Pushout (𝐂 := Sets) f g iA iB ∧
    (forall d1 d2: Simple_V D,
    Simple_E D d1 d2 ->
    (exists a1 a2: Simple_V A, d1 = iA a1 /\ d2 = iA a2 /\
    Simple_E A a1 a2) \/
    (exists b1 b2: Simple_V B, d1 = iB b1 /\ d2 = iB b2 /\
    Simple_E B b1 b2)).
Proof.
  split.
  intro po.
  split.
  by apply Po_SGraph_to_Sets.
  intros d1 d2 e.

  pose (po' := SGraph_canonical_po f g).
  destruct (unique_pullback po po') as [i [eq_i1 eq_i2] _].
  pose proof (E_i := SGraph_morph_E _ _ i¹ d1 d2 e).
  apply iso_eq_switch_r in eq_i1, eq_i2.
  pose (iso_i := iso_to_from (iso_eq i)).
  apply SGraph_morphP in iso_i.
  destruct E_i as [[a1 [a2 [eq1 [eq2 e']]]]|
    [b1 [b2 [eq1 [eq2 e']]]]].
  left.
  exists a1.
  exists a2.
  rewrite eq_i1.
  by rewrite 2!SGraph_unfold_comp /= /Sets_po_coproj1
    -eq1 -eq2 -2!SGraph_unfold_comp iso_i.
  right.
  exists b1.
  exists b2.
  rewrite eq_i2.
  by rewrite 2!SGraph_unfold_comp /= /Sets_po_coproj2
    -eq1 -eq2 -2!SGraph_unfold_comp iso_i.

  intros [po po_E].
  pose (D' := SGraph_po_obj f g).
  assert (iso: ∃ i: D ≅ D', i¹ ∘ iA = SGraph_po_coproj1 f g ∧
    i¹ ∘ iB = SGraph_po_coproj2 f g).
  destruct (unique_pushout po (Po_SGraph_to_Sets (SGraph_canonical_po f g)))
    as [i [eq_i1 eq_i2] _].
  unshelve eexists.
  unshelve econstructor.
  unshelve econstructor.
  apply (i¹).
  shelve.
  unshelve econstructor.
  unshelve econstructor.
  apply (i⁻¹).
  shelve.
  split;
  apply SGraph_morphP.
  apply (iso_to_from (iso_eq i)).
  apply (iso_from_to (iso_eq i)).
  destruct iA, iB.
  split;
  by apply SGraph_morphP.
  destruct iso as [i [eq_i1 eq_i2]].
  apply (Pushout_iso_D' i).
  rewrite eq_i1 eq_i2.
  apply SGraph_canonical_po.
  Unshelve.
  (* Both goals are proven using the equivalence
  between the existence of edges in A and B and the
  existence of an edge in D (one way is our hypothesis,
  the other way is always true by the property of
  morphisms). This corresponds exactly to the
  definition of edges in the canonical pullback. *)
  intros v1 v2 e.
  pose (po' := SGraph_canonical_po f g).
  destruct (po_E _ _ e) as [[a1 [a2 [eq1 [eq2 e']]]]|
    [b1 [b2 [eq1 [eq2 e']]]]].
  left.
  exists a1.
  exists a2.
  subst.
  split.
  apply (equal_f eq_i1 a1).
  split.
  apply (equal_f eq_i1 a2).
  assumption.
  right.
  exists b1.
  exists b2.
  subst.
  split.
  apply (equal_f eq_i2 b1).
  split.
  apply (equal_f eq_i2 b2).
  assumption.

  intros v1 v2 e.
  apply iso_eq_switch_l in eq_i1, eq_i2.
  destruct e as [[a1 [a2 [eq1 [eq2 e']]]]|
    [b1 [b2 [eq1 [eq2 e']]]]];
  subst.
  apply (SGraph_morph_E _ _ iA) in e'.
  by rewrite eq_i1 in e'.
  apply (SGraph_morph_E _ _ iB) in e'.
  by rewrite eq_i2 in e'.
Defined.

Lemma Po_rm_SGraph: Cat_has_po_along_reg_mono SGraph.
Proof.
  constructor.
  intros A B C f g _.
  exists (SGraph_po_obj f g).
  exists (SGraph_po_coproj1 f g).
  exists (SGraph_po_coproj2 f g).
  apply SGraph_canonical_po.
Defined.

HB.instance Definition _ := Po_rm_SGraph.

Lemma SGraph_stable_po [A B C D: SGraph] (f: C ~> A)
  (g: C ~> B) (iA: A ~> D) (iB: B ~> D) (po_bot: Pushout f g iA iB):
  Stable_po po_bot.
Proof.
  intros A' B' C' D' f' g' iA' iB' α β χ δ pb_l pb_b pb_f pb_r comm_top.
  apply Po_SGraph_Sets_eq.
  apply Po_SGraph_Sets_eq in po_bot.
  destruct po_bot as [po_bot po_bot_prop].
  pose (pb_f' := SGraph_canonical_pb δ iA).
  destruct (unique_pullback pb_f pb_f') as [i_A' [eq_A'1 eq_A'2] _].
  pose (pb_r' := SGraph_canonical_pb δ iB).
  destruct (unique_pullback pb_r pb_r') as [i_B' [eq_B'1 eq_B'2] _].
  apply Pb_SGraph_Sets_eq in pb_l, pb_b, pb_f, pb_r.
  destruct pb_l as [pb_l pb_l_prop].
  destruct pb_r as [pb_r pb_r_prop].
  destruct pb_b as [pb_b pb_b_prop].
  destruct pb_f as [pb_f pb_f_prop].
  unshelve epose proof (po_top := Sets_stable_po
    po_bot _ _ _ _ _ _ _ _ _ _ _ _
    pb_l pb_b pb_f pb_r _).
  destruct iA', f', iB', g';
  by apply SGraph_morphP in comm_top.
  split.
  assumption.
  intros d1 d2 e_D'.
  pose proof (e_D := SGraph_morph_E _ _ δ _ _ e_D').
  destruct (po_bot_prop _ _ e_D) as [[a1 [a2 [eq1 [eq2 e_A]]]]|
    [b1 [b2 [eq1 [eq2 e_B]]]]].
  left.
  pose (a'1 := exist _ (d1, a1) eq1: Sets_pb_obj δ iA).
  pose (a'2 := exist _ (d2, a2) eq2: Sets_pb_obj δ iA).
  exists (i_A'¹ a'1).
  exists (i_A'¹ a'2).
  apply SGraph_morphP in eq_A'1, eq_A'2.
  destruct iA' as [iA' ?].
  destruct (i_A'¹) as [i_A'1 ?].
  destruct α as [α ?].
  pose proof (eq_a'11 := equal_f eq_A'1 a'1).
  pose proof (eq_a'12 := equal_f eq_A'1 a'2).
  pose proof (eq_a'21 := equal_f eq_A'2 a'1).
  pose proof (eq_a'22 := equal_f eq_A'2 a'2).
  simpl.
  cbn in eq_a'11, eq_a'12, eq_a'21, eq_a'22.
  rewrite eq_a'11 eq_a'12.
  split.
  reflexivity.
  split.
  reflexivity.
  apply pb_f_prop.
  simpl.
  split.
  by rewrite eq_a'11 eq_a'12.
  by rewrite eq_a'21 eq_a'22.
  right.
  pose (b'1 := exist _ (d1, b1) eq1: Sets_pb_obj δ iB).
  pose (b'2 := exist _ (d2, b2) eq2: Sets_pb_obj δ iB).
  exists (i_B'¹ b'1).
  exists (i_B'¹ b'2).
  apply SGraph_morphP in eq_B'1, eq_B'2.
  destruct iB' as [iB' ?].
  destruct (i_B'¹) as [i_B'1 ?].
  destruct β as [β ?].
  pose proof (eq_b'11 := equal_f eq_B'1 b'1).
  pose proof (eq_b'12 := equal_f eq_B'1 b'2).
  pose proof (eq_b'21 := equal_f eq_B'2 b'1).
  pose proof (eq_b'22 := equal_f eq_B'2 b'2).
  simpl.
  cbn in eq_b'11, eq_b'12, eq_b'21, eq_b'22.
  rewrite eq_b'11 eq_b'12.
  split.
  reflexivity.
  split.
  reflexivity.
  apply pb_r_prop.
  simpl.
  split.
  by rewrite eq_b'11 eq_b'12.
  by rewrite eq_b'21 eq_b'22.
Defined.

Lemma SGraph_reg_mono [A B: SGraph] [m: A ~> B]
  (rm_m: Reg_mono m): forall a1 a2: Simple_V A,
  Simple_E _ (m a1) (m a2) -> Simple_E _ a1 a2.
Proof.
  intros a1 a2 e.
  pose proof (mono_m := Reg_mono_is_mono _ rm_m).
  destruct rm_m as [C [f [g eq]]].
  pose (D := {|
    Simple_V := Simple_V A;
    Simple_E :=
      fun x1 x2 : Simple_V A =>
      Simple_E B (m x1) (m x2)
    |}).
  unshelve epose proof (prop := Equalizer.Eq_prop eq (E' := D) _ _).
  unshelve econstructor.
  apply (SGraph_morph_V _ _ m).
  2: { destruct f, g, m.
  pose (eq_fgm := Equalizer.Eq_comp eq).
  apply SGraph_morphP.
  by apply SGraph_morphP in eq_fgm.
  }
  auto.
  destruct prop as [ɛ eq_ɛ _].
  (* Here we need to prove that ɛ is the identity morphism
  on the underlying sets. Since we cannot define the identity
  between D and A as a graph morphism, we start from the
  graph with the vertices of A but without the edges. *)
  pose (A_Set := {|
    Simple_V := Simple_V A;
    Simple_E :=
      fun x1 x2 : Simple_V A =>
      False
    |}).
  pose (id_A_Set_D := {|
    SGraph_morph_V := id: Simple_V A_Set -> Simple_V D;
    SGraph_morph_E :=
      fun (v1 v2 : Simple_V A_Set) (e : Simple_E A_Set v1 v2) => False_ind _ e
    |} : A_Set ~> D).
  pose (id_A_Set_A := {|
    SGraph_morph_V := id: Simple_V A_Set -> Simple_V A;
    SGraph_morph_E :=
      fun (v1 v2 : Simple_V A_Set) (e : Simple_E A_Set v1 v2) => False_ind _ e
    |} : A_Set ~> A).
  assert (eq_mɛ: m ∘ id_A_Set_A = m ∘ (ɛ ∘ id_A_Set_D)).
  destruct m, ɛ.
  apply SGraph_morphP in eq_ɛ.
  simpl in eq_ɛ.
  apply SGraph_morphP.
  simpl.
  by rewrite {1}eq_ɛ.
  pose proof (e_D := SGraph_morph_E _ _ ɛ _ _ e).
  apply mono_m in eq_mɛ.
  destruct ɛ as [ɛ ?].
  apply SGraph_morphP in eq_mɛ.
  rewrite /= {2}/id in eq_mɛ.
  clear dependent A_Set.
  pose proof (eq_a1 := (equal_f eq_mɛ a1)).
  pose proof (eq_a2 := (equal_f eq_mɛ a2)).
  rewrite /id /= in eq_a1, eq_a2.
  by rewrite /= -eq_a1 -eq_a2 in e_D.
Defined.

Lemma Reg_mono_SGraph_to_Sets [A B: SGraph] [f: A ~> B]:
  Reg_mono (𝐂 := SGraph) f -> Reg_mono (𝐂 := Sets) f.
Proof.
  case=> [E [h1 [h2 [eq eq_prop]]]].
  exists (Simple_V E).
  exists h1.
  exists h2.
  split.
    move: eq.
    move/(fst (SGraph_morphP _ _ _ _)).
    by destruct f, h1, h2.
  move=> E' e' eq_e'.
  have:= (eq_prop (Graph_of_Set E')
    (Graph_morph_of_Set e')).
  case=> [|ɛ eq_ɛ unique_ɛ].
    apply/(snd (SGraph_morphP _ _ _ _)).
    move: eq_e'.
    by destruct h1, h2.
  exists ɛ => [|ɛ' eq_ɛ'].
    move: eq_ɛ.
    move/(fst (SGraph_morphP _ _ _ _)).
    by destruct f, ɛ.
  unshelve epose proof (eq_ɛɛ' := 
    unique_ɛ (Graph_morph_of_Set ɛ') _).
    apply/(snd (SGraph_morphP _ _ _ _)).
    move: eq_ɛ'.
    by destruct f.
  move: eq_ɛɛ'.
  by move/(fst (SGraph_morphP _ _ _ _)).
Defined.

Lemma SGraph_po_rm_pb [A B C D: SGraph]
  [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
  (rm_f: Reg_mono f) (po: Pushout f g iA iB):
  Pullback iA iB f g.
Proof.
  apply Po_SGraph_Sets_eq in po.
  destruct po as [po po_prop].
  apply Pb_SGraph_Sets_eq.
  split.
    apply (po_is_pb po (Reg_mono_SGraph_to_Sets rm_f)).
  intros c1 c2 [e_A e_B].
  apply (SGraph_reg_mono rm_f _ _ e_A).
Defined.

Lemma Rm_Q_Adhesive_SGraph: Cat_is_Rm_Q_Adhesive SGraph.
Proof.
  constructor.
  intros.
  apply SGraph_stable_po.
  intros.
  by apply SGraph_po_rm_pb.
Defined.

HB.instance Definition _ := Rm_Q_Adhesive_SGraph.
