(** * Cospans *)
(** Definition of cospans as spans in the opposite category *)

Set Warnings "-notation-overridden".

Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Span.

Local Open Scope cat.

Definition Cospan {𝐂: precat} (A B: 𝐂) := @Span 𝐂^op A B.

Definition cosp_obj {𝐂: precat} [A B: 𝐂] (cosp: Cospan A B): 𝐂
  := sp_obj cosp.
Definition cosp_l {𝐂: precat} [A B: 𝐂] (cosp: Cospan A B):
  A ~> cosp_obj cosp := sp_l cosp.
Definition cosp_r {𝐂: precat} [A B: 𝐂] (cosp: @Cospan 𝐂 A B):
  B ~> cosp_obj cosp := sp_r cosp.

Definition Build_cospan {𝐂: precat} [A B X: 𝐂] (l: A ~> X) (r: B ~> X):
  Cospan A B := @Build_span 𝐂^op _ _ _ l r.

HB.instance Definition _ (𝐂 : precat) (A B : 𝐂) :=
  Quiver.copy (Cospan A B) (@Span 𝐂^op A B)^op.
HB.instance Definition _ (𝐂 : cat) (A B : 𝐂) :=
  Cat.copy (Cospan A B) (@Span 𝐂^op A B)^op.
