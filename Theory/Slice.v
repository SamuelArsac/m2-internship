Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Pullback.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Unique.
Require Import Equalizer.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Utils.

Local Open Scope cat.

Section Slice.

Context {𝐂 : cat} (C: 𝐂).

Definition Slice := {X: 𝐂 & X ~> C}.

HB.instance Definition _ := IsQuiver.Build Slice
  (fun x y => {f: (projT1 x) ~> (projT1 y) | projT2 y ∘ f = projT2 x }).

Definition Slice_id (x: Slice): x ~> x := exist _ idmap (comp1o (projT2 x)).

Definition Slice_comp (x y z: Slice) (f: x ~> y) (g: y ~> z): x ~> z.
  exists (proj1_sig g ∘ proj1_sig f).
  by rewrite -compoA (proj2_sig g) (proj2_sig f).
Defined.

HB.instance Definition _ := Quiver_IsPreCat.Build Slice
  Slice_id Slice_comp.

Lemma Slice_mapP (s1 s2 : Slice) (f g : s1 ~> s2) :
  proj1_sig f = proj1_sig g <-> f = g.
Proof.
  split.
  { intro.
  destruct f, g.
  simpl in *.
  revert e.
  rewrite H.
  intro.
  assert (e = e0).
  apply Prop_irrelevance.
  by rewrite H0. }
  intro.
  by rewrite H.
Qed.

Definition Slicecomp1o: forall (a b : Slice) (f : a ~> b), idmap \; f = f.
  intros.
  apply Slice_mapP.
  simpl.
  apply comp1o.
Defined.

Definition Slicecompo1: forall (a b : Slice) (f : a ~> b), f \; idmap = f.
  intros.
  apply Slice_mapP.
  simpl.
  apply compo1.
Defined.

Definition SlicecompoA: forall (a b c d : Slice) (f : a ~> b) (g : b ~> c) (h : c ~> d),
f \; (g \; h) = (f \; g) \; h.
  intros.
  apply Slice_mapP.
  simpl.
  apply compoA.
Defined.

HB.instance Definition _ := PreCat_IsCat.Build Slice Slicecomp1o Slicecompo1 SlicecompoA.
End Slice.

Section Slice_po_pb.

Context {𝐂: cat} [X: 𝐂].

Lemma Slice_to_cat_pb [A B C D: Slice X] [f: A ~> C] [g: B ~> C]
  [pA: D ~> A] [pB: D ~> B] (pb: Pullback f g pA pB):
  Pullback (proj1_sig f) (proj1_sig g) (proj1_sig pA) (proj1_sig pB).
Proof.
  apply Build_Pullback.
    by have /Slice_mapP := (pb_comm pb).
  move=> E qA qB comm.
  have eq: `2 (B) ∘ qB = `2 (A) ∘ qA.
    by rewrite /= -(proj2_sig f) compoA comm -compoA (proj2_sig g).
  have /= u := ump_pb pb (E := existT _ E (`2 (A) ∘ qA))
    (exist _ qA eq_refl) (exist _ qB eq).
  forward u.
    by apply/Slice_mapP.
  exists (proj1_sig (u: _ ~> D)).
    by have:= 'u=> -[/Slice_mapP eq_u1 /Slice_mapP eq_u2].
  move=> v [eq_v1 eq_v2].
  have eq': `2 (D) ∘ v = `2 (A) ∘ qA.
    by rewrite eq_v1 -compoA (proj2_sig pA).
  have -> // := (''u) (exist _ v eq').
  by split; apply/Slice_mapP.
Defined.

Lemma Slice_to_cat_po [A B C D: Slice X] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  (∃ D' (jA: `1 A ~> D') (jB: `1 B ~> D'),
    Pushout (proj1_sig f) (proj1_sig g) jA jB) ->
  Pushout (proj1_sig f) (proj1_sig g) (proj1_sig iA) (proj1_sig iB).
Proof.
  move=> [D' [jA [jB po_cat]]].
  suff: ∃ iso: D' ≅ (`1 D), iso¹ ∘ jA = (proj1_sig iA)
    ∧ iso¹ ∘ jB = (proj1_sig iB).
    move=> [iso [eq_iso1 eq_iso2]].
    move: po_cat.
    rewrite -eq_iso1 -eq_iso2.
    by move/(fst (Pushout_iso_D' iso)).
  have i := ump_po po_cat (proj1_sig iA) (proj1_sig iB).
  forward i.
    have:= po_comm po.
    by move/Slice_mapP.
  have d' := ump_po po_cat (`2 A) (`2 B).
  forward d'.
    by rewrite (proj2_sig f) (proj2_sig g).
  pose (D'' := existT _ D' (d': D' ~> X): Slice X).
  unshelve epose (i' := exist _ (i: D' ~> `1 D) _: D'' ~> D).
    simpl.
    apply (Pushout_joint_epi po_cat).
      by rewrite compoA -('i) -('d') (proj2_sig iA).
    by rewrite compoA -('i) -('d') (proj2_sig iB).
  pose (jA' := exist _ jA (eq_sym (fst ('d'))): A ~> D'').
  pose (jB' := exist _ jB (eq_sym (snd ('d'))): B ~> D'').
  have i'' := ump_po po jA' jB'.
  forward i''.
    apply/Slice_mapP=> /=.
    apply: po_comm po_cat.
  assert (IsIso2 (proj1_sig i') (proj1_sig (i'': D ~> D''))).
    split.
      replace (@idmap _ `1 D) with (proj1_sig (@idmap _ D))=> //.
      replace (proj1_sig i' ∘ proj1_sig _) with (proj1_sig (i' ∘ i''))=> //.
      apply/Slice_mapP.
      apply (Pushout_joint_epi po);
        rewrite compo1 compoA -('i'');
        apply/Slice_mapP=> /=;
        by rewrite -('i).
    apply (Pushout_joint_epi po_cat)=> /=;
      rewrite compo1 compoA -('i).
      by have /Slice_mapP := (fst ('i''))=> /= <-.
    by have /Slice_mapP := (snd ('i''))=> /= <-.
  unshelve eexists.
    exists (proj1_sig i').
    by exists (proj1_sig (i'': D ~> D'')).
  simpl.
  split; by rewrite -('i).
Defined.

Lemma Cat_to_slice_pb [A B C D: Slice X] [f: A ~> C] [g: B ~> C]
  [pA: D ~> A] [pB: D ~> B]
  (pb: Pullback (proj1_sig f) (proj1_sig g) (proj1_sig pA) (proj1_sig pB)):
  Pullback f g pA pB.
Proof.
  apply Build_Pullback.
    apply/Slice_mapP.
    apply: pb_comm pb.
  move=> /= E qA qB /Slice_mapP /= comm.
  have [u [eq_u1 eq_u2] unique_u]:= ump_pb pb (proj1_sig qA) (proj1_sig qB) comm.
  unshelve eexists.
      exists u.
      by rewrite -(proj2_sig qA) eq_u1 -compoA (proj2_sig pA).
    by split; apply/Slice_mapP.
  move=> v [/Slice_mapP eq_v1 /Slice_mapP eq_v2].
  apply/Slice_mapP=> /=.
  by apply unique_u.
Defined.

Lemma Cat_to_slice_po [A B C D: Slice X] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D]
  (po: Pushout (proj1_sig f) (proj1_sig g) (proj1_sig iA) (proj1_sig iB)):
  Pushout f g iA iB.
Proof.
  have d:= ump_po po (`2 (A)) (`2 (B)).
  forward d.
    by rewrite (proj2_sig f) (proj2_sig g).
  apply Build_Pushout.
    apply/Slice_mapP=> /=.
    apply: po_comm po.
  move=> /= E jA jB /Slice_mapP comm.
  have u:= ump_po po (proj1_sig jA) (proj1_sig jB) comm.
  unshelve eexists.
      exists u.
      apply: (Pushout_joint_epi po).
        by rewrite compoA -('u) (proj2_sig jA) (proj2_sig iA).
      by rewrite compoA -('u) (proj2_sig jB) (proj2_sig iB).
    split; apply/Slice_mapP; apply ('u).
  move=> v [/Slice_mapP /= eq_v1 /Slice_mapP /= eq_v2].
  apply/Slice_mapP=> /=.
  by apply: (''u).
Defined.

End Slice_po_pb.

Section Slice_has_pb.

Context {𝐂: cat_pb} [X: 𝐂].

Lemma Slice_has_pb: Cat_has_pb (Slice X).
Proof.
  constructor=> A B C f g.
  have:= Get_pb (proj1_sig f) (proj1_sig g)=>- /= [D' [pA' [pB' pb]]].
  unshelve eexists.
    exists D'.
    apply (`2 (A) ∘ pA').
  unshelve eexists.
    by exists pA'.
  unshelve eexists.
    exists pB'=> /=.
    by rewrite -(proj2_sig g) compoA -(pb_comm pb) -compoA (proj2_sig f).
  by apply Cat_to_slice_pb.
Defined.

HB.instance Definition _ := Slice_has_pb.
End Slice_has_pb.

Section Slice_po_rm.

Context {𝐂: cat_po_rm} [X: 𝐂].

Lemma Slice_to_cat_rm [A B: Slice X] (m: A ~> B): Reg_mono m ->
  Reg_mono (proj1_sig m).
Proof.
  move=> [C [f [g eqz]]].
  exists `1 (C).
  exists (proj1_sig f).
  exists (proj1_sig g).
  constructor.
    have:= Eq_comp eqz.
    by move/Slice_mapP.
  move=> E e eq_e.
  pose E': Slice X := existT _ E (`2 (B) ∘ e).
  have:= Eq_prop eqz (E' := E') (exist _ e eq_refl)=> ε.
  forward ε.
    by apply/Slice_mapP.
  exists (proj1_sig (ε: E' ~> A)).
    by have:= 'ε=> -/Slice_mapP.
  move=> ε' eq_ε'.
  rewrite ((''ε) (exist _ ε' _)) //.
    by rewrite /= eq_ε' -compoA (proj2_sig m).
  move=> /= eq.
  by apply/Slice_mapP.
Defined.

Lemma Slice_po_rm: Cat_has_po_along_reg_mono (Slice X).
Proof.
  constructor.
  move=> A B C m g rm_m.
  apply Slice_to_cat_rm in rm_m.
  have [/= D [iA [iB po]]]:=
    po_along_reg_mono (proj1_sig m) (proj1_sig g) rm_m.
  have d:= ump_po po (`2 (A)) (`2 (B)).
  forward d.
    by rewrite (proj2_sig m) (proj2_sig g).
  exists (existT _ D (d: D ~> X)).
  unshelve eexists.
    exists iA.
    by rewrite /= -('d). 
  unshelve eexists.
    exists iB.
    by rewrite /= -('d).
  by apply Cat_to_slice_po=> /=.
Defined.

HB.instance Definition _ := Slice_po_rm.
End Slice_po_rm.
