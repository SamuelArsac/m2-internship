Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
From HB Require Import structures.
Require Import cat_notations.
Require Import Morphism.
Require Import Subcategory.
Require Import Unique.

Local Open Scope cat.

Definition Iso_cat (𝐂: quiver): Type := 𝐂.

Section Iso_quiver.

Context {𝐂: precat}.

Definition IsIso2 [X Y: 𝐂] (f: X ~> Y) (g: Y ~> X) :=
    f ∘ g = idmap ∧ g ∘ f = idmap.

Definition iso_to_from [X Y: 𝐂] [f: X ~> Y] [g: Y ~> X]
  (iso_eq: IsIso2 f g): f ∘ g = idmap := fst iso_eq.
Definition iso_from_to [X Y: 𝐂] [f: X ~> Y] [g: Y ~> X]
  (iso_eq: IsIso2 f g): g ∘ f = idmap := snd iso_eq.

Definition IsIso [X Y: 𝐂] (f: X ~> Y) := {g: Y ~> X | IsIso2 f g}.

Definition Iso (X Y: 𝐂) := {
  f : X ~> Y &
  IsIso f
}.

HB.instance Definition _ := IsQuiver.Build (Iso_cat 𝐂) Iso.

Definition Build_Iso_eq [X Y: 𝐂] [f: X ~> Y] [g: Y ~> X]
  (eq1: f ∘ g = idmap) (eq2: g ∘ f = idmap): IsIso2 f g := (eq1, eq2).

Definition Build_IsIso [X Y: 𝐂] (to: X ~> Y) (from: Y ~> X)
  (iso_eq: IsIso2 to from): IsIso to
  := (exist _ from iso_eq).

Definition Build_Iso [X Y: 𝐂] (to: X ~> Y) (from: Y ~> X)
  (iso_eq: IsIso2 to from): Iso X Y
  := existT _ to (exist _ from iso_eq).

End Iso_quiver.

Reserved Notation "A ≅ B" (format "A  ≅  B", at level 70).
Reserved Notation "A ≅_ 𝐂 B" (format "A  ≅_ 𝐂  B", at level 70, 𝐂 at level 0).
Notation "A ≅ B" := (@hom (@reverse_coercion _ _ _ (Iso_cat _)) A B) (only printing).
Notation "A ≅ B" := (A ~>_(Iso_cat _) B).
Notation "A ≅_ 𝐂 B" := (A ~>_(Iso_cat 𝐂) B).
Notation "f '¹'" := (projT1 f) (at level 9, format "f '¹'").
Notation "f '⁻¹'" := (proj1_sig (projT2 f)) (at level 9, format "f '⁻¹'").

Definition iso_eq {𝐂: precat} [X Y: 𝐂] (iso: X ~>_(Iso_cat 𝐂) Y):
  IsIso2 (iso¹) (iso⁻¹) :=
  proj2_sig (projT2 iso).

Ltac promote_iso f :=
  match type of f with
  | ?A ~>_?𝐂 ?B =>
  fold (IsIso (𝐂 := 𝐂) f) in *;
  match goal with
  | [H: IsIso f|- _] =>
  let g := fresh in
  let iso_eq := fresh in
  destruct H as [g iso_eq];
  let i := fresh in 
  pose (i := Build_Iso (𝐂 := 𝐂) f g iso_eq: A ≅_𝐂 B);
  let eq := fresh in
  assert (eq: f = i¹); [reflexivity|
  clearbody i;
  subst f;
  rename i into f;
  clear g iso_eq
  ] end end.
  
Lemma Iso_mapP {𝐂: precat}: forall (X Y: 𝐂) (f g: X ≅ Y),
  f = g ↔ f¹ = g¹ ∧ f⁻¹ = g⁻¹.
Proof.
  intros.
  split.
  by inversion 1.
  intros [eq1 eq2].
  destruct f as [f1 [f2 iso_f]].
  destruct g as [g1 [g2 iso_g]].
  simpl in *.
  revert iso_f iso_g.
  rewrite eq1 eq2.
  intros.
  do 2 f_equal.
  apply Prop_irrelevance.
Qed.

Section Iso_cat.

Context {𝐂: cat}.

Definition Iso_id (X: 𝐂) : X ≅ X
  := Build_Iso idmap idmap (compo1 idmap, compo1 idmap).

Lemma Iso_comp (X Y Z: 𝐂) (f: X ≅ Y) (g: Y ≅ Z):
  X ≅ Z.
Proof.
  destruct f as [f1 [f2 iso_f]].
  destruct g as [g1 [g2 iso_g]].
  exists (g1 ∘ f1).
  exists (f2 ∘ g2).
  constructor.
  abstract by rewrite compoA -(compoA g2) iso_f compo1 iso_g.
  abstract by rewrite compoA -(compoA f1) iso_g compo1 iso_f.
Defined.

HB.instance Definition _ := IsPreCat.Build (Iso_cat 𝐂) Iso_id Iso_comp.

Lemma Iso_comp1o: forall (X Y: 𝐂) (f: X ≅ Y), f ∘ idmap = f.
Proof.
  intros.
  apply Iso_mapP.
  destruct f as [f1 [f2 iso_f]].
  simpl.
  by rewrite compo1 comp1o.
Qed.

Lemma Iso_compo1: forall (X Y: 𝐂) (f: X ≅ Y), idmap ∘ f = f.
Proof.
  intros.
  apply Iso_mapP.
  destruct f as [f1 [f2 iso_f]].
  simpl.
  by rewrite compo1 comp1o.
Qed.

Lemma Iso_compoA: forall (W X Y Z: 𝐂) (f: W ≅ X)
  (g: X ≅ Y) (h: Y ≅ Z), (h ∘ g) ∘ f = h ∘ (g ∘ f).
Proof.
  intros.
  apply Iso_mapP.
  destruct f as [f1 [f2 iso_f]].
  destruct g as [g1 [g2 iso_g]].
  destruct h as [h1 [h2 iso_h]].
  simpl.
  by rewrite 2!compoA.
Qed.

Lemma Iso_is_cat: PreCat_IsCat (Iso_cat 𝐂).
Proof.
  constructor.
  apply Iso_comp1o.
  apply Iso_compo1.
  apply Iso_compoA.
Qed.

HB.instance Definition _ := Iso_is_cat.

End Iso_cat.

Section Iso_lemmas_precat.

Context {𝐂: precat}.

Lemma iso_sym [X Y: 𝐂] (f: X ≅ Y) : Y ≅ X.
  apply (Build_Iso f⁻¹ f¹).
  split;
  apply iso_eq.
Defined.

Definition iso_op: forall [A A': 𝐂],
  A ≅_𝐂 A' -> A' ≅_(𝐂^op) A.
  intros ? ? iso.
  apply (Build_Iso (𝐂 := 𝐂^op) iso¹ iso⁻¹).
  abstract (split; apply (iso_eq iso)).
Defined.

Definition iso_op': forall [A A': 𝐂],
  A ≅_(𝐂^op) A' -> A' ≅_𝐂 A.
  intros ? ? iso.
  apply (Build_Iso (𝐂 := 𝐂) iso¹ iso⁻¹).
  abstract (split; apply (iso_eq iso)).
Defined.

Lemma iso_op_op': forall [A A': 𝐂] (iso: A ≅_(𝐂^op) A'),
  (iso_op (iso_op' iso)) = iso.
Proof.
  intros.
  destruct iso as [iso [iso_r [eq_iso1 eq_iso2]]].
  unfold iso_op.
  unfold iso_op'.
  simpl.
  unfold Build_Iso.
  do 2 f_equal.
  apply Prop_irrelevance.
Qed.

Lemma iso_op'_op: forall [A A': 𝐂] (iso: A ≅ A'),
  (iso_op' (iso_op iso)) = iso.
Proof.
  intros.
  destruct iso as [iso [iso_r [eq_iso1 eq_iso2]]].
  unfold iso_op.
  unfold iso_op'.
  simpl.
  unfold Build_Iso.
  do 2 f_equal.
  apply Prop_irrelevance.
Qed.

End Iso_lemmas_precat.

Section Iso_lemmas_cat.

Lemma Iso_mono {𝐂: cat} [A B: 𝐂] (iso: A ≅ B): Mono iso¹.
Proof.
  intros z g1 g2 eq.
  rewrite -(compo1 g1).
  destruct (iso_eq iso) as [_ eq_iso].
  by rewrite -eq_iso compoA eq -compoA eq_iso compo1.
Qed.

Lemma Iso_epi {𝐂: cat} [A B: 𝐂] (iso: A ≅ B): Epi iso¹.
Proof.
  apply (Iso_mono (𝐂 := 𝐂^op) (iso_op iso)).
Qed.

Context {𝐂: cat}.

Lemma iso_eq_switch_r [A B C: 𝐂] [iso: A ≅ B] (f: B ~> C) (g: A ~> C):
  f ∘ iso¹ = g ↔ f = g ∘ iso⁻¹.
Proof.
  split; intro eq.
  by rewrite -eq compoA iso_eq comp1o.
  by rewrite eq compoA iso_eq comp1o.
Qed.

Lemma iso_eq_switch_l [A B C: 𝐂] [iso: B ≅ C] (f: A ~> B) (g: A ~> C):
  iso¹ ∘ f = g ↔ f = iso⁻¹ ∘ g.
Proof.
  split; intro eq.
  by rewrite -eq -compoA iso_eq compo1.
  by rewrite eq -compoA iso_eq compo1.
Qed.

Lemma Mono_decomp_iso [A B C] (iso: A ≅ B)
  (m: B ~>_𝐂 C): Mono (m ∘ iso¹) -> Mono m.
Proof.
  intros mono_m_iso.
  intros D f1 f2 eq.
  unshelve epose proof (eq' := mono_m_iso _
    (iso⁻¹ ∘ f1) (iso⁻¹ ∘ f2) _).
  by rewrite 2!compoA -(compoA f1) -(compoA f2)
    (iso_to_from (iso_eq iso)) 2!compo1.
  apply iso_eq_switch_l in eq'.
  by rewrite -compoA (iso_to_from (iso_eq iso)) compo1 in eq'.
Qed.

End Iso_lemmas_cat.
