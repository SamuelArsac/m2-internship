(** * Square *)
(** Category of spans commuting with a given cospan *)

Unset Universe Checking.
Require Import cat.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Span.
Require Import Cospan.
Require Export Subcategory.

Local Open Scope cat.

Section Square_cat.

Context {𝐂: precat} [A B: 𝐂] (c: Cospan A B).

Definition Square_obj (s: Span A B) :=
  sp_l c ∘ sp_l s = sp_r c ∘ sp_r s.

Program Definition Square := Full_sub Square_obj.

Definition Build_square (s: Span A B)
  (comm: sp_l c ∘ sp_l s = sp_r c ∘ sp_r s): Square.
  unshelve econstructor.
  simpl.
  apply s.
  simpl.
  apply comm.
Defined.

End Square_cat.

Lemma Square_mapP {𝐂: precat} (A B: 𝐂) (c: Cospan A B) (s1 s2 : Square c) (f g : s1 ~> s2) :
  sp_map f = sp_map g ↔ f = g.
Proof.
  split.
  intro.
  by apply Span_mapP.
  intro.
  by rewrite H.
Qed.