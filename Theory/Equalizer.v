(** * Equalizers *)

Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Pullback.
Require Import Unique.
Require Import Isomorphism.

Local Open Scope cat.

Section Equalizer.

  Context {𝐂: precat}.
  Context [A B E: 𝐂].

  Class Equalizer (f g: A ~> B) (e: E ~> A) := {
      Eq_comp: f ∘ e = g ∘ e;

      Eq_prop: forall (E': 𝐂) (e': E' ~> A), f ∘ e' = g ∘ e' ->
       ∃! ε: E' ~> E,
        e' = e ∘ ε
    }.

End Equalizer.

Arguments Eq_comp {_} [_ _ _ _ _ _] _.
Arguments Eq_prop {_} [_ _ _ _ _ _] _ [_] _ _.

Lemma Pb_equalizer {𝐂: cat} [A B C: 𝐂] [e: A ~> B] [f g: B ~> C] (pb: Pullback f g e e):
  Equalizer f g e.
Proof.
  constructor.
  by apply pb_comm.
  intros.
  pose (ɛ := ump_pb pb e' e' H).
  exists (ɛ :> E' ~> A).
  apply ('ɛ).
  intros.
  by apply (''ɛ).
Qed.

Lemma Equalizer_unique {𝐂: cat} [A A' B C: 𝐂]
  [e: A ~> B] [e': A' ~> B] [f g: B ~> C]:
  Equalizer f g e -> Equalizer f g e' -> A ≅ A'.
Proof.
  intros eq_e eq_e'.
  destruct (Eq_prop eq_e' e (Eq_comp eq_e)) as [u eq_u unique_u].
  exists u.
  pose proof (Eq_prop eq_e e' (Eq_comp eq_e')) as [u' eq_u' unique_u'].
  exists u'.
  split.
  rewrite eq_u in eq_u'.
  pose proof (Eq_prop eq_e' e' (Eq_comp eq_e')).
  apply (Unicity X (u ∘ u') idmap).
  by rewrite -compoA.
  by rewrite comp1o.
  rewrite eq_u' in eq_u.
  pose proof (Eq_prop eq_e e (Eq_comp eq_e)).
  apply (Unicity X (u' ∘ u) idmap).
  by rewrite -compoA.
  by rewrite comp1o.
Qed.