Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
From HB Require Import structures.
Require Import ssreflect.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Unique.
Require Import Equalizer.
Require Import Reg_mono.
Require Import Slice.
Require Import Rm_Quasi_Adhesive.
Require Import Rm_Adhesive.
Require Import Adhesive.
Require Import VanKampen.
Require Import Subobject.
Require Import Morphism.
Require Import Adhesive_morphisms.
Require Import Isomorphism.
Require Import Utils.

Local Open Scope cat.

Section Slice_rm_qadh.

Context {𝐂: rm_q_adhesive} (X: 𝐂).

Lemma Cat_to_slice_rm [A B: Slice X] (m: A ~> B):
  Reg_mono (proj1_sig m) -> Reg_mono m.
Proof.
  move=> rm_m.
  apply (Reg_mono_from_pb m).
  have [/= C [f [g po]]]:= po_along_reg_mono
    (proj1_sig m) (proj1_sig m) rm_m.
  have pb := po_is_pb po rm_m.
  have c := ump_po po (`2 B) (`2 B) eq_refl.
  pose C' := existT _ C (c: C ~> X): Slice X.
  pose f' := exist _ f (eq_sym (fst ('c))): B ~> C'.
  pose g' := exist _ g (eq_sym (snd ('c))): B ~> C'.
  exists C', f', g'.
  by apply Cat_to_slice_pb.
Qed.

Lemma Slice_rm_qadh: Cat_is_Rm_Q_Adhesive (Slice X).
Proof.
  constructor; move=> A B C D f g iA iB po rm_f;
  have rm_f_cat := Slice_to_cat_rm _ rm_f;
  have /= po_cat :=
    po_along_reg_mono (proj1_sig f)
      (proj1_sig g) rm_f_cat.
    move=> A' B' C' D' f' g' iA' iB' α β χ δ
      pb1 pb2 pb3 pb4 comm.
    simpl in A', B', C', D'.
    apply Cat_to_slice_po.
    unshelve eapply (stable_po _ rm_f_cat
      (Slice_to_cat_pb pb1) (Slice_to_cat_pb pb2)
      (Slice_to_cat_pb pb3) (Slice_to_cat_pb pb4)).
      apply (Slice_to_cat_po po po_cat).
    by move: comm=> /Slice_mapP.
  apply Cat_to_slice_pb.
  apply po_is_pb=> //.
  apply (Slice_to_cat_po po po_cat).
Defined.

HB.instance Definition _ := Slice_rm_qadh.

End Slice_rm_qadh.

Section Slice_rm_adh.

Context {𝐂: rm_adhesive} (X: 𝐂).

Lemma Slice_rm_adh: Rm_Q_Adhesive_is_Rm_Adhesive (Slice X).
Proof.
  constructor.
  apply: reg_subobj_union_is_reg_subobj.
  move=> /= A B C D f g iA iB rm_f po A' B' C' D' f' g'
    iA' iB' α β χ δ eq1 eq2 eq3 pb1 pb2.
  split.
    move=> [pb3 pb4].
    apply (stable_po po rm_f (Pb_sym pb1) (Pb_sym pb2)
      (Pb_sym pb3) (Pb_sym pb4) eq1).
  move=> po_top.
  have rm_f_cat := Slice_to_cat_rm _ rm_f.
  have po_cat := Slice_to_cat_po po
    (po_along_reg_mono (proj1_sig f)
      (proj1_sig g) rm_f_cat).
  have pb1_cat := Slice_to_cat_pb pb1.
  have pb2_cat := Slice_to_cat_pb pb2.
  have rm_f'_cat := Reg_mono_stable_PB _ _ _ _ pb1_cat rm_f_cat.
  have po_top_cat := Slice_to_cat_po po_top
    (po_along_reg_mono (proj1_sig f')
      (proj1_sig g') rm_f'_cat).
  have /Slice_mapP /= eq1_cat := eq1.
  have /Slice_mapP /= eq2_cat := eq2.
  have /Slice_mapP /= eq3_cat := eq3.
  by have [/Cat_to_slice_pb + /Cat_to_slice_pb] :=
    snd (Po_rm_VK rm_f_cat po_cat _ _ _ _ _ _ _ _ _ _ _ _
    eq1_cat eq2_cat eq3_cat pb1_cat pb2_cat) po_top_cat.
Qed.

HB.instance Definition _ := Slice_rm_adh.

End Slice_rm_adh.

Section Slice_adh.

Context {𝐂: adhesive} (X: 𝐂).

Lemma Slice_to_cat_mono [A B: Slice X] (m: A ~> B): Mono m ->
  Mono (proj1_sig m).
Proof.
  move=> mono_m C g1 g2 eq.
  pose C' := existT _ C (`2 A ∘ g1): Slice X.
  pose g1' := exist _ g1 eq_refl: C' ~> A.
  have eq_g2': `2 (A) ∘ g2 = `2 (A) ∘ g1.
    by rewrite -(proj2_sig m) 2!compoA eq.
  pose g2' := exist _ g2 eq_g2': C' ~> A.
  replace g1 with (proj1_sig g1') by reflexivity.
  replace g2 with (proj1_sig g2') by reflexivity.
  apply/(Slice_mapP _ _ _ g1' g2').
  apply mono_m.
  by apply/Slice_mapP.
Qed.

Lemma Slice_adh: Rm_Adhesive_is_Adhesive (Slice X).
Proof.
  constructor=> A B m mono_m.
  apply: Cat_to_slice_rm.
  apply: mono_is_reg.
  by apply: Slice_to_cat_mono.
Qed.

HB.instance Definition _ := Slice_adh.

End Slice_adh.
