(** * Pushouts *)

Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Span.
Require Import Cospan.
Require Import Square.
Require Import Initial.
Require Import Unique.
Require Import Pullback.
Require Import Morphism.

Local Open Scope cat.

Section Pushout.

(** Definition of pushouts as pullbacks in the opposite category *)

Context {𝐂: cat}.
Context [A B C D: 𝐂].
Context (f: C ~> A) (g: C ~> B).
Context (iA: A ~> D) (iB: B ~> D).

Definition Pushout := @Pullback 𝐂^op A B C D f g iA iB.

Definition po_comm: Pushout -> iA ∘ f = iB ∘ g :=
  @pb_comm 𝐂^op _ _ _ _ _ _ _ _.

Definition po_init (po: Pushout): @Initial (@Square 𝐂^op A B
  (Build_span f g))^op (exist _ (Build_cospan iA iB)
  (pb_comm (𝐂 := 𝐂^op) po)) :=
  pb_term (𝐂 := 𝐂^op) po.

Definition Ump_po := forall E (jA: A ~> E) (jB: B ~> E),
  jA ∘ f = jB ∘ g ->
  ∃! u: D ~> E, jA = u ∘ iA ∧ jB = u ∘ iB.

Definition Build_Pushout :
  iA ∘ f = iB ∘ g -> Ump_po -> Pushout
  := @Build_Pullback 𝐂^op _ _ _ _ _ _ _ _.

Definition Destruct_Pushout : Pushout ->
  iA ∘ f = iB ∘ g ∧ Ump_po
  := @Destruct_Pullback 𝐂^op _ _ _ _ _ _ _ _.

Definition ump_po (po: Pushout): Ump_po := ump_pb po.

End Pushout.

Arguments Build_Pushout {_} [_ _ _ _ _ _ _ _].
Arguments Destruct_Pushout {_} [_ _ _ _ _ _ _ _].
Arguments po_comm {_} [_ _ _ _ _ _ _ _] _.
Arguments ump_po {_} [_ _ _ _ _ _ _ _] _ [_] _ _ comm'.

Definition Has_po {𝐂: cat} [A B C: 𝐂] (f: C ~> A) (g: C ~> B)
  := ∃ D (iA: A ~> D) (iB: B ~> D), Pushout f g iA iB.

Lemma Po_along_id {𝐂: cat} [A B: 𝐂] (a: A ~> B): Pushout a idmap idmap a.
Proof.
  exact (Pb_along_id (𝐂 := 𝐂^op) a).
Qed.

Section Symmetry.

(** Swapping the two first morphisms with the two others in the arguments *)

Definition Po_sym {𝐂: cat} [A B C X: 𝐂]
  [f: C ~> A] [g: C ~> B] [iA: A ~> X] [iB: B ~> X]
  (po: Pushout f g iA iB): Pushout g f iB iA
  := Pb_sym (𝐂 := 𝐂^op) po.

Definition Po_sym_eq {𝐂: cat} [A B C X: 𝐂]
  (f: C ~> A) (g: C ~> B) (iA: A ~> X) (iB: B ~> X):
  Pushout f g iA iB ↔ Pushout g f iB iA
  := Pb_sym_eq (𝐂 := 𝐂^op) _ _ _ _.

End Symmetry.

Definition Stable_po {𝐂: cat} [A B C D: 𝐂]
  [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
  (po: Pushout f g iA iB)
  := forall (A' B' C' D': 𝐂) (f': C' ~> A') (g': C' ~> B')
    (iA': A' ~> D') (iB': B' ~> D') (α: A' ~> A) (β: B' ~> B)
    (χ: C' ~> C) (δ: D' ~> D) (pb1: Pullback α f f' χ)
    (pb2: Pullback β g g' χ) (pb3: Pullback δ iA iA' α)
    (pb4: Pullback δ iB iB' β) (eq_top: iA' ∘ f' = iB' ∘ g'),
  Pushout f' g' iA' iB'.
