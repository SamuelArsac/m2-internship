From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Utils.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Product.
Require Import Cokernel_pair.
Require Import Split_mono.
Require Import Subcategory.
Require Import Adhesive_morphisms.
Require Import Rm_Quasi_Adhesive.
Require Import Rm_Adhesive.

Local Open Scope cat.

HB.mixin Record Rm_Adhesive_is_Adhesive 𝐂 of Rm_Adhesive 𝐂 := {
  mono_is_reg: forall [A B: 𝐂] (m: A ~> B), Mono m -> Reg_mono m
}.

Unset Universe Checking.
#[short(type="adhesive")]
HB.structure Definition Adhesive: Set :=
  { 𝐂 of Rm_Adhesive_is_Adhesive 𝐂 & }.
Set Universe Checking.

HB.factory Record Cat_is_Adhesive 𝐂 of Cat_pb 𝐂 := {
  po_mono_VK: forall [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
    [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f) (po: Pushout f g iA iB),
    VanKampen po;
  po_along_mono: forall [A B C: 𝐂] (f: C ~> A) (g: C ~> B)
    (mono_f: Mono f), ∃ D (iA: A ~> D) (iB: B ~> D), Pushout f g iA iB
}.

HB.builders Context 𝐂 of Cat_is_Adhesive 𝐂.

HB.instance Definition _ := Cat_has_po_along_reg_mono.Build 𝐂
    (fun A B C m g rm_m =>
        po_along_mono _ _ _ m g (Reg_mono_is_mono _ rm_m)).

HB.instance Definition _ := Cat_is_Rm_Adhesive_usual.Build 𝐂
    (fun A B C D f g iA iB rm_f =>
        po_mono_VK _ _ _ _ f g iA iB (Reg_mono_is_mono _ rm_f)).

Lemma Mono_is_reg: forall [A B: 𝐂] (m: A ~> B) (mono_m: Mono m), Reg_mono m.
  intros.
  destruct (po_along_mono _ _ _ m m mono_m) as [C [f [g po]]].
  simpl in C.
  exists C.
  exists f.
  exists g.
  pose (pb := VK_mono_pb _ _ _ _ po mono_m (po_mono_VK _ _ _ _ _ _ _ _ mono_m po)).
  destruct (Destruct_Pullback pb) as [e unicity].
  constructor; auto.
  intros.
  pose (unicity E' e' e' H).
  exists (u: E' ~> A).
  apply ('u).
  intros.
  by apply (''u).
Qed.

HB.instance Definition _ := Rm_Adhesive_is_Adhesive.Build 𝐂
  (fun (A B: 𝐂) (m: A ~> B) (mono_m: Mono m) =>
  Mono_is_reg m mono_m).

HB.end.

HB.factory Record Cat_is_Adhesive_adh_mono 𝐂 of Cat_pb 𝐂 := {
  mono_is_adh: forall (A B: 𝐂) (m: A ~> B) (mono_m: Mono m),
    Adhesive_morph m
}.

HB.builders Context 𝐂 of Cat_is_Adhesive_adh_mono 𝐂.

Lemma Adh_stable_union: Adh_morph_stable_union 𝐂.
Proof.
  intros A B C Z a b adh_a adh_b c mono_c union.
  apply (mono_is_adh _ _ _ mono_c).
Defined.

Lemma Split_mono_adh: Split_mono_adhesive 𝐂.
Proof.
  intros A B m split_mono.
  apply mono_is_adh.
  apply (Reg_mono_is_mono _ (Split_mono_reg _ split_mono)).
Defined.

Lemma Po_along_mono: forall [A B C: 𝐂] (f: C ~> A) (g: C ~> B)
  (mono_f: Mono f), ∃ D (iA: A ~> D) (iB: B ~> D),
  Pushout f g iA iB.
Proof.
  intros.
  destruct (Adhesive_is_Pre_adhesive _ (mono_is_adh _ _ _ mono_f) _ g)
    as [D [iA [iB [po [stable pb]]]]].
  simpl in D.
  exists D.
  exists iA.
  by exists iB.
Defined.

HB.instance Definition _ := Cat_is_Adhesive.Build 𝐂
  (fun A B C D f g iA iB mono_f po =>
    Bin_union_closed_VK Adh_stable_union Split_mono_adh
    _ _ _ _ _ _ _ _ po (mono_is_adh _ _ _ mono_f))
  Po_along_mono.

HB.end.

HB.factory Record Cat_is_Adhesive_quasi 𝐂 of Cat_pb 𝐂 := {
  po_along_mono: forall [A B C: 𝐂] (f: C ~> A) (g: C ~> B)
    (mono_f: Mono f), ∃ D (iA: A ~> D) (iB: B ~> D), Pushout f g iA iB;
  po_mono_stable [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
    (mono_f: Mono f) (po: Pushout f g iA iB):
    Stable_po po;
  po_mono_pb [A B C D: 𝐂]
    [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
    (mono_f: Mono f) (po: Pushout f g iA iB):
    Pullback iA iB f g
}.

HB.builders Context 𝐂 of Cat_is_Adhesive_quasi 𝐂.

Lemma Mono_is_adh [A C: 𝐂] [m: A ~> C] (mono_m: Mono m):
  Adhesive_morph m.
Proof.
  intros A' C' g h m' pb B' g'.
  pose proof (mono_m' := Mono_stable_pb pb mono_m). 
  destruct (po_along_mono _ _ _ m' g' mono_m')
    as [D' [iA' [iB' po]]].
  simpl in A', C', B', D'.
  exists D'.
  exists iA'.
  exists iB'.
  exists po.
  split.
  by apply po_mono_stable.
  by apply po_mono_pb.
Defined.

HB.instance Definition _ := Cat_is_Adhesive_adh_mono.Build 𝐂
  Mono_is_adh.

HB.end.

Lemma Po_mono_VK {𝐂: adhesive} [A B C D: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f)
  (po: Pushout f g iA iB): VanKampen po.
Proof.
  apply (Po_rm_VK (mono_is_reg _ _ _ mono_f)).
Defined.

Lemma po_along_mono {𝐂: adhesive} [A B C: 𝐂] (f: C ~> A)
  (g: C ~> B) (mono_f: Mono f):
  ∃ D (iA: A ~> D) (iB: B ~> D), Pushout f g iA iB.
Proof.
  apply (po_along_reg_mono _ _ (mono_is_reg _ _ _ mono_f)).
Defined.

Lemma Mono_stable_po {𝐂: adhesive} [A B C D: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f)
  (po: Pushout f g iA iB): Mono iB.
Proof.
  apply (VK_mono_stable_po _ _ _ _ po mono_f
    (Po_mono_VK mono_f po)).
Defined.

Lemma Mono_is_adh {𝐂: adhesive} (A B: 𝐂) [ m: A ~> B]
  (mono_m: Mono m): Adhesive_morph m.
Proof.
  apply (Reg_mono_is_adh (mono_is_reg _ _ _ mono_m)).
Defined.

Lemma Po_mono_stable {𝐂: adhesive} [A B C D: 𝐂] [f: C ~> A]
  [g: C ~> B] [iA: A ~> D] [iB: B ~> D] (mono_f: Mono f)
  (po: Pushout f g iA iB): Stable_po po.
Proof.
  apply (VanKampen_stable _ (Po_mono_VK mono_f po)).
Defined.

Lemma Po_mono_pb {𝐂: adhesive} [A B C D: 𝐂]
  [f: C ~> A] [g: C ~> B] [iA: A ~> D] [iB: B ~> D]
  (mono_f: Mono f) (po: Pushout f g iA iB):
  Pullback iA iB f g.
Proof.
  apply (VK_mono_pb _ _ _ _ po mono_f
    (Po_mono_VK mono_f po)).
Defined.