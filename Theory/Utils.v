From mathcomp Require Import ssreflect.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.

(* From MetaCoq *)
Ltac forward_gen H tac :=
  match type of H with
  | ?X -> _ => let H' := fresh in assert (H':X) ; [tac|specialize (H H'); clear H']
  end.

Tactic Notation "forward" constr(H) := forward_gen H ltac:(idtac).
Tactic Notation "forward" constr(H) "by" tactic(tac) := forward_gen H tac.

Lemma sig_eqP : forall (A: Type) (x y: A) (P: A -> Prop) (Px: P x) (Py: P y),
  exist _ x Px = exist _ y Py <-> x = y.
intros.
split.
intro.
by inversion H.
intros.
revert Px.
rewrite H.
intros.
f_equal.
apply Prop_irrelevance.
Qed.