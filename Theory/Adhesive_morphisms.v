From mathcomp Require Import ssreflect.
From HB Require Import structures.
Unset Universe Checking.
Require Import cat.
Set Universe Checking.
Require Import cat_notations.
Require Import Morphism.
Require Import Pushout.
Require Import Pushout_lemmas.
Require Import Pullback.
Require Import Pullback_lemmas.
Require Import Terminal.
Require Import Unique.
Require Import VanKampen.
Require Import Reg_mono.
Require Import Isomorphism.
Require Import Subobject.
Require Import Equalizer.
Require Import Split_mono.

Local Open Scope cat.

Definition Pre_adhesive_morph {𝐂: cat} [A C: 𝐂] (f: C ~> A) :=
  forall B (g: C ~> B), ∃ D (iA: A ~> D)
    (iB: B ~> D) (po: Pushout f g iA iB),
    Stable_po po ∧ Pullback iA iB f g.

Lemma Pre_adh_stb_po {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  Pre_adhesive_morph f -> Stable_po po.
Proof.
  intro pre_adh_f.
  destruct (pre_adh_f _ g) as [D' [jA [jB [po' [stb_po' _]]]]].
  by apply (Stable_po_switch po').
Qed.

Lemma Pre_adh_po_pb {𝐂: cat} [A B C D: 𝐂] [f: C ~> A] [g: C ~> B]
  [iA: A ~> D] [iB: B ~> D] (po: Pushout f g iA iB):
  Pre_adhesive_morph f -> Pullback iA iB f g.
Proof.
  intro pre_adh_f.
  destruct (pre_adh_f _ g) as [D' [jA [jB [po' [_ pb]]]]].
  destruct (unique_pushout po po') as [u [eq_u1 eq_u2] _].
  rewrite -eq_u1 -eq_u2 in pb.
  apply Pullback_iso_C in pb.
  by rewrite -compoA (iso_eq u) compo1 in pb.
Qed.

Definition Adhesive_morph {𝐂: cat} [A C: 𝐂] (f: A ~> C) :=
  forall B D (g: B ~> C) (pA: D ~> A) (pB: D ~> B) (pb: Pullback f g pA pB),
    Pre_adhesive_morph pB.

Lemma Iso_pre_adhesive {𝐂: cat} [A C: 𝐂] (f: C ≅ A): Pre_adhesive_morph f¹.
  intros B g.
  pose (po := Pushout_along_iso g f).
  exists B.
  exists (g ∘ f⁻¹).
  exists idmap.
  exists po.
  split.
  apply Pushout_along_iso_stable.
  by apply Pushout_along_iso_is_pb.
Qed.

Lemma Iso_adhesive {𝐂: cat} [A C: 𝐂] (f: A ≅ C): Adhesive_morph f¹.
Proof.
  intros B D g pA pB pb.
  pose (Iso_stable_pb _ pb).
  promote_iso pB.
  apply (Iso_pre_adhesive pB).
Qed.

Lemma Adhesive_is_Pre_adhesive {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Adhesive_morph f -> Pre_adhesive_morph f.
Proof.
  intro adh_f.
  apply (adh_f _ _ idmap idmap f).
  apply Build_Pullback.
  by rewrite compo1 comp1o.
  intros C pA pB eq.
  exists pA.
  rewrite compo1 in eq.
  rewrite compo1.
  by split.
  intros v [eq_v1 eq_v2].
  by rewrite compo1 in eq_v1.
Qed.

Lemma Pre_adhesive_is_regular_mono {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Pre_adhesive_morph f -> Reg_mono f.
  intro pre_adh_f.
  destruct (pre_adh_f _ f) as [C [iB [iB' [po [stable pb]]]]].
  exists C.
  exists iB.
  exists iB'.
  constructor.
  by apply pb_comm.
  intros.
  destruct (Destruct_Pullback pb) as [pb_comm ump_pb].
  pose (ump_pb E' e' e' H).
  exists (u :> E' ~> A).
  apply ('u).
  intros.
  by apply (''u).
Qed.

Lemma Adhesive_stable_pb {𝐂: cat} [A B C D: 𝐂] [f: A ~> C] [g: B ~> C]
  [pA: D ~> A] [pB: D ~> B] (pb: Pullback f g pA pB):
  Adhesive_morph f -> Adhesive_morph pB.
Proof.
  intro adh_f.
  intros ? ? ? ? ? pb'.
  apply (adh_f _ _ _ _ _ (Pb_comp pb pb')).
Qed.

Lemma Adh_is_Reg_mono {𝐂: cat} [A B: 𝐂] (f: A ~> B):
  Adhesive_morph f -> Reg_mono f.
Proof.
  intro adh_f.
  apply Adhesive_is_Pre_adhesive in adh_f.
  apply (Pre_adhesive_is_regular_mono _ adh_f).
Qed.

Lemma Pre_adhesive_comp {𝐂: cat_pb} [A B C: 𝐂] [a: B ~> C] [b: A ~> B]:
  Pre_adhesive_morph a -> Pre_adhesive_morph b -> Pre_adhesive_morph (a ∘ b).
Proof.
  intros preadh_a preadh_b.
  unfold Pre_adhesive_morph in *.
  intros D c.
  destruct (preadh_b D c) as [E [d [e [po_E [stb_po_E po_E_is_pb]]]]].
  destruct (preadh_a E d) as [F [f [g [po_F [stb_po_F po_F_is_pb]]]]].
  exists F.
  exists f.
  exists (g ∘ e).
  exists (Po_comp po_E po_F).
  split.
  apply (Stable_po_comp stb_po_E stb_po_F).
  apply (Pb_comp po_F_is_pb po_E_is_pb).
Qed.

Lemma Adhesive_comp {𝐂: cat_pb} [A B C: 𝐂] [a: B ~> C] [b: A ~> B]:
  Adhesive_morph a -> Adhesive_morph b -> Adhesive_morph (a ∘ b).
Proof.
  intros adh_a adh_b.
  intros A' C' α c' χ pb.
  apply Pb_sym in pb.
  destruct (pb_split pb) as [B' [a' [b' [β [eq_c' [pb1 pb2]]]]]].
  rewrite eq_c' in pb.
  rewrite eq_c'.
  apply Pb_sym in pb, pb1, pb2.
  pose proof (preadh_b' := adh_b _ _ _ _ _ pb1).
  pose proof (preadh_a' := adh_a _ _ _ _ _ pb2).
  apply (Pre_adhesive_comp preadh_a' preadh_b').
Qed.

Lemma Adhesive_po_is_mono {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Mono n.
Proof.
  (* For stability, suppose that m : C → A is adhesive, and consider
  a pushout [po] which is also a pullback, since m is adhesive. *)
  intro adh_m.
  pose proof (pb := Pre_adh_po_pb po (Adhesive_is_Pre_adhesive _ adh_m)).
  (* Pull back this pushout along n to get a cube in which the top face,
  like the bottom, is a pushout, and the four vertical faces are pullbacks;
  and in which n1 and n2 are the kernel pair of n. *)
  destruct (Get_pb n n) as [B' [n1 [n2 pb_right]]].
  simpl in B'.
  pose proof (pb_left :=
    fst (Mono_trivial_pb (f := m)) (Reg_mono_is_mono _
      (Adh_is_Reg_mono _ adh_m))).
  destruct (Fill_cube_pb (po_comm po) pb_left (Pb_sym pb) pb_right)
    as [f' [pb_back comm_top]].
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (po_top := stb_po _ _ _ _ _ _ _ _ _ _ _ _ pb_left pb_back
    (Pb_sym pb) pb_right comm_top).
  (* Since n1 is a pushout of an isomorphism it is an isomorphism. *)
  pose (Iso_stable_po (Iso_id _) po_top).
  promote_iso n1.
  (* This implies that the kernel pair of n is trivial,
  and so that n is a monomorphism; thus any pushout of an
  adhesive morphism is a monomorphism. *)
  rewrite -(compo1 n1¹) in pb_right.
  apply (Pullback_iso_D n1) in pb_right.
  simpl in pb_right.
  pose proof (eq_id := Kernel_pair_id (n2 ∘ n1⁻¹) pb_right).
  rewrite eq_id in pb_right.
  by apply (Mono_trivial_pb (f := n)).
Qed.

Lemma Adhesive_po_is_pre_adh {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Pre_adhesive_morph n.
Proof.
  intros adh_m E h.
  pose proof (pb := Pre_adh_po_pb po (Adhesive_is_Pre_adhesive _ adh_m)).
  destruct ((Adhesive_is_Pre_adhesive _ adh_m) E (h ∘ f))
    as [F [l [p [po_F [stb_po_F pb_C]]]]].
  destruct (po_fill po_F po) as [k [eq_l po'_F]].
  subst.
  exists F.
  exists k.
  exists p.
  exists po'_F.
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  apply Stable_po_sym in stb_po_F, stb_po.
  apply Stable_po_sym.
  apply (Stable_po_decomp _ stb_po stb_po_F).

  pose proof (mono_n := Adhesive_po_is_mono po adh_m).
  pose proof (pb_n := (fst (Mono_trivial_pb (f := n))) mono_n).
  unshelve eapply (Pb_sym (lemma22 _ stb_po (Pb_sym pb) (Pb_sym pb_C) pb_n
    _ (po_comm po'_F))).
  pose proof (mono_p := Adhesive_po_is_mono po_F adh_m).
  rewrite (po_comm po'_F) comp1o.
  apply Build_Pullback.
  by rewrite comp1o.
  intros Y qA qB comm.
  exists qB.
  rewrite compoA in comm.
  apply mono_p in comm.
  split.
  assumption.
  by rewrite compo1.
  intros v [eq_v1 eq_v2].
  by rewrite eq_v2 compo1.
Qed.

(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
2.3 Proposition. The class of adhesive morphisms is stable under pushout.*)
Lemma Adhesive_stable_po {𝐂: cat_pb} [A B C D: 𝐂] [m: C ~> A] [f: C ~> B]
  [g: A ~> D] [n: B ~> D] (po: Pushout m f g n):
  Adhesive_morph m -> Adhesive_morph n.
Proof.
  intros adh_m D' B' d b n' pb_B'.
  destruct (Get_pb d g) as [A' [g' [a pb_A']]].
  destruct (Get_pb a m) as [C' [m' [c pb_C']]].
  simpl in A', C'.
  destruct (Fill_cube_pb (po_comm po) pb_C' pb_A' (Pb_sym pb_B'))
    as [f' [pb'_C' comm']].
  pose proof (adh_m' := Adhesive_stable_pb (Pb_sym pb_C') adh_m).
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (po_D' := stb_po _ _ _ _ _ _ _ _ _ _ _ _ pb_C' pb'_C' pb_A'
    (Pb_sym pb_B') comm').
  apply (Adhesive_po_is_pre_adh po_D' adh_m').
Qed.

(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
2.4. Proposition. Binary unions are effective provided that
at least one of the objects is adhesive. *)
Lemma Adhesive_subobj_union_effective {𝐂: cat_pb} [A B E: 𝐂]
  (h: A ~> E) (p: B ~> E) (mono_h: Mono h) (adh_p: Adhesive_morph p):
  Subobj_effective_union h p mono_h
    (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_p)).
Proof.
  destruct (Get_pb h p) as [C [m [f pb]]].
  simpl in C.
  pose proof (mono_f := Mono_stable_pb pb mono_h).
  pose proof (adh_m := Adhesive_stable_pb (Pb_sym pb) adh_p).
  destruct (Adhesive_is_Pre_adhesive _ adh_m _ f)
    as [D [g [n [po _]]]].
  simpl in D.
  exists D.
  exists C.
  exists m.
  exists g.
  exists f.
  exists n.
  exists pb.
  exists po.
  intros x [eq_x1 eq_x2].
  assert (mono_g: Mono g).
  apply (Mono_decomp g x).
  by rewrite -eq_x1.
  pose proof (adh_n := Adhesive_stable_po po adh_m).
  pose proof (stb_po :=
    Pre_adh_stb_po po (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (pb' :=
    Pre_adh_po_pb po (Adhesive_is_Pre_adhesive _ adh_m)).
  apply (Mono_trivial_pb).
  unshelve eapply (lemma22 po stb_po
    (Pb_sym (Pb_along_id g)) _
    (Pb_sym (Pb_along_id n)) _ eq_refl).
  rewrite -eq_x1 compo1.
  apply Pb_sym.
  apply Mono_trivial_pb in mono_g.
  unshelve eapply (lemma22 po stb_po mono_g _
    pb').
  rewrite -eq_x1 compo1.
  by apply Mono_trivial_pb.
  by rewrite compo1 -eq_x2.
  by rewrite comp1o.
  apply Pb_sym.
  unshelve eapply (lemma22 (a1' := f) (p1 := m) 
    (a2' := idmap) (p2 := idmap) po stb_po).
  rewrite compo1.
  by apply Pb_sym.
  rewrite -eq_x1 -eq_x2 compo1.
  by apply Pb_sym.
  rewrite compo1.
  apply (Mono_trivial_pb (f := n)).
  apply (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_n)).
  rewrite -eq_x2 compo1.
  apply (Mono_trivial_pb (f := p)).
  apply (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_p)).
  by rewrite compo1 comp1o -eq_x2.
Defined.

Definition Adh_morph_stable_union (𝐂: cat) :=
  forall (A B C Z: 𝐂) (a: A ~> Z) (b: B ~> Z)
  (adh_a: Adhesive_morph a) (adh_b: Adhesive_morph b)
  (c: C ~> Z) (mono_c: Mono c),
  Subobj_union Z (Build_subobj a 
    (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_a)))
  (Build_subobj b 
    (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_b)))
  (Build_subobj c mono_c)
  -> Adhesive_morph c.

Definition Split_mono_adhesive (𝐂: cat) :=
  (forall (A B: 𝐂) (m: A ~> B), Split_mono m -> Adhesive_morph m).

(* On the axioms for adhesive and quasiadhesive categories - Garner & Lack - 2012
2.5. Lemma. *)
Lemma lemma25 {𝐂: cat_pb} (stb_union: Adh_morph_stable_union 𝐂)
  (split_mono_adh: Split_mono_adhesive 𝐂) [A A2 B C C2 D: 𝐂]
  [f: C ~> B] [f1 f2: C2 ~> C] [g: A ~> D] [g1 g2: A2 ~> A]
  [m: C ~> A] [m2: C2 ~> A2] [n: B ~> D] [γ: C ~> C2]
  [δ: A ~> A2] (adh_m: Adhesive_morph m):
  Pushout m f g n -> Pullback f f f1 f2 ->
  Pullback g g g1 g2 -> g1 ∘ m2 = m ∘ f1 -> g2 ∘ m2 = m ∘ f2 ->
  f1 ∘ γ = idmap -> f2 ∘ γ = idmap -> g1 ∘ δ = idmap ->
  g2 ∘ δ = idmap ->
  (Pullback m2 δ γ m ∧ Pushout m γ δ m2 ∧ Pullback m g1 f1 m2 ∧
  Pushout m2 f1 g1 m ∧ Pullback m g2 f2 m2 ∧ Pushout m2 f2 g2 m ∧
  Pullback n g f m).
Proof.
  intros po_r kern_f kern_g eq_m21 eq_m22 eq_f1 eq_f2
    eq_g1 eq_g2.
  pose proof (stb_po_r := Pre_adh_stb_po po_r
    (Adhesive_is_Pre_adhesive _ adh_m)).
  pose proof (pb_r := Pre_adh_po_pb po_r
    (Adhesive_is_Pre_adhesive _ adh_m)).
  assert (pb_mid1: Pullback g1 m m2 f1).
  unshelve eapply (Pb_decomp eq_m21 (Pb_sym kern_g) _).
  rewrite (pb_comm pb_r) eq_m22.
  apply (Pb_comp pb_r (Pb_sym kern_f)).
  assert (pb_mid2: Pullback g2 m m2 f2).
  unshelve eapply (Pb_decomp eq_m22 kern_g _).
  rewrite (pb_comm pb_r) eq_m21.
  apply (Pb_comp pb_r kern_f).
  pose proof (po_mid1 := stb_po_r _ _ _ _ _ _ _ _ _ _ _ _
    pb_mid2 kern_f kern_g pb_r eq_m21).
  pose proof (po_mid2 := stb_po_r _ _ _ _ _ _ _ _ _ _ _ _
  pb_mid1 (Pb_sym kern_f) (Pb_sym kern_g) pb_r eq_m22).
  pose proof (adh_m2 := Adhesive_stable_pb (Pb_sym pb_mid1) adh_m).
  assert (pb_l: Pullback δ m2 m γ).
  apply Pb_sym.
  unshelve eapply (Pb_decomp _ (Pb_sym pb_mid1) _).
  apply (Pullback_joint_mono kern_g).
  by rewrite -2!compoA eq_m21 eq_g1 compoA eq_f1 compo1 comp1o.
  by rewrite -2!compoA eq_m22 eq_g2 compoA eq_f2 compo1 comp1o.
  rewrite eq_g1 eq_f1.
  apply (Pb_along_id m).
  destruct ((Adhesive_is_Pre_adhesive _ adh_m) _ γ)
    as [E [i [j [po_ij [stb_po_ij pb_ij]]]]].
  unshelve epose proof (h1 := ump_po po_ij idmap (m ∘ f1) _).
  by rewrite compoA eq_f1 compo1 comp1o.
  unshelve epose proof (k := ump_po po_ij δ m2 _).
  apply (Pullback_joint_mono kern_g).
  by rewrite -2!compoA eq_g1 eq_m21 compoA eq_f1 compo1 comp1o.
  by rewrite -2!compoA eq_g2 eq_m22 compoA eq_f2 compo1 comp1o.
  assert (adh_δ: Adhesive_morph δ).
  apply split_mono_adh.
  by exists g1.
  unshelve epose proof (adh_k := stb_union _ _ _ _ δ m2 adh_δ adh_m2 k _ _).
  pose proof (mono_δ := Reg_mono_is_mono _
    (Adh_is_Reg_mono _ adh_δ)).
  pose proof (effective :=
    Adhesive_subobj_union_effective _ _ mono_δ adh_m2).
  apply (Subobj_effective_union_mono _ _ _ _ _ effective
    _ _ _ _ pb_l po_ij).
  split; by rewrite -('k).
  apply Subobj_effective_union_is_union.
  split.
  by apply Pb_sym.
  split.
  unshelve epose proof (po_mh1 := Po_decomp (snd ('h1)) (Po_sym po_ij) _).
  rewrite eq_f1 -('h1).
  apply (Po_sym (Po_along_id m)).
  assert (po_kh1: Pushout k h1 g1 idmap).
  unshelve eapply (Po_decomp _ (Po_sym po_mh1) _).
  rewrite compo1.
  apply (Pushout_joint_epi po_ij).
  by rewrite compoA -('k) eq_g1 -('h1).
  by rewrite compoA -('k) eq_m21 -('h1).
  rewrite -('k) compo1.
  apply po_mid1.
  pose proof (pb_g1id := 
    Pb_sym (Pre_adh_po_pb po_kh1 (Adhesive_is_Pre_adhesive _ adh_k))).
  set (k' := (k: E ~> A2)) in pb_g1id.
  pose proof (Iso_stable_pb (Iso_id _) pb_g1id).
  promote_iso k'.
  pose proof (po := fst (Pushout_iso_D' k') po_ij).
  by rewrite -H2 -2!('k) in po.
  split.
  by apply Pb_sym.
  split.
  apply po_mid1.
  split.
  by apply Pb_sym.
  split.
  apply po_mid2.
  by apply Pb_sym.
Defined.

Lemma Bin_union_closed_VK_aux {𝐂: cat_pb} [A A' B B' C C' D D': 𝐂]
  [m: C ~> A] [f: C ~> B] [g: A ~> D] [n: B ~> D]
  [m': C' ~> A'] [f': C' ~> B'] [g': A' ~> D'] [n': B' ~> D']
  [a: A' ~> A] [b: B' ~> B] [c: C' ~> C] [d: D' ~> D]
  (po_bot: Pushout m f g n)
  (pb_bot: Pullback g n m f) [po_top: Pushout m' f' g' n']
  (stb_po_top: Stable_po po_top) (pb_top: Pullback g' n' m' f')
  (pb_l: Pullback m a c m') (pb_b: Pullback f b c f')
  (eq_r: d ∘ n' = n ∘ b) (eq_f: d ∘ g' = g ∘ a)
  (mono_n: Mono n) (mono_n': Mono n'): Pullback n d b n'.
Proof.
  apply (Mono_trivial_pb (f := n)) in mono_n.
  rename mono_n into pb_n.
  apply (Mono_trivial_pb (f := n')) in mono_n'.
  rename mono_n' into pb_n'.
  unshelve eapply (lemma22 _ stb_po_top (Pb_sym pb_top) _ pb_n' _ eq_r).
  rewrite eq_f -(pb_comm pb_b).
  apply (Pb_comp (Pb_sym pb_bot) pb_l).
  rewrite eq_r comp1o -{2}(compo1 b).
  apply (Pb_comp pb_n (Pb_sym (Pb_along_id b))).
Qed.

(* Quiver diagram:
https://q.uiver.app/#q=WzAsOCxbMSwwLCJDJyJdLFsxLDMsIkMiXSxbNCwwLCJCJyJdLFs0LDMsIkIiXSxbMyw0LCJEIl0sWzAsNCwiQSJdLFswLDEsIkEnIl0sWzMsMSwiRCciXSxbMCw2LCJtJyIsMl0sWzAsMiwiZiciXSxbMiw3LCJuJyJdLFs2LDcsImcnIiwwLHsibGFiZWxfcG9zaXRpb24iOjcwfV0sWzYsNSwiYSIsMl0sWzUsNCwiZyIsMl0sWzMsNCwibiJdLFsxLDMsImYiLDAseyJsYWJlbF9wb3NpdGlvbiI6MzB9XSxbMSw1LCJtIl0sWzAsMSwiYyIsMCx7ImxhYmVsX3Bvc2l0aW9uIjo3MH1dLFsyLDMsImIiXSxbNyw0LCJkIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV1d
*)
Lemma Bin_union_closed_VK {𝐂: cat_pb}:
  Adh_morph_stable_union 𝐂 ->
  Split_mono_adhesive 𝐂 ->
  forall (A B C D: 𝐂) (m: C ~> A) (f: C ~> B) (g: A ~> D) (n: B ~> D)
  (po: Pushout m f g n), Adhesive_morph m -> VanKampen po.
Proof.
  intros stb_union split_mono_adh ? ? ? ? ? ? ? ? ? adh_m.
  intros ? ? ? ? m' f' g' n' a b c d eq_top eq_f eq_r
    pb_l pb_b.
  pose proof (stb_po := Pre_adh_stb_po po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  { intros [pb_f pb_r].
  apply Pb_sym in pb_l, pb_b, pb_f, pb_r.
  eapply stb_po; auto.
  apply pb_l.
  apply pb_b.
  apply pb_f.
  apply pb_r. }
  intros po_top.
  pose proof (adh_m' := Adhesive_stable_pb pb_l adh_m).
  pose proof (adh_n' := Adhesive_stable_po po_top adh_m').
  pose proof (adh_n := Adhesive_stable_po po adh_m).
  pose proof (stb_po_top := Pre_adh_stb_po po_top
    (Adhesive_is_Pre_adhesive _ adh_m')).
  pose proof (pb_top := Pre_adh_po_pb po_top
    (Adhesive_is_Pre_adhesive _ adh_m')).
  pose proof (pb_bot := Pre_adh_po_pb po
    (Adhesive_is_Pre_adhesive _ adh_m)).
  split.
  2: apply (Bin_union_closed_VK_aux po pb_bot stb_po_top pb_top
    pb_l pb_b eq_r eq_f (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_n))
    (Reg_mono_is_mono _ (Adh_is_Reg_mono _ adh_n'))).

  destruct (Get_pb g' g') as [A2' [g1' [g2' pb_g']]].
  destruct (Get_pb g g) as [A2 [g1 [g2 pb_g]]].
  simpl in A2, A2'.
  (* Diagram for this application of lemma 2.2:
  https://q.uiver.app/#q=WzAsOSxbMCwzLCJDJyJdLFszLDMsIkEnIl0sWzEsNCwiQiciXSxbNCw0LCJEJyJdLFs0LDEsIkEnIl0sWzcsMSwiQSJdLFs3LDQsIkQiXSxbMywwLCJBJ18yIl0sWzEsMSwiQyciXSxbMCwxLCJtJyIsMCx7ImxhYmVsX3Bvc2l0aW9uIjo3MH1dLFswLDIsImYnIiwyXSxbMiwzLCJuJyIsMl0sWzEsMywiZyciXSxbNCwzLCJnJyJdLFs0LDUsImEiLDJdLFszLDYsImQiLDJdLFs1LDYsImciXSxbNyw0LCJnJ18xIl0sWzcsMSwiZydfMiIsMCx7ImxhYmVsX3Bvc2l0aW9uIjo3MH1dLFs4LDQsIm0nIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzgsMiwiZiciLDIseyJsYWJlbF9wb3NpdGlvbiI6MzB9XV0=
  *)
  unshelve eapply (lemma22 _ stb_po_top pb_g' _ pb_top _ eq_f).
  unshelve epose proof (a2 := ump_pb pb_g (a ∘ g1') (a ∘ g2') _).
  by rewrite -2!compoA -eq_f 2!compoA (pb_comm pb_g').
  rewrite ('a2) eq_f.
  apply (Pb_comp pb_g).
  destruct (Get_pb f f) as [C2 [f1 [f2 pb_f]]].
  simpl in C2.
  unshelve epose proof (m2 := ump_pb pb_g (m ∘ f1) (m ∘ f2) _).
  by rewrite -2!compoA (pb_comm pb_bot) 2!compoA (pb_comm pb_f).
  pose proof (γ := ump_pb pb_f idmap idmap eq_refl).
  pose proof (δ := ump_pb pb_g idmap idmap eq_refl).
  (* Diagram for this application of lemma 2.5:
  https://q.uiver.app/#q=WzAsOCxbMCwwLCJDIl0sWzEsMCwiQ18yIl0sWzAsMSwiQSJdLFsxLDEsIkFfMiJdLFsyLDAsIkMiXSxbMiwxLCJBIl0sWzMsMCwiQiJdLFszLDEsIkQiXSxbMCwxLCJcXGdhbW1hIl0sWzAsMiwibSIsMl0sWzIsMywiXFxkZWx0YSIsMl0sWzEsMywibV8yIiwyXSxbMSw0LCJmXzEiLDAseyJvZmZzZXQiOi0yfV0sWzEsNCwiZl8yIiwyLHsib2Zmc2V0IjoyfV0sWzMsNSwiZ18xIiwwLHsib2Zmc2V0IjotMn1dLFszLDUsImdfMiIsMix7Im9mZnNldCI6Mn1dLFs0LDUsIm0iXSxbNCw2LCJmIl0sWzUsNywiZyIsMl0sWzYsNywibiJdXQ==
  *)
  destruct (lemma25 stb_union split_mono_adh adh_m po pb_f pb_g
    (eq_sym (fst ('m2))) (eq_sym (snd ('m2)))
    (eq_sym (fst ('γ))) (eq_sym (snd ('γ)))
    (eq_sym (fst ('δ))) (eq_sym (snd ('δ)))) as [? [po_mγ ?]].
  pose proof (stb_po_mγ := Pre_adh_stb_po po_mγ
    (Adhesive_is_Pre_adhesive _ adh_m)).

  pose proof (δ' := ump_pb pb_g' idmap idmap eq_refl).
  destruct (Get_pb f' f') as [C2' [f1' [f2' pb_f']]].
  simpl in C2'.
  unshelve epose proof (c2 := ump_pb pb_f (c ∘ f1') (c ∘ f2') _).
  by rewrite -2!compoA (pb_comm pb_b) 2!compoA (pb_comm pb_f').
  assert (pb_f2c: Pullback f2 c c2 f2').
  apply (Pb_decomp (eq_sym (snd ('c2))) pb_f).
  rewrite (pb_comm pb_b) -('c2).
  apply (Pb_comp pb_b pb_f').
  pose proof (γ' := ump_pb pb_f' idmap idmap eq_refl).
  unshelve epose proof (m2' := ump_pb pb_g' (m' ∘ f1') (m' ∘ f2') _).
  by rewrite -2!compoA eq_top 2!compoA (pb_comm pb_f').
  assert (pb_c2γ: Pullback c2 γ γ' c).
  unshelve eapply (Pb_decomp _ (Pb_sym pb_f2c)).
  apply (Pullback_joint_mono pb_f);
  by rewrite -2!compoA -('c2) -('γ) compo1 compoA -('γ') comp1o.
  rewrite -('γ) -('γ').
  apply Pb_along_id.

  apply Pb_sym.

  destruct (lemma25 stb_union split_mono_adh adh_m' po_top pb_f' pb_g'
    (eq_sym (fst ('m2'))) (eq_sym (snd ('m2')))
    (eq_sym (fst ('γ'))) (eq_sym (snd ('γ')))
    (eq_sym (fst ('δ'))) (eq_sym (snd ('δ')))) as [? [po_m'γ' ?]].
    pose proof (stb_po_m'γ' := Pre_adh_stb_po po_m'γ' (Adhesive_is_Pre_adhesive _ adh_m')).
  assert (mono_δ: Mono δ).
  intros X h1 h2 eq_h.
  by rewrite -(compo1 h1) -(compo1 h2) ('δ) 2!compoA eq_h.
  assert (mono_δ': Mono δ').
  intros X h1 h2 eq_h.
  by rewrite -(compo1 h1) -(compo1 h2) ('δ') 2!compoA eq_h.
  (* --- *)
  assert (eq_r': a2 ∘ δ' = δ ∘ a).
  apply (Pullback_joint_mono pb_g);
  by rewrite -2!compoA -('δ) compo1 -('a2) compoA -('δ') comp1o.
  assert (eq_f': a2 ∘ m2' = m2 ∘ c2).
  apply (Pullback_joint_mono pb_g);
  by rewrite -2!compoA -('m2) -('a2) 2!compoA -('c2) -('m2')
    -2!compoA (pb_comm pb_l).
  (* Diagram for this application of lemma 2.2:
  https://q.uiver.app/#q=WzAsOSxbMCwzLCJDIl0sWzMsMywiQ18yIl0sWzEsNCwiQSJdLFs0LDQsIkFfMiJdLFs0LDEsIkEnXzIiXSxbNywxLCJBJyJdLFs3LDQsIkEiXSxbMSwxLCJBJyJdLFszLDAsIkMnXzIiXSxbMCwxLCJcXGdhbW1hIiwwLHsibGFiZWxfcG9zaXRpb24iOjcwfV0sWzAsMiwibSIsMl0sWzEsMywibV8yIl0sWzIsMywiXFxkZWx0YSIsMl0sWzQsMywiYV8yIiwyXSxbNCw1LCJnJ18yIl0sWzUsNiwiYSJdLFszLDYsImdfMiIsMl0sWzcsMiwiYSIsMix7ImxhYmVsX3Bvc2l0aW9uIjozMH1dLFs3LDQsIlxcZGVsdGEnIiwwLHsibGFiZWxfcG9zaXRpb24iOjMwfV0sWzgsMSwiY18yIiwwLHsibGFiZWxfcG9zaXRpb24iOjcwfV0sWzgsNCwibSdfMiJdXQ==
  *)
  eapply (lemma22 _ stb_po_mγ).

  apply Pb_sym.
  apply Stable_po_sym in stb_po_m'γ'.
  unshelve eapply (Bin_union_closed_VK_aux (Po_sym po_mγ) p
    stb_po_m'γ' p1 (Pb_sym pb_c2γ) pb_l eq_r' eq_f' mono_δ mono_δ').

  rewrite -('δ) -('δ').
  apply Pb_along_id.

  apply Pb_sym.
  pose proof (adh_m2 := Adhesive_stable_po po_mγ adh_m).
  pose proof (adh_m2' := Adhesive_stable_po po_m'γ' adh_m').
  apply (Bin_union_closed_VK_aux po_mγ (Pb_sym p)
    stb_po_m'γ' (Pb_sym p1) pb_l (Pb_sym pb_c2γ)
    eq_f' eq_r'
    (Reg_mono_is_mono _(Adh_is_Reg_mono _ adh_m2))
    (Reg_mono_is_mono _(Adh_is_Reg_mono _ adh_m2'))).

  rewrite -('m2) -('m2').
  apply (Pb_comp (Pb_sym pb_l) (Pb_sym pb_f2c)).

  by rewrite -('a2).

  rewrite -(pb_comm pb_l) eq_r.
  apply (Pb_comp pb_bot pb_b).
Qed.
  